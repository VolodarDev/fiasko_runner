import sys
from YaDiskClient.YaDiskClient import YaDisk
import platform
import os

from py_lib import fileutils
from py_lib import local_properties


yandex_disk = None
yandex_disk_deploy_directory = None
yandex_disk_ls_info = {}
OUT_DIRECTORY = ''


def yandex_disk_ls(path):
    if path not in yandex_disk_ls_info:
        try:
            ls = yandex_disk.ls(path)
            yandex_disk_ls_info[path] = ls
        except:
            return None
    return yandex_disk_ls_info[path]


def yandex_disk_init():
    global yandex_disk
    global yandex_disk_deploy_directory
    yandex_disk_deploy_directory = local_properties.read('yandex_disk.deploy_directory')
    login = local_properties.read('yandex_disk.login')
    password = local_properties.read('yandex_disk.password')
    yandex_disk = YaDisk(login, password)


def yandex_disk_isdir(path):
    try:
        if not path.endswith('/'):
            path += '/'
        ls = yandex_disk_ls(path)
        if ls is None:
            return False
        for e in ls:
            if 'path' in e and e['path'] == path and 'isDir' in e:
                return e['isDir']
        return False
    except:
        return False


def yandex_disk_create_dir(path):
    try:
        directories = path.split('/')
        path_ = ''
        for directory in directories:
            if not directory:
                continue
            path_ += '/' + directory
            if not yandex_disk_isdir(path_):
                print 'create path [{}]'.format(path_)
                yandex_disk.mkdir(path_)
    except:
        pass


def yandex_disk_create_dir_for_file(file):
    directory = file[0:file.rfind('/')]
    if not yandex_disk_isdir(directory):
        yandex_disk_create_dir(directory)


def yandex_disk_convert_path_to_remote(path):
    global OUT_DIRECTORY
    return yandex_disk_deploy_directory + path.replace(OUT_DIRECTORY, '')


def yandex_disk_get_remote_md5(file):
    directory = file[0:file.rfind('/')]
    ls = yandex_disk_ls(directory)
    if ls is None:
        return ''
    for e in ls:
        if 'path' in e and e['path'] == file and 'etag' in e:
            return e['etag']
    return ''


def yandex_upload(src, dst):
    if yandex_disk is not None:
        path = yandex_disk_convert_path_to_remote(dst)
        # print 'Compare file with yandex.disk [{}]'.format(dst)
        md5_local = fileutils.getMd5File(src)
        md5_remote = yandex_disk_get_remote_md5(path)
        # print 'local:  [{}] ===> {}\nremote: [{}] ===> {}'.format(md5_local, src, md5_remote, path)
        if md5_local != md5_remote:
            yandex_disk_create_dir_for_file(path)
            print '\tUpload file to yandex.disk [{}]'.format(path)
            yandex_disk.upload(src, path)


def copy_file(src, dst):
    def local(src, dst):
        fileutils.copy(src, dst)
    local(src, dst)
    yandex_upload(src, dst)


def write_file(path, content):
    def local(path, content):
        fileutils.write(path, content)
    local(path, content)
    yandex_upload(path, path)


def create_dir_for_file(file):
    def local(file):
        fileutils.createDirForFile(file)
    local(file)


def update():
    print '\tUpdate...:'
    os.system('git pull')


def include_files_to_vs_project(src_folder, group, project_file):
    lines = open(project_file).read().split('\n')
    files = fileutils.getFilesList(src_folder)
    for file in files:
        while file.find('/') != -1:
            file = file[0:file.find('/')] + '\\' + file[file.find('/') + 1:]

        if file.endswith('.cpp'):
            s = '    <ClCompile Include="{}{}" />'.format(group, file)
        elif file.endswith('.h'):
            s = '    <ClInclude Include="{}{}" />'.format(group, file)
        else:
            continue
        exist = False
        for i, line in enumerate(lines):
            if line.find(s) != -1:
                exist = True
                break
        if exist:
            continue

        ingroup = False
        end_line = -1
        for i, line in enumerate(lines):
            if line == '  <ItemGroup>':
                ingroup = True
            if line == '  </ItemGroup>':
                ingroup = False
            if file.endswith('.cpp'):
                ss = '    <ClCompile Include="'
            elif file.endswith('.h'):
                ss = '    <ClInclude Include="'
            if ingroup and line.find(ss) != -1:
                end_line = i
        print 'include', file
        if file.endswith('.cpp'):
            s = '    <ClCompile Include="{}{}" />'.format(group, file)
        elif file.endswith('.h'):
            s = '    <ClInclude Include="{}{}" />'.format(group, file)
        lines.insert(end_line + 1, s)
    fileutils.write(project_file, '\n'.join(lines))


def exclude_files_from_vs_project(project_file):
    project_dir = project_file[0:project_file.rfind('/') + 1]
    project_dir.replace('/', '\\')

    ingroup = False
    end_line = -1
    lines = open(project_file).read().split('\n')
    buffer = []
    index = 0
    for line in lines:
        if line == '  <ItemGroup>':
            ingroup = True
        if line == '  </ItemGroup>':
            ingroup = False

        include = True
        if ingroup:
            l = line.strip()
            file = ''
            k = 0
            if l.find('<ClInclude Include="') == 0:
                k = len('<ClInclude Include="')
            if l.find('<ClCompile Include="') == 0:
                k = len('<ClCompile Include="')
            if k > 0:
                r = l.rfind('"')
                file = l[k:r]
                if not fileutils.isfile(project_dir + file):
                    print 'exclude', file
                    include = False
        if include:
            buffer.append(line)
    fileutils.write(project_file, '\n'.join(buffer))


def build_windows(root_directory):
    print '\tPrepare solutions...:'
    client = root_directory + 'client/proj.win32/project_mt_rts.vcxproj'
    exclude_files_from_vs_project(client)
    include_files_to_vs_project(root_directory + 'client/Classes/', '..\\Classes\\', client)
    include_files_to_vs_project(root_directory + '.gen/client/mg/', '..\\..\\.gen\\client\\mg\\', client)

    msBuild = 'C:/Program Files (x86)/MSBuild/14.0/Bin/MSBuild.exe'
    if not os.path.isfile(msBuild):
        print '14 tools not found. Try use 12'
        msBuild = 'C:/Program Files (x86)/MSBuild/12.0/Bin/MSBuild.exe'
    if not os.path.isfile(msBuild):
        print '12 tools not found. Try use 11'
        msBuild = 'C:/Program Files (x86)/MSBuild/11.0/Bin/MSBuild.exe'
    if not os.path.isfile(msBuild):
        print '11 tools not found. exit'
        exit(-1)

    print '\tBuild client:'
    cmd_client = '"{}" {}client/proj.win32/project_mt_rts.sln /p:Configuration=Release /p:Platform=win32 /m'.format(msBuild, ROOT_DIR)
    os.system(cmd_client)


def deploy_windows(root_directory):
    print '\tCopy binaries...:'
    client_src = root_directory + 'client/proj.win32/Release.win32/project_mt_rts.exe'
    client_dst = OUT_DIRECTORY + 'client/project_mt_rts.client.exe'
    copy_file(client_src, client_dst)

    def copyDirDlls(dir, dst_dir):
        files = fileutils.getFilesList(root_directory + dir)
        for file in files:
            if file.endswith('.dll'):
                src = root_directory + dir + file
                dst = OUT_DIRECTORY + dst_dir + file
                copy_file(src, dst)

    copyDirDlls('client/proj.win32/Release.win32/', 'client/')
    create_dir_for_file(root_directory + 'client/Resources/data/data.xml')
    write_file(OUT_DIRECTORY + 'run_phone.bat', 'cd client \nstart project_mt_rts.client.exe -width 1366 -height 768 -win_scale 1 \nexit')
    write_file(OUT_DIRECTORY + 'run_tablet.bat', 'cd client \nstart project_mt_rts.client.exe -width 1024 -height 768 -win_scale 1  \nexit')


def generate():
    print '\tGenerate...:'
    file = 'sublime/mlc_build.py'
    result = os.system('python {0}'.format(file))
    if result != 0:
        exit(-1)


def copy_mlc(root_directory):
    print '\tCopy mlc...:'
    files = fileutils.getFilesList(root_directory + 'mlc/src/')
    for file in files:
        if file.endswith('.pyc'):
            continue
        src = root_directory + 'mlc/src/' + file
        dst = OUT_DIRECTORY + 'mlc/src/' + file
        copy_file(src, dst)


def copy_configs(root_directory):
    print '\tCopy confings...:'
    files = fileutils.getFilesList(root_directory + 'configs/')
    for file in files:
        src = root_directory + 'configs/' + file
        dst = OUT_DIRECTORY + 'configs/' + file
        if file.endswith('.mlc') and ':storage' not in open(src).read():
            continue
        copy_file(src, dst)


def deploy(root_directory):
    PLATFORM_WINDOWS = 'Windows'

    update()
    generate()
    if platform.system() == PLATFORM_WINDOWS:
        build_windows(root_directory)
        deploy_windows(root_directory)
        pass
    copy_mlc(root_directory)
    copy_configs(root_directory)

if __name__ == '__main__':
    ROOT_DIR = '../'
    OUT_DIRECTORY = local_properties.read('deploy_directory').replace('\\', '/')
    if not OUT_DIRECTORY:
        OUT_DIRECTORY = ROOT_DIR + 'bin/'
    if OUT_DIRECTORY[-1] != '/':
        OUT_DIRECTORY += '/'
    print OUT_DIRECTORY

    if local_properties.get_bool('yandex_disk.upload'):
        yandex_disk_init()

    deploy(ROOT_DIR)
