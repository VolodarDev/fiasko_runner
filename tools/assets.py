# https://www.codeandweb.com/texturepacker/documentation
#
# TexturePacker [options] [<imagefolder>] [<images>] [<tpsfiles>]
# <imagefolder>
# Recursively adds all known image files from the folder to the sprite sheet
#
# <tpsfiles>
# Builds sprite sheets using parameters from a .tps file, additional options override settings from the .tps
#
# <images>
# Adds single images, supported image formats are: bmp, gif, ico, jpeg, jpg, mng, pbm, pgm, pkm, png, ppm, psd, pvr, pvr.ccz, pvr.gz, pvrtc, svg, svgz, swf, tga, tif, tiff, webp, xbm, xpm.
#
# Option                         Description
# --help                         Display help message
# --version                     Print version information
# --license-info                 Prints information about the currently installed license
# --activate-license <key>         Activate a license key
# --gui                         Launch TexturePacker with graphical user interface
# --force-publish                 Ignore smart update hash and force re-publishing of the files
# --replace <regexp>=<string>     Replaces matching parts of the sprites name with <string>; Uses full regular expressions, make sure to escape the expression
# --ignore-files <wildcard>     Ignores all images fitting the given pattern (may be used several times) You can use * and ?, make sure to escape the wildcards when working with bash. Please specify the complete path like /path/to/your/sprite.png or use a wildcard like *your/sprite.jpg
# --background-color <rrggbb>     Set solid background color, default is none, which is transparent The value is a tuple of 3 hexadecimal digit pairs, each pair represents a color channel in order red, green, blue, E.g. ff0000 for red, ffffff for white
# --verbose                     More output
# --quiet                         Display errors only

import os
import sys
import xml.etree.ElementTree as ET
import re

from py_lib import fileutils


def build_atlases(root, dst_folder):
    print('Start build atlasses')
    ATLASSES = root
    folders = fileutils.getFoldersList(ATLASSES)
    files = []
    for folder in folders:
        data = dst_folder + folder
        assets = ATLASSES + folder
        maxsize = 2048

        fileutils.createDirForFile(data)
        options = ''
        options += ' --format cocos2d'
        options += ' --algorithm MaxRects'
        options += ' --maxrects-heuristics Best'
        options += ' --max-width {0} --max-height {0}'.format(maxsize)
        options += ' --data {0}.plist --sheet {0}.png'.format(data)
        options += ' --prepend-folder-name'
        cmd = 'TexturePacker {0} {1}'.format(options, assets)
        result = os.system(cmd)
        if result != 0:
            exit(-1)
        files.append(data + '.plist')
        files.append(data + '.png')
    return files


def build_descriptor(dest_folder, files, out_file):
    buffer = ''
    total_size = 0
    for file in files:
        md5 = fileutils.getMd5File(file)
        size = fileutils.getSize(file)
        path = file[len(dest_folder):]
        buffer += '{};{};{}\n'.format(path, md5, size)
        total_size += size
    fileutils.write(out_file, buffer)
    print('All assets size: {}M'.format(round(total_size / (1024.0 * 1204.0), 2)))
    print('All assets, count = ', len(files))


def build_locales(src_folder, dst_file):
    # languages = ['en', 'ru']
    languages = ['ru', 'en']
    out_root = ET.Element('locales')
    locales = {}

    errors = []

    for lang in languages:
        directory = src_folder + lang + '/'
        files = fileutils.getFilesList(directory)
        locales[lang] = {}
        for file in files:
            if not file.endswith('.xml'):
                continue
            file = directory + file
            tree = ET.parse(file)
            root = tree.getroot()
            for node in root:
                key = node.attrib['key']
                value = node.text if node.text is not None else ''
                if not key:
                    errors.append('Error: Detected empty key in file {}'.
                                  format(file))
                if key in locales[lang]:
                    errors.append('Error: Detected dublicated locale \n[{}]=[{}]\n[{}]=[{}]'.
                                  format(key, locales[lang][key].encode('utf-8'), key, value.encode('utf-8')))
                locales[lang][key] = value

        for l_0 in locales:
            for l_1 in locales:
                if l_0 == l_1:
                    continue
                for key in locales[l_0]:
                    if key not in locales[l_1]:
                        errors.append('Error: locale [{}/{}] not found in scope {}'.
                                      format(l_0, key, l_1))

        locales_root = ET.SubElement(out_root, lang)
        for key in locales[lang]:
            node = ET.SubElement(locales_root, 'locale')
            node.attrib['key'] = key
            node.text = locales[lang][key]

    if errors:
        print( '========================================')
        for error in errors:
            print(error)
        print( '========================================')

    fileutils.createDirForFile(out_root)
    fileutils.save_xml(dst_file, out_root)
    return dst_file


def move_assets(src, dst, folder, exts=None):
    files = []
    root = src + folder
    exts = exts.split(';') if exts else []
    for file in fileutils.getFilesList(root):
        filename, file_extension = os.path.splitext(file)
        if not exts or file_extension in exts:
            files.append(dst + folder + file)
            fileutils.copy(root + file, dst + folder + file)
    return files


if __name__ == '__main__':
    build_assets('../', '../client/bin/assets/')
