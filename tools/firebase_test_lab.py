#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
import sys
import subprocess
import platform
from random import randint


# Push apk/aab to Firebase TestLab and run robo-test
# Used GCloud - https://cloud.google.com/sdk/install
# 
# curl https://sdk.cloud.google.com | bash
# gcloud auth login     
# gcloud firebase test android models list
# gcloud config set account ACCOUNT
# gcloud config set project PROJECT_ID

def get_app_info(app_gradle_path):
    data = open(app_gradle_path).read()
    min_sdk_version = re.findall(r'minSdkVersion = (\d+)', data)[0]
    print('  - Min SDK Version: ', min_sdk_version)
    return int(min_sdk_version)


def get_devices_list(app_info):
    arguments = [
        'gcloud',
        'firebase',
        'test',
        'android',
        'models',
        'list',
    ]
    ignore_lists = [
        'htc_ocmdugl',
        'htc_ocndugl',
        'sawfish',
    ]
    process = subprocess.Popen(arguments, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=(platform.system() == "Windows"))
    out, err = process.communicate()
    out = out.decode('utf-8')
    err = err.decode('utf-8')
    print('Return Code:', process.returncode)
    print('Out:\n', out)
    print('Err:\n', err)
    matches = re.findall(r'(\w+)(.+)PHYSICAL.+ \d+ x \d+.+\s([\d,]+)', out)
    devices = []
    for match in matches:
        device_id = match[0]
        try:
            if 'watch' in match[1].lower():
                print('Skip %s as watch' % device_id)
                continue
            sdks = [int(x) for x in match[2].split(',')]
            sdks = list(filter(lambda x: x >= app_info, sdks))
        except ValueError:
            continue
        if sdks and device_id not in ignore_lists:
            devices.append((device_id, sdks))
    return devices


def run_robo_tests(devices, path_to_apk):
    arguments = [
        'gcloud',
        'firebase',
        'test',
        'android',
        'run',
        '--type',
        'robo',
        '--app',
        path_to_apk,
    ]

    device_id = 'Any'
    os_sdk_id = 'Any'
    os_sdk_id

    if devices is not None:
        device = devices[randint(0, len(devices) - 1)]
        device_id = device[0]
        os_sdk_id = device[1][randint(0, len(device[1]) - 1)]
        os_sdk_id = str(os_sdk_id)

        arguments.extend([
            '--device-ids',
            device_id,
            '--os-version-ids',
            os_sdk_id,
        ])
    print('Run robo-test on device: %s OS: %s' % (device_id, os_sdk_id))

    process = subprocess.Popen(arguments, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=(platform.system() == "Windows"))
    out, err = process.communicate()
    out = out.decode('utf-8')
    err = err.decode('utf-8')
    print('Return Code:', process.returncode)
    print('Out:\n', out)
    print('Err:\n', err)

    if (process.returncode == 1 or process.returncode == 10) and devices is not None:
        return run_robo_tests(None, path_to_apk)

    return process.returncode


def main(apk_path):
    app_gradle_path = '../client/proj.android-studio/build.gradle'

    app_info = get_app_info(app_gradle_path)
    devices = get_devices_list(app_info)
    result = run_robo_tests(devices, apk_path)
    if result == 20:
        result = run_robo_tests(devices, apk_path)
    exit(result)


if __name__ == '__main__':
    sys.argv.extend(['-apk', '/Users/vladimirtolmachev/work/fiasko_runner/client/build/android/project_fiasko/outputs/apk/release/project_fiasko-release.apk'])
    apk = sys.argv[sys.argv.index('-apk') + 1]
    main(apk)
