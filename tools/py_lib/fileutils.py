import os
from os.path import isfile, isdir
from shutil import copyfile
import filecmp
import tempfile
import hashlib
import xml.etree.ElementTree as ET
import xml.dom.minidom


def createDirForFile(file):
    dir = file
    if '/' not in dir:
        return
    k = dir.rindex('/')
    dir = dir[:k]
    if not os.path.exists(dir):
        print('Create directory [{}]'.format(dir))
        os.makedirs(dir)


def _getFilesList(path, prefix):
    try:
        list = os.listdir(path)
        listFiles = []
        for i in list:
            if isdir(path + i):
                result = _getFilesList(path + i + "/", prefix + i + "/")
                for r in result:
                    listFiles.append(r)
            if isfile(path + i):
                listFiles.append(prefix + i)
        return listFiles
    except OSError as e:
        print( '============================')
        print( "Except: I/O error({0}): {1}".format(e.errno, e.strerror))
        print( 'FileUtils::_getFilesList({}, {})'.format(path, prefix))
        print( '============================')
        return []


def getFoldersList(path):
    try:
        list = os.listdir(path)
        foldersFiles = []
        for i in list:
            if isdir(path + i) and i[0] != '.':
                foldersFiles.append(i)
        return foldersFiles
    except OSError as e:
        print( '============================')
        print( "Except: I/O error({0}): {1}".format(e.errno, e.strerror))
        print( 'FileUtils::getFoldersList({})'.format(path))
        print( '============================')
        return []


def getFilesList(path):
    return _getFilesList(path, "")


def copy(src, dst):
    if not isfile(dst) or not filecmp.cmp(src, dst):
        print('copy ' + src)
        createDirForFile(dst)
        copyfile(src, dst)


def write(file, data):
    exist = isfile(file)
    log_title = 'update' if exist else 'create'
    if not exist or open(file).read() != data:
        print(log_title, file)
        open(file, 'w').write(data)


def loadDictFromFile(path):
    dict = {}
    try:
        file = open(path, "r")
        for line in file:
            str = line.strip()
            args = str.split(" ")
            if len(args) == 2:
                key = str.split(" ")[0]
                value = str.split(" ")[1]
                dict[key] = value
        return dict
    except:
        return dict
    return dict


def saveDictToFile(path, dict):
    try:
        _dict = loadDictFromFile(path)
        for key in dict:
            _dict[key] = dict[key]
        file = open(path, "w")
        for key in _dict:
            str = key + " " + _dict[key] + "\n"
            file.write(str)
    except:
        return False
    return True


def _getCacheFilePath():
    return tempfile.gettempdir() + "/build_data.txt"


def isFileChanges(file):
    dict = loadDictFromFile(_getCacheFilePath())
    if file in dict:
        cache = dict[file]

        m = hashlib.md5()
        m.update(open(file).read())
        return not cache == str(m.hexdigest())
    return True


def saveMd5ToCache(file):
    m = hashlib.md5()
    m.update(open(file).read())
    saveDictToFile(_getCacheFilePath(), {file: m.hexdigest()})


def getMd5File(file):
    m = hashlib.md5()
    m.update(open(file, 'rb').read())
    return str(m.hexdigest())


def getSize(file):
    return os.path.getsize(file)


def save_xml(file, root):
    buffer = ET.tostring(root)
    xml_ = xml.dom.minidom.parseString(buffer)
    buffer = xml_.toprettyxml(encoding='utf-8').decode('utf-8')
    lines = buffer.split('\n')
    buffer = ''
    for line in lines:
        if line.strip():
            buffer += line + '\n'
    write(file, buffer)
