import os
from os.path import isfile
import fileutils
import re

root = os.path.dirname(re.sub(r'\\', r'/', os.path.abspath(__file__)))
root = '/'.join(root.split('/')[0:-2])
local_properties_filepath = root + '/local.properties'


def append(lines, property, default_value):
    for line in lines:
        if line.find(property + ': ') == 0:
            return lines
    lines.append('{}: {}'.format(property, default_value))
    return lines


def read(property):
    create()
    if not fileutils.isfile(local_properties_filepath):
        return ''
    lines = open(local_properties_filepath).read().split('\n')
    for line in lines:
        k = line.find(property + ': ')
        if k == 0:
            return line[len(property + ': '):]
    return ''


def get_bool(property):
    value = read(property)
    if not value:
        return False
    return value[0].lower() in ['1', 'y', 't']


def create():
    if isfile(local_properties_filepath):
        lines = open(local_properties_filepath).read().split('\n')
    else:
        lines = []
    lines = append(lines, 'deploy_directory', 'bin')
    lines = append(lines, 'build.generate_confings', '1')
    lines = append(lines, 'build.generate_xml_types', '1')
    lines = append(lines, 'build.assets', '1')
    # lines = append(lines, 'build.locales', '1')
    lines = append(lines, 'build.extract_locales', '0')
    lines = append(lines, 'google_sheets_id_locales', '')
    lines = append(lines, 'yandex_disk.upload', '0')
    lines = append(lines, 'yandex_disk.deploy_directory', '')
    lines = append(lines, 'yandex_disk.login', 'mixgamesapp@yandex.ru')
    lines = append(lines, 'yandex_disk.password', 'QWEasd123')

    fileutils.write(local_properties_filepath, '\n'.join(lines))


if __name__ == '__main__':
    create()
