#!/bin/bash
set -e

#  usage 
display_usage() { 
	echo "Build app" 
	echo " - Usage: build.sh platform(ios, android) app_version" 
} 
if [  $# -le 1 ] 
then 
	display_usage
	exit 1
fi
if [[ ( $# == "--help") ||  $# == "-h" ]] 
then 
	display_usage
	exit 0
fi 

pushd . > /dev/null

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ROOT_DIR=$CURRENT_DIR/../
ASSETS_DIR=$ROOT_DIR/assets
BUILD_DIR_IOS=$ROOT_DIR/client/build_ios
PROJ_DIR_IOS=$ROOT_DIR/client/proj.ios_mac
PROJ_WORKSPACE=$ROOT_DIR/client/proj.ios_mac/project_fiasko.xcworkspace
CLIENT_DIR=$ROOT_DIR/client
PROJ_DIR_ANDROID=$ROOT_DIR/client/proj.android-studio
INFO_PLIST=$PROJ_DIR_IOS/ios/Info.plist
BUILD_VERSION=$2
APP_VERSION="1.0.$BUILD_VERSION"
BUILD_PLATFORM=$1

if [[ ( $3 == "publish") ]]; then PUBLISH=1; else PUBLISH=0; fi

if [ -d $ROOT_DIR/bin ]; then rm -r $ROOT_DIR/bin; fi
mkdir -p $ROOT_DIR/bin | > /dev/null

function pod_install() {
	pushd . > /dev/null
	cd $PROJ_DIR_IOS
	pod install
	popd > /dev/null
}

function change_version_ios(){
	/usr/libexec/Plistbuddy -c "Set CFBundleShortVersionString $APP_VERSION" $INFO_PLIST
	/usr/libexec/Plistbuddy -c "Set CFBundleVersion $BUILD_VERSION" $INFO_PLIST
}

function change_version_android(){
	sed -i '' 's/versionCode="[0-9.]*"/versionCode="'$BUILD_VERSION'"/' $PROJ_DIR_ANDROID/app/AndroidManifest.xml
	sed -i '' 's/versionName="[0-9.]*"/versionName="'$APP_VERSION'"/' $PROJ_DIR_ANDROID/app/AndroidManifest.xml
}

function build_mlc_configs() {
	python3 $ROOT_DIR/tools/sublime/mlc_build.py
}

function build_ios(){
	pushd . > /dev/null
	cd $PROJ_DIR_IOS
	xcodebuild -workspace $PROJ_WORKSPACE archive -archivePath $BUILD_DIR_IOS/fiasko.xcarchive -scheme fiasko-mobile -allowProvisioningUpdates
	popd > /dev/null
}

function export_ipa(){
	cat <<EOM > $BUILD_DIR_IOS/export.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>compileBitcode</key>
	<false/>
	<key>destination</key>
	<string>export</string>
	<key>method</key>
	<string>app-store</string>
	<key>signingStyle</key>
	<string>automatic</string>
	<key>stripSwiftSymbols</key>
	<true/>
	<key>teamID</key>
	<string>KF7UXPRLXA</string>
	<key>thinning</key>
	<string>&lt;none&gt;</string>
</dict>
</plist>
EOM
	xcodebuild -exportArchive -archivePath $BUILD_DIR_IOS/fiasko.xcarchive -exportPath $BUILD_DIR_IOS -exportOptionsPlist $BUILD_DIR_IOS/export.plist -allowProvisioningUpdates
}

function upload_ipa(){
	USER=tolm_vl@hotmail.com
	PASS=mbor-hpyc-izyg-axxm
	xcrun altool --upload-app -f $BUILD_DIR_IOS/fiasko-mobile.ipa -u "$USER" -p "$PASS" --verbose
}

function remove_dev_resources(){
	if [ -d $ASSETS_DIR/atlases ]; then rm -r $ASSETS_DIR/atlases; fi
}

function build_apk(){
	echo "Build apk..."
	pushd . > /dev/null
	cd $PROJ_DIR_ANDROID
	touch ../CMakeLists.txt
	./gradlew assembleRelease
	cp $ROOT_DIR/client/build/android/project_fiasko/outputs/apk/release/project_fiasko-release.apk $1 > /dev/null
	popd > /dev/null
}

function firebase_test_lab_run(){
	build_apk $OUT_APK_FIREBASE

	pushd . > /dev/null
	cd $CURRENT_DIR
	python3 firebase_test_lab.py -apk $OUT_APK_FIREBASE
	popd > /dev/null
}

function publish_apk(){
	cd $PROJ_DIR_ANDROID
	gradle publishApk
}

function build_android(){
	pushd . > /dev/null

	# Clean prev apk files
	cd $CLIENT_DIR
	OUT_APK=$ROOT_DIR/bin/fiasko-$APP_VERSION.apk
	OUT_APK_FIREBASE=$ROOT_DIR/bin/fiasko-firebase-testlab-$APP_VERSION.apk
	if [ -d $OUT_APK ]; then rm $OUT_APK; fi
	if [ -d $OUT_APK_FIREBASE ]; then rm $OUT_APK_FIREBASE; fi

	# Remove DEV preprocessor:
	sed 's/-DDEV=1//g' CMakeLists.txt > CMakeLists.txt.temp
	rm CMakeLists.txt
	mv CMakeLists.txt.temp CMakeLists.txt

	# Build firebase version
	firebase_test_lab_run

	build_apk $OUT_APK
	if [[ ( $PUBLISH == 1) ]];
	then
		# publish
		publish_apk
	fi

	popd > /dev/null
}

build_mlc_configs
remove_dev_resources
if [[ ( $BUILD_PLATFORM == "ios") ]]
then

	# remove and create build directory
	if [ -d $BUILD_DIR_IOS ]; then rm -r $BUILD_DIR_IOS; fi
	mkdir -p $BUILD_DIR_IOS

	pod_install
	change_version_ios
	build_ios
	export_ipa
	upload_ipa
fi
if [[ ( $BUILD_PLATFORM == "android") ]]
then
	change_version_android
	build_android
fi

cd $ROOT_DIR
git checkout assets/ 
echo 'Done'
popd > /dev/null