import os
import py_lib.fileutils as fs

if __name__ == '__main__':
    folders = fs.getFoldersList('./')
    for folder in folders:
        if folder == 'py_lib':
            continue
        for i, file in enumerate(fs.getFilesList('./' + folder + '/')):
            file = folder + '/' + file
            name = str(i + 1)
            if len(name) == 1:
                name = '0' + name
            os.rename(file, folder + '/' + name + '.png')

        if len(folder.split(' ')) != 2:
            continue
        name = folder.split(' ')[0]
        angle = folder.split(' ')[1]
        while len(angle) < 3:
            angle = '0' + angle
        if name == 'movement':
            os.rename(folder, 'move_' + angle)
        if name == 'shot':
            os.rename(folder, 'attack_' + angle)
        if name == 'wait':
            os.rename(folder, 'idle_' + angle)
