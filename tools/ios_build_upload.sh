pushd .
cd ../client/proj.ios_mac

APPLE_APP_ID=123123
APPLE_TEAM_ID=123123
USER=email
PASS=password
SCHEME=project_fiasko-mobile

pod install

#change build version
VERSION=$(/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "ios/Info.plist")
VERSION=$((VERSION+1))
/usr/libexec/Plistbuddy -c "Set CFBundleShortVersionString 1.0.$VERSION" ios/Info.plist
/usr/libexec/Plistbuddy -c "Set CFBundleVersion $VERSION" ios/Info.plist

# build archive
xcodebuild -workspace project_fiasko.xcworkspace archive -archivePath project_fiasko.xcarchive -scheme $SCHEME -allowProvisioningUpdates

create export.plist file
cat <<EOM > export.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
<key>teamID</key>
<string>$APPLE_TEAM_ID</string>
<key>method</key>
<string>app-store</string>
<key>uploadSymbols</key>
<true/>
</dict>
</plist>
EOM

xcodebuild -exportArchive -archivePath project_fiasko.xcarchive -exportPath ipa -exportOptionsPlist export.plist -allowProvisioningUpdates

#upload 
set -ex
IPA_FILE="ipa/project_fiasko.ipa"
IPA_FILENAME=$(basename $IPA_FILE)
MD5=$(md5 -q $IPA_FILE)
BYTESIZE=$(stat -f "%z" $IPA_FILE)
TEMPDIR=itsmp
# Remove previous temp
test -d ${TEMPDIR} && rm -rf ${TEMPDIR}
mkdir ${TEMPDIR}
mkdir ${TEMPDIR}/mybundle.itmsp
cp $IPA_FILE ${TEMPDIR}/mybundle.itmsp/$IPA_FILENAME
cat <<EOM > ${TEMPDIR}/mybundle.itmsp/metadata.xml
<?xml version="1.0" encoding="UTF-8"?>
<package version="software4.7" xmlns="http://apple.com/itunes/importer">
<software_assets apple_id="$APPLE_APP_ID">
<asset type="bundle">
<data_file>
<file_name>$IPA_FILENAME</file_name>
<checksum type="md5">$MD5</checksum>
<size>$BYTESIZE</size>
</data_file>
</asset>
</software_assets>
</package>
EOM
cp ${IPA_FILE} $TEMPDIR/mybundle.itsmp
/Applications/Xcode.app/Contents/Applications/Application\ Loader.app/Contents/itms/bin/iTMSTransporter -m upload -f ${TEMPDIR} -u "$USER" -p "$PASS" -v detailed

popd