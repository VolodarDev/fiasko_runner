

class Params(object):
    root = ''
    all_files = []
    xml_files = []
    ini_files = []
    resources_folder = ''
    out_folder = ''
    is_incremental = True
