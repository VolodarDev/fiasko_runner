import os
import sys

root = os.path.dirname(os.path.abspath(__file__).replace('\\', '/'))
root = '/'.join(root.split('/')[0:-2])

sys.path.insert(0, root + '/tools')
sys.path.insert(0, root + '/tools/py_lib')
sys.path.insert(0, root + '/tools/xml')
sys.path.insert(0, root + '/tools/google_drive')
sys.path.insert(0, root + '/.gen/py')

import locales
import assets
import resources_xml_parser
import local_properties

deploy_directory = '/bin/'


def generate_configs():
    file = root + '/mlc/src/main.py'
    if os.system('python {0} -o {1}/generated/mg -i {1}/configs -side client -f xml -data {1}/configs/data/ -data_out {2}/assets/data'.
                 format(file, root, root)) != 0:
        exit(-1)


def parse_xmls():
    resources_xml_parser.set(root, root + deploy_directory + 'assets/', root + '/generated/xml/', assets_dir=root + '/assets/')
    if not resources_xml_parser.validate():
        exit(-1)
    if not resources_xml_parser.generate_code():
        exit(-1)


def build_assets():
    dest = root + '/assets/'
    files = []
    files.append(dest + 'data/data.xml')
    files.append(dest + 'data/locales.xml')
    files.extend(assets.move_assets(root + '/assets/', dest, 'fonts/', '.ttf'))
    files.extend(assets.move_assets(root + '/assets/', dest, 'ini/', '.xml'))
    files.extend(assets.move_assets(root + '/assets/', dest, 'locales/', '.xml'))
    files.extend(assets.move_assets(root + '/assets/', dest, 'textures/', '.png;.jpg'))
    atlases = assets.build_atlases(root + '/assets/atlases/', root + '/assets/plists/')
    files.extend(atlases)
    locales.build(root + '/assets/locales/', root + '/generated/locale/')
    assets.build_locales(root + '/assets/locales/', dest + 'data/locales.xml')
    assets.build_descriptor(dest, files, dest + 'descriptor.txt')


if __name__ == '__main__':
    if local_properties.get_bool('build.extract_locales'):
        import extract_locales
        extract_locales.main(root)
    if local_properties.get_bool('build.generate_confings'):
        generate_configs()
    if local_properties.get_bool('build.assets'):
        build_assets()
    if local_properties.get_bool('build.generate_xml_types'):
        parse_xmls()
