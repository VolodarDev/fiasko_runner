from TestCase import TestCase
import xml.etree.ElementTree as ET
import urllib2
from Factory import Factory
from CommandSequence import CommandSequence

enable_logging = 0


class TestRequest(TestCase):
    SERVER_URL = ''

    def __init__(self, model=None):
        super(TestRequest, self).__init__()
        self.model = model
        self.response = None
        self.response_handler = None

    def _run(self):
        return False

    def getSerializedString(self, request):
        root = ET.Element(request.get_type())
        request.serialize(root)
        buffer = ET.tostring(root)
        return buffer

    def request(self, request, dispatch_response=True):
        try:
            message = self.getSerializedString(request)
            url = TestRequest.SERVER_URL.format(self.model.user_id) + message
            if enable_logging:
                print '      ' + url
            url = url.replace('\n', '%20')
            url = url.replace(' ', '%20')
            response_message = urllib2.urlopen(url).read()
            response_message = response_message.replace('<?xml version="1.0"?>\n', '').strip()
            if enable_logging:
                print '      ' + response_message
            if not response_message.startswith('exception'):
                self.response = Factory.create_command(response_message)
                if dispatch_response and self.response_handler:
                    if isinstance(self.response, CommandSequence):
                        for command in self.response.commands:
                            self.response_handler.visit(command)
                    else:
                        self.response_handler.visit(self.response)
                return self.response
            return None
        except urllib2.URLError as error:
            print '\t\tURLError:', error.reason
            return None
        # except ValueError as error:
        #     print '\t\tValueError:', error.message
        #     return None
        except Exception as error:
            print '\t\tException:', error.message
            return None
