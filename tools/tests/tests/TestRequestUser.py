from TestRequest import TestRequest
from RequestUser import RequestUser


class TestRequestUser(TestRequest):

    def __init__(self):
        super(TestRequestUser, self).__init__()

    def _run(self):
        request = RequestUser()
        request.user_id = self.model.user_id
        request.auth_key = self.model.auth_key
        response = self.request(request)
        result = response.get_type() == 'ResponseUser' and response.user
        self.add_result(result, 'Request user model')
        return True
