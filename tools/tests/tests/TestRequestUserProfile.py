from TestRequest import TestRequest
from RequestUserProfiles import RequestUserProfiles


class TestRequestUserProfile(TestRequest):

    def __init__(self):
        super(TestRequestUserProfile, self).__init__()

    def _run(self):
        request = RequestUserProfiles()
        request.user_id = self.model.user_id
        request.auth_key = self.model.auth_key
        request.ids = [self.model.user_id]
        if self.model.user_id > 1:
            request.ids.append(self.model.user_id - 1)

        response = self.request(request)
        result = response and response.get_type() == 'ResponseUserProfiles' and len(response.profiles) == len(request.ids) and \
            response.profiles[-1] and response.profiles[-1].id == self.model.user_id
        self.add_result(result, 'Request user profile')
        return True
