from TestRequest import TestRequest
from RequestRegistration import RequestRegistration


class TestRegistration(TestRequest):

    def __init__(self):
        super(TestRegistration, self).__init__()

    def _run(self):
        self._create_new()
        self._restore()

    def _create_new(self):
        request = RequestRegistration()
        request.device_id = 'auto_test_device_id'
        response = self.request(request)
        result = response.get_type() == 'ResponseRegistration' and response.user_id > 0 and len(response.auth_key) > 0
        self.add_result(result, 'Create new user [with id={}]'.format(response.user_id))
        return True

    def _restore(self):
        user_id = self.model.user_id
        request = RequestRegistration()
        request.device_id = 'auto_test_device_id'
        response = self.request(request)
        result = response.get_type() == 'ResponseRegistration' and response.user_id == user_id and len(response.auth_key) > 0
        self.add_result(result, 'Restore user by device_id [with id={}, device_id={}]'.format(response.user_id, request.device_id))
        return True
