
class TestCase(object):
    global_results = []

    def __init__(self):
        super(TestCase, self).__init__()
        self.results = []

    def __enter__(self):
        print 'Execute test {}:'.format(self.__class__)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        TestCase.print_results(self.results, True, '    Success steps: ')

    @staticmethod
    def print_results(results, details=False, logmessage=''):
        total = 0
        success = 0
        for result in results:
            total += 1
            message = ''
            if result[0]:
                success += 1
                message = '    Success: {}'
            else:
                message = '    Fail   : {}'
            if details:
                print message.format(result[1])
        print '{}{}/{}'.format(logmessage, success, total)

    def execute(self):
        self._run()
        return False

    def add_result(self, result, message=''):
        self.results.append([result, message])
        TestCase.global_results.append([result, message])

    def get_result(self):
        for result in self.results:
            if not result[0]:
                return False
        return True
