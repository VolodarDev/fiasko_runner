from IVisitorResponse import IVisitorResponse


class ResponseHandler(IVisitorResponse):

    def __init__(self, model=None):
        IVisitorResponse.__init__(self)
        self.model = model
        print self.model

    def visit_responseError(self, ctx):
        print 'Error:', ctx.error
        pass

    def visit_responseRegistration(self, ctx):
        self.model.user_id = ctx.user_id
        self.model.auth_key = ctx.auth_key
        pass

    def visit_responseUser(self, ctx):
        self.model.user = ctx.user
        pass
