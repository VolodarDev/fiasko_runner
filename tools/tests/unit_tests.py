import os
import sys

root = os.path.dirname(os.path.abspath(__file__).replace('\\', '/'))
root = os.path.dirname(os.path.dirname(root))
sys.path.insert(0, root + '/.gen/py')
sys.path.insert(0, root + '/tools/py_lib')
sys.path.insert(0, os.path.dirname(__file__) + '/tests')

from TestModel import TestModel
from TestCase import TestCase
from TestRequest import TestRequest
from ResponseHandler import ResponseHandler
from DataStorage import DataStorage
from TestRegistration import TestRegistration
from TestRequestUser import TestRequestUser
from TestRequestUserProfile import TestRequestUserProfile


class UnitTests:

    def initialize(self, endpoint=''):
        if not endpoint:
            endpoint = 'http://192.168.1.64:4222'
        TestRequest.SERVER_URL = endpoint + '/?request='

        DataStorage.shared().initialize(open('../../server/bin/deploy/assets/data/data.xml').read())
        self.model = TestModel()
        self.response_handler = ResponseHandler(self.model)
        return self.model

    def execute(self, TestClass, *args):
        with eval(TestClass)(*args) as test:
            test.model = self.model
            test.response_handler = self.response_handler
            test.execute()
        return test

    def wait_response_on_request(self, request, response):
        _request = eval(request)()
        _response = eval(response)()
        with TestRequest() as test:
            test.model = self.model
            test.response_handler = self.response_handler
            test.request(_request)
            result = _response.get_type() == test.response.get_type()
            test.add_result(result, 'wait_response_on_request: {} -> {}'.format(request, response))

    @staticmethod
    def print_results():
        TestCase.print_results(TestCase.global_results, details=False, logmessage='All tests: ')
