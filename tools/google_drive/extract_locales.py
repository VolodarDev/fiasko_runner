import sys
import xml.etree.ElementTree as ET

sys.path.insert(0, sys.argv[0][0:sys.argv[0].rfind('/') + 1] + '../py_lib')
import fileutils
import local_properties
from google_sheets import GoogleSheets


def parse(range, locales_dict):
    header = range[0]
    range = range[1:]

    locales = locales_dict
    for line in range:
        key = line[0]
        if not key:
            continue
        if key in locales:
            print 'Error: Detect dublicated keys: [{}]'.format(key)
            exit(-1)
        locales[key] = []
        for i, value in enumerate(line[1:]):
            if not value:
                print 'Warning: Detect empty locale: [{}][{}]=[{}]'.format(header[i + 1], key, value)
            locales[key].append(value)
    return locales, header


def parse_configs(assets_path):
    dictionary = {}
    locales_path = assets_path
    for file in fileutils.getFilesList(locales_path):
        if not file.endswith('.xml'):
            continue
        file = locales_path + file
        tree = ET.parse(file)
        root = tree.getroot()
        for node in root:
            key = node.attrib['key']
            dictionary[key] = file
    return dictionary


def write(assets_path, locales, key_in_files, language, header):
    index = header.index(language) - 1
    files = {}
    for key in key_in_files:
        files[key_in_files[key]] = None

    for file in files:
        files[file] = ET.parse(file).getroot()

    std_locales = {}
    std_locales['button_'] = assets_path + 'buttons.xml'
    std_locales['title_'] = assets_path + 'titles.xml'
    std_locales['message_'] = assets_path + 'messages.xml'
    std_locales['unit_'] = assets_path + 'units.xml'
    std_locales['building_'] = assets_path + 'building.xml'
    std_locales['technology_'] = assets_path + 'technologies.xml'
    std_locales['resource_'] = assets_path + 'resources.xml'
    std_locales['report_'] = assets_path + 'reports.xml'
    std_locales['process_'] = assets_path + 'processes.xml'
    std_locales['helper_'] = assets_path + 'helpers.xml'
    std_locales['parameter_'] = assets_path + 'parameters.xml'

    for key in sorted(locales.items(), key=lambda x: x[0]):
        key = key[0]

        def write_to_file(file):
            if file not in files:
                fileutils.createDirForFile(file)
                fileutils.write(file, '<locales>\n</locales>')
                files[file] = ET.parse(file).getroot()
            added = False
            for node in files[file]:
                if node.attrib['key'] == key:
                    node.attrib['value'] = locales[key][index]
                    added = True
            if not added:
                node = ET.SubElement(files[file], 'locale')
                node.attrib['key'] = key
                node.attrib['value'] = locales[key][index]

        if key in key_in_files:
            filepath = key_in_files[key]
            write_to_file(filepath)
        else:
            other_path = ''
            for std in std_locales:
                if key.find(std) == 0:
                    other_path = std_locales[std]
                    break
            if not other_path:
                other_path = assets_path + 'other.xml'
            write_to_file(other_path)

    for file in files:
        fileutils.save_xml(file, files[file])
    find_new_locales(locales, files)


def find_new_locales(locales, files):
    new = {}
    for path in files:
        for node in files[path]:
            key = node.attrib['key']
            value = node.attrib['value']
            if key not in locales:
                new[key] = value
    if new:
        print '============================================='
        print 'New locales:'
        print '============================================='
        for key in new:
            print '{}\t{}'.format(key, new[key].encode('utf-8'))
        print '============================================='


def write_language(root, language, locales, header):
    assets_path = root + '/assets/locales/{}/'.format(language)
    key_in_files = parse_configs(assets_path)
    write(assets_path, locales, key_in_files, language, header)


def main(root='../..'):
    print 'Start read a google spreedsheets with locales'
    gs = GoogleSheets(CLIENT_SECRET_FILE='../google_drive_client_secret.json')
    gs.set_document(local_properties.read('google_sheets_id_locales'))

    locales = {}
    titles = gs.get_sheet_titles()
    for title in titles:
        range = gs.read_range(title, 'A1', 'C')
        if range:
            locales, header = parse(range, locales)

    write_language(root, 'ru', locales, header)
    # write_language('en', locales, header)

    print '\tSuccess'


if __name__ == '__main__':
    main()
