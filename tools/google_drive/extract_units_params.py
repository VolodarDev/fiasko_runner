import sys
import xml.etree.ElementTree as ET
from google_sheets import GoogleSheets

sys.path.insert(0, sys.argv[0][0:sys.argv[0].rfind('/') + 1] + '../py_lib')
import fileutils

GOOGLE_SHEETS_ID = '1xzZlfSnegO_hsAv_cnZDib1U1KTvpwHOYSjjjY6q8I0'


def get_parse_header(range):
    return range[0], range[1:]


def convert_to_dictionary(header, range):
    result = {}
    for line in range:
        if not line or not line[0]:
            continue
        dict_ = {}
        for i, param in enumerate(header):
            if not param:
                continue
            dict_[param] = line[i]
        result[dict_['Unit:']] = dict_
    return result


def parse_google_doc():
    gs = GoogleSheets(CLIENT_SECRET_FILE='../google_drive_client_secret.json')
    gs.set_document(GOOGLE_SHEETS_ID)

    range = gs.read_range("Units", 'A1', 'Z')
    header, range = get_parse_header(range)
    params = convert_to_dictionary(header, range)
    return params


def convert_effeciency(value):
    if value.endswith('%'):
        value = value[0:-1]
        value = str(float(value) / 100)
        return value
    return value


def save(params):
    path = '../../configs/data/units.xml'
    tree = ET.parse(path)
    root = tree.getroot()
    for unitXml in root:
        name = unitXml.attrib['name']
        if name not in params:
            continue
        unit = params[name]
        unitXml.attrib['damage_atk'] = unit['Attack Damage']
        unitXml.attrib['damage_def'] = unit['Defense Damage']
        unitXml.attrib['health'] = unit['Health']
        unitXml.attrib['armor'] = unit['Armor']
        unitXml.attrib['velocity'] = unit['Velocity']
        while unitXml.find('effeciency'):
            unitXml.remove(unitXml.find('effeciency'))
        effeciency = ET.SubElement(unitXml, 'effeciency')
        ET.SubElement(effeciency, 'pair').attrib = {'key': 'guardian', 'value': convert_effeciency(unit['Effective vs guardian'])}
        ET.SubElement(effeciency, 'pair').attrib = {'key': 'archer', 'value': convert_effeciency(unit['Effective vs archer'])}
        ET.SubElement(effeciency, 'pair').attrib = {'key': 'knight', 'value': convert_effeciency(unit['Effective vs knight'])}

    fileutils.save_xml(path, root)


def main(root='../..'):
    params = parse_google_doc()
    save(params)


if __name__ == '__main__':
    main()
