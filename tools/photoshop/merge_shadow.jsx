﻿#target photoshop

app.bringToFront();
    
function ProcessDir(dir, out, depth) {
    if(depth <= 0){
        return
    }

    var folder = Folder(dir); 
    var outFolder = Folder(out + "/" + folder.name);

    var pngs = folder.getFiles("*.png");
    var psds = folder.getFiles("*.psd");
    for(var i in psds){
        shadow = app.open(psds[i]);
        shadow.activeLayer.copy();
        unit = app.open(pngs[i])
        
        app.activeDocument = unit;
        app.activeDocument.paste();
        app.activeDocument.activeLayer.blendMode = BlendMode.MULTIPLY
        app.activeDocument.activeLayer.opacity = opacity;
        app.activeDocument.activeLayer.moveToEnd (app.activeDocument);
        erase();
        HueSatLight(0, 0, -100);
        
        unit.resizeImage (UnitValue(size, "px"), UnitValue(size, "px"), null);
        
        if(!createFolder(outFolder)){
            return;
        }
        var file = new File(outFolder.fullName + "/" + unit.name);
        
        pngSaveOptions = new PNGSaveOptions();
        pngSaveOptions.interlaced = false;
        app.activeDocument.saveAs (file, pngSaveOptions, true, Extension.LOWERCASE);
        unit.close(SaveOptions.DONOTSAVECHANGES);
        shadow.close(SaveOptions.DONOTSAVECHANGES);
    }
    var folders = folder.getFiles();
    for (var i in folders){
        var file = folders[i];
        if(file instanceof Folder){
            ProcessDir(file, outFolder.fullName, depth-1);
        }
    }
}

function createFolder(outFolder){
    if (!outFolder.exists) {
        if (!outFolder.create()) {
            alert("Cannot create output folder");
            return false;
        }
    }
    return true;
}

function erase(){
    var id16821 = charIDToTypeID( "Fl  " );
    var desc3348 = new ActionDescriptor();
    var id16822 = charIDToTypeID( "From" );
    var desc3349 = new ActionDescriptor();
    var id16823 = charIDToTypeID( "Hrzn" );
    var id16824 = charIDToTypeID( "#Pxl" );
    desc3349.putUnitDouble( id16823, id16824, 0.0 ); //x
    var id16825 = charIDToTypeID( "Vrtc" );
    var id16826 = charIDToTypeID( "#Pxl" );
    desc3349.putUnitDouble( id16825, id16826, 0.0 ); //y
    var id16827 = charIDToTypeID( "Pnt " );
    desc3348.putObject( id16822, id16827, desc3349 );
    var id16828 = charIDToTypeID( "Tlrn" );
    desc3348.putInteger( id16828, 32 ); //tolerance
    var id16829 = charIDToTypeID( "AntA" ); //Antialias
    desc3348.putBoolean( id16829, true );
    var id16830 = charIDToTypeID( "Usng" );
    var id16831 = charIDToTypeID( "FlCn" );
    var id16832 = charIDToTypeID( "BckC" );
    desc3348.putEnumerated( id16830, id16831, id16832 );
    var id16833 = charIDToTypeID( "Md  " );
    var id16834 = charIDToTypeID( "BlnM" );
    var id16835 = charIDToTypeID( "Clar" );
    desc3348.putEnumerated( id16833, id16834, id16835 );
    //var id16872 = charIDToTypeID( "Cntg" );
    //desc3355.putBoolean( id16872, false ); Contiguous set to false
    executeAction( id16821, desc3348, DialogModes.NO );
}

function HueSatLight(Hue,Sat,Light) {  
    var desc9 = new ActionDescriptor();  
    desc9.putBoolean( charIDToTypeID('Clrz'), false );  
        var list2 = new ActionList();  
            var desc10 = new ActionDescriptor();  
            desc10.putInteger( charIDToTypeID('H   '), Hue );  
            desc10.putInteger( charIDToTypeID('Strt'), Sat );  
            desc10.putInteger( charIDToTypeID('Lght'), Light );  
        list2.putObject( charIDToTypeID('Hst2'), desc10 );  
    desc9.putList( charIDToTypeID('Adjs'), list2 );  
    executeAction( charIDToTypeID('HStr'), desc9, DialogModes.NO );  
}; 

var dir = Folder.selectDialog ("Please select folder");
var out = dir + "/frames"
var size = prompt("Unit size","","Input unit size");
var opacity = prompt("Shadow opacity","","Input shadow opacity [0..100]");

ProcessDir(dir, out, 5);
alert("Done");