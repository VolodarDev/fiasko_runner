import os
import sys
import re

from py_lib import fileutils


root = '../client/Classes/'
ignore_list = ['common.h']
search_paths = [root, '../.gen/client/', '../client/Classes/ml/']


def is_not_ignore(file):
    for ignore in ignore_list:
        if file.endswith(ignore):
            return False
    return True


def search(file, local_folder):
    for path in search_paths:
        if os.path.isfile(path + file):
            return path + file
        if os.path.isfile(path + local_folder + '/' + file):
            return path + local_folder + '/' + file
    return None


def extract_word(line, after, before):
    if after in line and before in line:
        k = line.find(after) + len(after)
        l = line.find(before)
        return line[k:l].strip()
    return ''


def find_declarations(file_, included_from_file):
    file = root + file_
    if not os.path.isfile(file):
        local = os.path.dirname(included_from_file)
        file = search(file_, local)
        if file is None:
            print 'not founded', file_, 'included from', included_from_file
            return []

    declarations = []
    text = open(file).readlines()
    for line in text:
        line = line.strip()
        name = ''
        if not name:
            name = extract_word(line, 'class', ':')
        if not name:
            name = extract_word(line, 'class', '{')
        if not name:
            name = extract_word(line + '\n', 'class', '\n')
        if not name:
            name = extract_word(line + '\n', 'namespace', '\n')
        if name and not name.endswith(';') and name not in declarations:
            ch = [',' '...', '<', '>']
            correct = True
            for c in ch:
                correct = c not in name
            if correct:
                declarations.append(name)
    return declarations


def main(file):
    lines = open(root + file).readlines()
    text = '\n'.join(lines)
    print file
    for line in lines:
        include = extract_word(line, '#include "', '.h"')
        if include and include not in ['cocos2d']:
            include += '.h'
            declarations = find_declarations(include, file)
            used = False
            for declaration in declarations:
                if declaration in text:
                    used = True
                    break
            if file.endswith(include[0:-2] + '.cpp'):
                used = True
            if not used and is_not_ignore(include):
                print '    Unused include:', include

    pass


if __name__ == '__main__':
    files = fileutils.getFilesList(root)
    for file in files:
        if file.endswith('.cpp'):
            main(file)
