import xml.etree.ElementTree as ET
from validate_action import validate_action
from params import Params as params


def get_root():
    return params.root


class Property(object):
    assets = []
    data_tree = None
    deploy_dir = ''

    def __init__(self):
        super(Property, self).__init__()
        self.name = ''
        self.message = None

    def is_value_valid(self, value):
        self.message = '    Need declare property "{}"'.format(self.name)
        return False

    def is_valid_link_to_data(self, value):
        if Property.data_tree is None:
            Property.data_tree = ET.parse(Property.deploy_dir + 'data/data.xml')
        data_root = Property.data_tree.getroot()
        value = value[len('data:'):]
        parts = value.split('/')
        if len(parts) < 3:
            self.message = '    Error validate link to data. see string:\t{}'.format(value)
            return False
        data_type = parts[0]
        data_name = parts[1]
        parts = parts[2:]
        node = data_root.find(data_type)
        if node is None or not len(node):
            self.message = '    Data [{}] is invalid:'.format(data_type)
            return False
        for data in node:
            if data.get('key') == data_name:
                data = data.find('value')
                for i, part in enumerate(parts):
                    is_attr = i == len(parts) - 1
                    if is_attr:
                        self.message = '    Data link to [{}/{}]: invalid property [{}]:'.format(data_type, data_name, '/'.join(parts))
                        return part in data.attrib
                    data = data.find(part)
                    if data is None:
                        self.message = '    Data link to [{}/{}]: invalid property [{}]. Data object have not a [{}] node:'. \
                            format(data_type, data_name, '/'.join(parts), part)
                        return False
                break
        self.message = '    Data [{}] not include object [{}]:'.format(data_type, data_name)
        return False


def FactoryProperties(name):
    properties = {
        'type': PType,
        'disableshadow': PEmpty,
        'disableoutline': PEmpty,
        'disableglow': PEmpty,
        'name': PName,
        'path': PName,
        'template': PTemplate,
        'text': PAnyText,
        'callback': PAnyText,
        'place_holder': PAnyText,
        'opacity': PNumber,
        'fontsize': PNumber,
        'rotation': PNumber,
        'x': PNumber,
        'y': PNumber,
        'z': PNumber,
        'globalzorder': PNumber,
        'tag': PNumber,
        'duration': PNumber,
        'textwidth': PNumber,
        'textheight': PNumber,
        'linespacing': PNumber,
        'percent':  PNumber,
        'scrollbarwidth':  PNumber,
        'size': PTwoNumber,
        'pos': PTwoNumber,
        'indicator_position': PTwoNumber,
        'inner_size': PTwoNumber,
        'inner_pos': PTwoNumber,
        'center': PTwoNumber,
        'textarea': PTwoNumber,
        'midpoint': PTwoNumber,
        'barchangerate': PTwoNumber,
        'rect': PRect,
        'scale_9': PRect,
        'scale': PScale,
        'image': PImage,
        'imageN': PImage,
        'imageS': PImage,
        'imageD': PImage,
        'indicator_image': PImage,
        'slider_bar': PImage,
        'slider_progress_bar': PImage,
        'slider_ballN': PImage,
        'slider_ballS': PImage,
        'slider_ballD': PImage,
        'font': PFont,
        'fontttf': PFont,
        'v_align': PVAlign,
        'textalign': PHAlign,
        'layout_type': PLayout,
        'direction': PDirection,
        'color': PColor3,
        'scrollbarcolor': PColor3,
        'scrollbarcolor4': PColor4,
        'textcolor': PColor3,
        'visible': PBoolean,
        'cascadecolor': PBoolean,
        'cascadeopacity': PBoolean,
        'wrap': PBoolean,
        'enabled': PBoolean,
        'scale_effect': PBoolean,
        'useblur': PBoolean,
        'clipping': PBoolean,
        'inertiascroll': PBoolean,
        'bounce': PBoolean,
        'scrollbarautohide': PBoolean,
        'hotlocalisation': PBoolean,
        'swallowtouches': PBoolean,
        'cursor_enabled': PBoolean,
        'indicator_enabled': PBoolean,
        'strech': PStrech,
        'blending': PBlending,
        'animation': PAction,
        'action': PAction,
        'enableshadow': PShadow,
        'enableoutline': POutline,
        'enableglow': PGlow,
        'progresstype': PProgressType,
        'resource': PResource,

        # user values:
        'visiblesize': PTwoNumber,
        'contentsize': PTwoNumber,
    }
    result = None
    if name in properties:
        result = properties[name]()
    else:
        result = Property()
    result.name = name
    return result


class PName(Property):

    def is_value_valid(self, value):
        return True


class PType(Property):

    known_types = None

    def __init__(self):
        super(PType, self).__init__()
        if PType.known_types is None:
            self._parse_types()

    def _parse_types(self):
        PType.known_types = []
        PType.known_types.append('node')
        PType.known_types.append('sprite')
        PType.known_types.append('layer')
        PType.known_types.append('scene')
        PType.known_types.append('progresstimer')
        PType.known_types.append('ui_text')
        PType.known_types.append('ui_button')
        PType.known_types.append('ui_layout')
        PType.known_types.append('ui_slider')
        PType.known_types.append('ui_image')
        PType.known_types.append('ui_scroll')
        PType.known_types.append('ui_textfield')
        PType.known_types.append('ui_pageview')
        PType.known_types.append('layerext')
        PType.known_types.append('nodeext')
        PType.known_types.append('spriteext')
        PType.known_types.append('scrolllayer')

        root = get_root()
        app = open(root + '/client/Classes/AppDelegate.cpp').read()
        k = 0
        while True:
            k = app.find('mlObjectFactory::shared().book', k)
            if k == -1:
                break
            l = app.find('"', k)
            r = app.find('"', l + 1)
            PType.known_types.append(app[l + 1:r])
            k = r

    def is_value_valid(self, value):
        return value in PType.known_types


class PNumber(Property):

    def is_float(self, string):
        try:
            if string.isdigit():
                return True
            rand_parts = string.split('..')
            if len(rand_parts) == 1:
                float(rand_parts[0])
                return True
            if len(rand_parts) == 2:
                float(rand_parts[0])
                float(rand_parts[1])
                return True
            float(string)
            return True
        except ValueError:
            return False

    def is_value_valid(self, value):
        return self.is_float(value)


class PTwoNumber(PNumber):

    def is_value_valid(self, value):
        for x in value.split(','):
            x = x.replace('frame:', '')
            x = x.replace('add:', '')
            parts = x.split('x')
            if len(parts) == 1:
                return False
            if len(parts) == 2:
                return self.is_float(parts[0]) and self.is_float(parts[1])


class PRect(PNumber):

    def is_value_valid(self, value):
        xx = value.split(',')
        if len(xx) != 2:
            return False
        for x in xx:
            x = x.replace('frame:', '')
            x = x.replace('add:', '')
            parts = x.split('x')
            if len(parts) == 1:
                return False
            if len(parts) == 2:
                return self.is_float(parts[0]) and self.is_float(parts[1])


class PScale(PNumber):

    def is_value_valid(self, value):
        for x in value.split(','):
            x = x.replace('frame:', '')
            x = x.replace('add:', '')
            parts = x.split('x')
            if len(parts) == 1:
                return self.is_float(parts[0])
            if len(parts) == 2:
                return self.is_float(parts[0]) and self.is_float(parts[1])


class PImage(PNumber):

    def is_value_valid(self, value):
        if value.startswith('data:'):
            return self.is_valid_link_to_data(value)
        return ('atlases/' + value in Property.assets) or ('textures/' + value in Property.assets) or value in Property.assets


class PTemplate(PNumber):

    def is_value_valid(self, value):
        return value in Property.assets and (value.startswith('ini/') or value.startswith('dev/'))


class PFont(PNumber):

    def is_value_valid(self, value):
        return value in Property.assets and value.startswith('fonts/')


class PVAlign(PNumber):

    def is_value_valid(self, value):
        return value in ['c', 't', 'b', 'center', 'top', 'bottom']


class PHAlign(PNumber):

    def is_value_valid(self, value):
        return value in ['center', 'left', 'right']


class PLayout(PNumber):

    def is_value_valid(self, value):
        return value in ['h', 'v', 'n', 'r', 'horizontal', 'vertical', 'absolute', 'relative']


class PDirection(PNumber):

    def is_value_valid(self, value):
        return value in ['v', 'h', 'b', 'vertical', 'horizontal', 'both']


class PBoolean(PNumber):

    def is_value_valid(self, value):
        return value in ['yes', 'no', 'true', 'false']


class PEmpty(PNumber):

    def is_value_valid(self, value):
        return value == ''


class PAnyText(PNumber):

    def is_value_valid(self, value):
        return True


class PColor3(PNumber):

    def is_value_valid(self, value):
        for v in value:
            if v.upper() not in '0123456789ABCDEF':
                return False
        return len(value) == 6


class PColor4(PNumber):

    def is_value_valid(self, value):
        for v in value:
            if v.upper() not in '0123456789ABCDEF':
                return False
        return len(value) == 8


class PStrech(Property):

    def is_value_valid(self, value):
        try:
            pc = None
            if '[' in value and ']' in value:
                pc = value[value.find('[') + 1: value.find(']')]
                value = value[0: value.find('[')]
            mode = value[value.rfind(':') + 1:]
            value = value[0: value.rfind(':')]
            result = True
            result = result and mode in ['x', 'y', 'xy', 'min', 'max']
            result = result and PTwoNumber().is_value_valid(value)
            if pc is not None:
                values = pc.split(',')
                for v in values:
                    l, r = v.split(':')
                    if l not in ['maxx', 'maxy', 'minx', 'miny', 'max', 'min']:
                        result = False
                    if not PTwoNumber().is_float(r):
                        result = False
            return result
        except:
            return False
        return False


class PBlending(Property):

    def is_value_valid(self, value):
        return value in [
            'additive',
            'disable',
            'alphapremultiplied',
            'alphanonpremultiplied'
        ]


class PAction(Property):

    def is_value_valid(self, value):
        return validate_action(value)


class PShadow(Property):

    def is_value_valid(self, value):
        if value == '':
            return True
        values = value.split(',')
        for v in values:
            l, r = value.split(':')
            if not self._check_property(l, r):
                return False
        return True

    def _check_property(self, property, value):
        if property == 'color':
            return PColor3().is_value_valid(value)
        elif property == 'offset':
            return PTwoNumber().is_value_valid(value)
        elif property == 'blurradius':
            return PNumber().is_value_valid(value)
        self.message = '    Unknow parameter "{}"'.format(property)
        return False


class POutline(PShadow):

    def _check_property(self, property, value):
        if property == 'color':
            return PColor3().is_value_valid(value)
        elif property == 'width':
            return PNumber().is_value_valid(value)
        self.message = '    Unknow parameter "{}"'.format(property)
        return False


class PGlow(PShadow):

    def is_value_valid(self, value):
        return value == ''


class PProgressType(Property):

    def is_value_valid(self, value):
        return value in ['radial', 'bar']


class PResource(Property):

    def is_value_valid(self, value):
        return value in Property.assets
