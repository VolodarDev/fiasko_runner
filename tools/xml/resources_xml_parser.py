import re
import validate_assets_ini_files
import fileutils
from params import Params


def set(root, deploy_dir, out_folder, is_incremental=False, assets_dir=''):
    Params.root = root
    Params.deploy_dir = deploy_dir
    Params.assets_dir = assets_dir
    Params.out_folder = out_folder
    Params.is_incremental = is_incremental

    if assets_dir:
        Params.all_files = fileutils.getFilesList(assets_dir)
    else:
        Params.all_files = fileutils.getFilesList(deploy_dir)

    for file in Params.all_files:
        if file.endswith('.xml'):
            Params.xml_files.append(file)
            if 'ini/' in file and file != 'ini/textures.xml':
                Params.ini_files.append(file)


def remove_variant(name, remove_ini=True):
    if remove_ini and name.find('ini/') == 0:
        name = name[len('ini/'):]
    return name


def create_header():
    pattern_h = '''#ifndef __generated_xml_types__
#define __generated_xml_types__
#include <string>

namespace xml
{1}
{0}{2}

#endif
'''
    pattern_c = '''#include "types.h"

namespace xml
{1}
{0}{2}
'''
    namespaces = {}
    values = {}

    for origin_file in Params.ini_files:
        upper = True
        file = origin_file
        file = remove_variant(file[:len(file) - 4])
        last = 0
        for i, ch in enumerate(file):
            if upper:
                file = file[0:i] + file[i].upper() + file[i + 1:]
                upper = False
                continue
            if ch in '/_':
                upper = True
            if ch == '/':
                last = i
        namespace = file[0:last]
        type = file[last + 1:].upper()
        namespace = re.sub('_', '', namespace)
        namespace = re.sub('/', '', namespace)
        if namespace:
            if namespace not in namespaces:
                namespaces[namespace] = []
            if namespace not in values:
                values[namespace] = {}
            if type not in values[namespace]:
                values[namespace][type] = remove_variant(origin_file, remove_ini=False)
                namespaces[namespace].append(type)

    H = ''
    C = ''
    for namespace in namespaces:
        buffer_h = '\tnamespace {} \n\t{}'.format(namespace[0].lower() + namespace[1:], '{')
        buffer_c = '\tnamespace {} \n\t{}'.format(namespace[0].lower() + namespace[1:], '{')
        for type in namespaces[namespace]:
            buffer_h += '\n\t\textern const std::string {};'.format(type)
            buffer_c += '\n\t\tconst std::string {}("{}");'.format(type, values[namespace][type])
        buffer_h += '\n\t}\n\n'
        buffer_c += '\n\t}\n\n'
        H += buffer_h
        C += buffer_c

    hpp = pattern_h.format(H, '{', '}')
    cpp = pattern_c.format(C, '{', '}')
    hpp_file = Params.out_folder + 'types.h'
    cpp_file = Params.out_folder + 'types.cpp'

    fileutils.write(hpp_file, hpp)
    fileutils.write(cpp_file, cpp)
    return namespaces, values


def create_loader(namespaces, values):
    pattern_h = '''#ifndef __generated_xml_loader_h__
#define __generated_xml_loader_h__
#include "cocos2d.h"
#include "ml/IntrusivePtr.h"
#include "ml/loadxml/xmlLoader.h"
#include "types.h"

namespace xml
{1}
{0}{2}

#endif'''
    pattern_c = '''#include "loader.h"

namespace xml
{1}
{0}{2}
'''
# namespace xml
# {
#     namespace create
#     {
#         namespace ScenesDialogs
#         {
#             intrusive_ptr<cocos2d::Node> PROCESS_TRAINER()
#         }
#     }
# }
    H = ''
    C = ''
    for namespace in namespaces:
        ns = namespace[0].lower() + namespace[1:]
        buffer_h = '\tnamespace {} \n\t{}'.format(ns, '{')
        buffer_c = '\tnamespace {} \n\t{}'.format(ns, '{')
        for type in namespaces[namespace]:
            buffer_h += '\n\t\tintrusive_ptr<cocos2d::Node> load_{}();'.format(type.lower())
            buffer_c += '''
        intrusive_ptr<cocos2d::Node> load_{0}()
        {3}
            return xmlLoader::load_node({2});
        {4}'''.format(type.lower(), ns, type, '{', '}')
            buffer_h += '''
        template <class T> intrusive_ptr<T> load_{0}()
        {3}
            return xmlLoader::load_node<T>({2});
        {4}'''.format(type.lower(), ns, type, '{', '}')

        buffer_h += '\n\t}\n\n'
        buffer_c += '\n\t}\n\n'
        H += buffer_h
        C += buffer_c

    hpp = pattern_h.format(H, '{', '}')
    cpp = pattern_c.format(C, '{', '}')
    hpp_file = Params.out_folder + 'loader.h'
    cpp_file = Params.out_folder + 'loader.cpp'

    fileutils.write(hpp_file, hpp)
    fileutils.write(cpp_file, cpp)
    return True


def generate_code():
    fileutils.createDirForFile(Params.out_folder + 'loader.h')
    namespaces, values = create_header()
    if not create_loader(namespaces, values):
        print('Error on create loaders for xml types')
        return False
    return True


def validate():
    if not validate_assets_ini_files.validate():
        print('Error validate xml configs')
        return False
    return True
