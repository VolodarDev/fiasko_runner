import fileutils
import os
import xml.etree.ElementTree as ET
from params import Params as params
from validate_action import validate_actions
from xml_properties import FactoryProperties
from xml_properties import Property


def find_image(path):
    if path in params.all_files:
        return True
    if 'atlases/' + path in params.all_files:
        return True
    if 'textures/' + path in params.all_files:
        return True
    return False


def find_xml(path):
    if path in params.all_files:
        return True
    if 'ini/' + path in params.all_files:
        return True
    return False


def findAsset(path):
    return find_image(path) or find_xml(path)


def log_deprecated_override_by_name(file):
    lines = open(params.assets_dir + file).read().split('\n')
    lines_invalid = []
    for i, line in enumerate(lines):
        line = line.strip()
        if i > 0 and line.startswith('<node ') and 'type="' not in line and 'template="' not in line and 'path="' not in line:
            lines_invalid.append(line)
    if len(lines_invalid) > 0:
        print("Unknow type/template/path:", file)
        for line in lines_invalid:
            print('\t' + line)


game_types = None


def validate_data():
    files_invalid = {}

    def add_error(filepath, message):
        if filepath not in files_invalid:
            files_invalid[filepath] = []
        files_invalid[filepath].append(message)

    def check_node(filepath, node):
        for attr in node.attrib:
            value = node.attrib[attr]
            if value.endswith('.png') or value.endswith('.jpg'):
                if not find_image(value):
                    add_error(filepath, 'Invalid link to image: {}="{}"'.format(attr, value))
            if value.endswith('.xml'):
                if not find_xml(value):
                    add_error(filepath, 'Invalid link to xml: {}="{}"'.format(attr, value))

        for child in node:
            check_node(filepath, child)

    directory = params.root + '/configs/data/'
    files = fileutils.getFilesList(directory)
    for file in files:
        if file.endswith('.DS_Store'):
            continue
        file = directory + file
        tree = ET.parse(file)
        check_node(file, tree.getroot())

    if len(files_invalid):
        print('Errors in data configs:')
        for file in files_invalid:
            print(file)
            for msg in files_invalid[file]:
                print('\t', msg)
    return len(files_invalid) == 0


# TODO: dont open file on every call 'validate_property_value'
# TODO: use xml parser

def validate_node_properties(xmlnode, filepath):
    if xmlnode.tag != 'node':
        return True
    result = True
    for attr in xmlnode.attrib:
        value = xmlnode.attrib[attr]
        if '@{' in value and '}' in value:
            # TODO: check all macros values
            continue
        prop = FactoryProperties(attr)
        if not prop.is_value_valid(value):
            message = prop.message
            if message is None:
                message = 'Error in file [{}]:\n    Invalid property value: {}="{}"'.format(filepath, attr, value)
            else:
                message = 'Error in file [{}]:\n'.format(filepath) + message
            print(message)
            result = False
    for child in xmlnode:
        result = validate_node_properties(child, filepath) and result
    return result


def validate():
    no_errors = True
    Property.assets = params.all_files
    Property.deploy_dir = params.deploy_dir
    for file in params.xml_files:
        if params.is_incremental:
            if not fileutils.isFileChanges(params.assets_dir + file):
                continue

        tree = ET.parse(params.assets_dir + file)
        root = tree.getroot()

        no_errors = validate_node_properties(root, file) and no_errors
        no_errors = validate_actions(root, file) and no_errors

        if params.is_incremental and no_errors:
            fileutils.saveMd5ToCache(params.assets_dir + file)

        log_deprecated_override_by_name(file)
    no_errors = validate_data() and no_errors
    return no_errors
