import xml.etree.ElementTree as ET
import fileutils


def build(locales_folder, out_folder):
    default_language = 'ru'
    directory = locales_folder + default_language + '/'
    consts = {}
    for file in fileutils.getFilesList(directory):
        if not file.endswith('.xml'):
            continue
        tree = ET.parse(directory + file)
        root = tree.getroot()
        for node in root:
            key = node.attrib['key']
            name = ''
            up = True
            for ch in key:
                if ch == '_':
                    up = True
                    continue
                name += ch.upper() if up else ch
                up = False
            consts[key] = name

    header = []
    defination = []
    initialize = []
    for key in consts:
        header.append('\textern std::string {};'.format(consts[key]))
        defination.append('\tstd::string {}("");'.format(consts[key]))
        initialize.append('\t\t{} = manager.locale("{}");'.format(consts[key], key))

    header = '\n'.join(sorted(header))
    defination = '\n'.join(sorted(defination))
    initialize = '\n'.join(sorted(initialize))

    fileutils.createDirForFile(out_folder)

    fileutils.write(out_folder + 'all.h', all_hpp.format(header, '{', '}'))
    fileutils.write(out_folder + 'all.cpp', all_cpp.format(defination, initialize, '{', '}'))
    fileutils.write(out_folder + 'initialize.h', initialize_hpp)
    fileutils.write(out_folder + 'initialize.cpp', initialize_cpp.format(initialize, '{', '}'))

all_hpp = '''#ifndef __generated_text_locales_h__
#define __generated_text_locales_h__
#include <string>
namespace locale
{1}
{0}
{2}
#endif'''
all_cpp = '''#include "all.h"
#include "ml/Localization.h"
namespace locale
{2}
{0}
{3}'''

initialize_hpp = '''#ifndef __generated_locale_initialize_h__
#define __generated_locale_initialize_h__
namespace locale
{
    void initialize();
}
#endif'''
initialize_cpp = '''#include "initialize.h"
#include "all.h"
#include "ml/Localization.h"
namespace locale
{1}
    void initialize()
    {1}
        auto& manager = Localization::shared();
{0}
    {2}
{2}'''
