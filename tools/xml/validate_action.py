from params import Params as params


def read_macroses(path):
    text = open(params.assets_dir + path).read()
    macroses = {}
    comments = []
    k = text.find('<!--')
    while k != -1:
        l = text.find('-->', k)
        if l == -1:
            l = len(text)
        comments.extend(text[k + 4:l].split('\n'))
        k = text.find('<!--', l + 3)

    for line in comments:
        line = line.strip()
        if not line:
            continue
        k = line.find('@{')
        l = line.find('}:')
        if k != -1 and l != -1 and l > k:
            name = line[k + 2:l]
            values = [x.strip() for x in line[l + 2:].strip().split(',')]
            macroses[name] = []
            macroses[name].extend(values)
    return macroses


def parse_macroses(line):
    k = line.find('@{')
    l = line.find('}')
    result = []
    while k != -1 and l != -1:
        result.append([line[k + 2:l], k, l + 1])
        k = line.find('@{', l + 1)
        l = line.find('}', l + 1)
    return result


def parse_variants(line):
    k = line.find('@(')
    l = line.find(')')
    result = []
    while k != -1 and l != -1:
        result.append([k, l + 1, line[k + 2:l].split('/')])
        k = line.find('@(', l + 1)
        l = line.find(')', l + 1)
    return result


def validate_animation(body):
    from validate_assets_ini_files import find_image
    # [0.75..0.85,[folder:unit_quardian/move_000/,indexes:.png,01:08]]
    folder = body[body.find('folder:') + len('folder:'): body.find(',', body.find('folder:'))]
    frames = body[body.find('indexes:') + len('indexes:'): body.find(']', body.find('indexes:'))].split(',')
    ext = frames[0]
    frames = frames[1].split(':')
    if len(frames) != 2:
        return False, '\tAnimate: cannot parse interval of frames'
    l = max(len(frames[0]), len(frames[1]))
    F = int(frames[0])
    T = int(frames[1])
    F, T = min(F, T), max(F, T)
    index = F
    result = True
    messages = []
    while index <= T:
        ind = str(index)
        while len(ind) < l:
            ind = '0' + ind
        frame = folder + ind + ext
        if find_image('atlases/' + frame) == False:
            result = False
            messages.append('\tSprite frame "{}" not found in "atlases" folder'.format(frame))
        index += 1

    return result, '\n'.join(messages)


def parse_action(string):
    # RepeatForever[Sequence[Animate[0.75..0.85,[folder:unit_quardian/move_000/,indexes:.png,01:08]]]
    l = string.find('[')
    r = string.rfind(']')

    name = string[0: l].strip()
    body = string[l + 1: r].strip()
    return name, body


def split_actions(string):
    actions = []
    counter = 0
    n = 0
    for i, ch in enumerate(string):
        if ch == '[':
            counter += 1
        elif ch == ']':
            counter -= 1
        if counter == 0 and ch == ',':
            actions.append(string[n:i])
            n = i + 1
    actions.append(string[n:])
    return actions


def _validate_action(name, body, filepath, macro_values={}):
    def is_float(string):
        try:
            if string.isdigit():
                return True
            rand_parts = string.split('..')
            if len(rand_parts) == 1:
                float(rand_parts[0])
                return True
            if len(rand_parts) == 2:
                float(rand_parts[0])
                float(rand_parts[1])
                return True
            float(string)
            return True
        except ValueError:
            return False

    macroses = parse_macroses(body)
    if macroses:
        macros = macroses[0]
        macro = macros[0]
        k = macros[1]
        l = macros[2]
        if macro in macro_values:
            values = macro_values[macro]
            result = True
            messages = []
            for value in values:
                body_changed = body[0:k] + value + body[l:]
                r, message = _validate_action(name, body_changed, filepath, macro_values)
                result = r and result
                if not r:
                    messages.append(message)
            return result, '\n'.join(messages)
        else:
            return True, '\tWarning: undefined macro values for macros "{}". Action: {}[{}]'.format(macro, name, body)
        return True, 'Ok'

    variants = parse_variants(body)
    for variant in variants:
        k = variant[0]
        l = variant[1]
        result = True
        messages = []
        for value in variant[2]:
            body_changed = body[0:k] + value + body[l:]
            r, message = _validate_action(name, body_changed, filepath, macroses)
            result = r and result
            if not r:
                messages.append(message)
        return result, '\n'.join(messages)

    recursive_actions_1 = ['RepeatForever', 'BounceIn', 'BounceOut',
                           'BounceInOut', 'BackIn', 'BackOut', 'BackInOut', 'SineIn', 'SineOut', 'SineInOut']
    recursive_actions_2 = ['EaseIn', 'EaseOut', 'EaseInOut', 'Repeat']

    actions_1 = ['DelayTime', 'FadeIn', 'FadeOut']
    actions_2 = ['RotateTo', 'RotateBy', 'Blink', 'FadeTo']
    actions_3 = ['ScaleTo', 'ScaleBy', 'SkewTo', 'SkewBy', 'MoveTo', 'MoveBy', 'JumpTo', 'JumpBy']
    actions_4 = ['TintTo', 'TintBy']
    actions_n = [actions_1, actions_2, actions_3, actions_4]

    if name in recursive_actions_1:
        name, body = parse_action(body)
        return _validate_action(name, body, filepath, macroses)
    if name in recursive_actions_2:
        args = split_actions(body)
        if len(args) != 2:
            return False, '\tAction "{}" have to have 2 arguments'.format(name)
        if not is_float(args[1]):
            return False, '\tAction "{}" have to have 2-th argument as number'.format(name)
        name, body = parse_action(body)
        return _validate_action(name, body, filepath, macroses)

    for n, actions in enumerate(actions_n):
        if name in actions:
            args = split_actions(body)
            if len(args) != n + 1:
                return False, '\tAction "{}" have to have {} arguments. args: ({})'.format(name, n + 1, args)
            if not is_float(args[n]):
                return False, '\tAction "{}" have to have {}-th argument as number. Current value: {}'.format(name, n + 1, args[n])
            return True, ''

    if name == 'Sequence' or name == 'Spawn':
        actions = split_actions(body)
        result = True
        messages = []
        if len(actions) == 1:
            print('{}\n\tOptimize: {} has only one action. It can be optimized'.format(filepath, name))
        for action in actions:
            name1, body1 = parse_action(action)
            if name1 == 'RepeatForever':
                return False, '\t{} cannot has {} action'.format(name, name1)
            r, m = _validate_action(name1, body1, filepath, macroses)
            result = r and result
            if m:
                messages.append(m)
        return result, '\n'.join(messages)

    if name == 'Animate':
        return validate_animation(body)
    if name in ['Hide', 'Show', 'Enable', 'Disable']:
        return True, ''

    return False, '\tUnknow action "{}"'.format(name)


def validate_action(value, macro_values={}):
    name, body = parse_action(value)
    return _validate_action(name, body, '', macro_values)


def validate_actions(root, filepath):
    macro_values = read_macroses(filepath)
    result = True
    log_path = True
    for child in root:
        if child.tag == 'actions':
            for action_xml in child:
                action = action_xml.attrib['value']
                name, body = parse_action(action)
                r, message = _validate_action(name, body, filepath, macro_values)
                result = r and result
                if (not r) or message:
                    if log_path:
                        print('{}. Error validate action in file'.format(filepath))
                        log_path = False
                    if message:
                        print(message)
    return result


if __name__ == '__main__':
    action = '''RepeatForever[
            Sequence[Animate[1..1.5,[folder:unit_quardian/@{side}/idle_045/,indexes:.png,01:15]],
            DelayTime[8.5..15.8],
            Animate[1..1.5,[folder:unit_quardian/@{side}/idle_045/,indexes:.png,01:151]]]]"
            '''
    print(validate_action(action, {'side': ['red', 'blue']}))
