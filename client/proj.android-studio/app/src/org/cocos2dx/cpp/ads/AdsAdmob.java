package org.cocos2dx.cpp.ads;

import com.fungames.fiasko.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


import org.cocos2dx.cpp.AppActivity;

public class AdsAdmob {
    private static InterstitialAd mInterstitialAd;

    public static void init() {
        AppActivity.instance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MobileAds.initialize(AppActivity.instance, new OnInitializationCompleteListener() {
                    @Override
                    public void onInitializationComplete(InitializationStatus initializationStatus) {
                        AdsAdmob.loadInterstitial();
                    }
                });
                loadInterstitial();
            }
        });
    }

    public static void loadInterstitial() {
        AppActivity.instance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                    return;
                }
                String unitId = AppActivity.instance.getString(R.string.admob_interstitial);
                mInterstitialAd = new InterstitialAd(AppActivity.instance);
                mInterstitialAd.setAdUnitId(unitId);
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                    }
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                    }
                    @Override
                    public void onAdOpened() {
                    }
                    @Override
                    public void onAdClosed() {
                        loadInterstitial();
                    }
                });
            }
        });
    }

    public static void showInterstial() {
        AppActivity.instance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }
            }
        });
    }
}
