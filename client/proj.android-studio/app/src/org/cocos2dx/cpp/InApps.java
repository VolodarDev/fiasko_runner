/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.anjlab.android.iab.v3.*;

import java.util.List;
import java.util.UUID;


public class InApps implements BillingProcessor.IBillingHandler {
	private static final String LOG_TAG = "id_inapp";

	private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4x5K0CTT//CN91hTeFzzNjq4dGQA8z7QA2U0O96SDGaEua4DlsqdkzbDXcuaEtcRwPSvj0d9XTGYxDvRTav4heiOD1bfhwvFT9trxtc870NJGm6vZuV3x2Jw8kobItmNQUcWYov+P5dz4oUKWppE7F8TPkoUsct/+wzsZ0vbyxjHFunCJrx3wQ5YGmoOj2Q2rn1AZGaYjUnXXWgLfc4501Rwn8438xUBJk24Mkzjo9eSsck+hY6PemcGf6eyf8+ebB2jNZFz+7Ar8/0sVFNPg7RMgvB5X7ZQRwirBUkDTypi1ZoaKNkaCfsvRF/T1X6Kr6SNkamptrMTFSKzzxn0kwIDAQAB";

	private static BillingProcessor billingProcessor;
	private boolean readyToPurchase = false;
	private static AppActivity app;
	static InApps me;

	protected void onCreate(AppActivity application) {
		me = this;
		app = application;

		billingProcessor = new BillingProcessor(app, LICENSE_KEY, this);

	}

	static public void getNonConsumedProducts() {
		billingProcessor.loadOwnedPurchasesFromGoogle();
	}

	public void onDestroy() {
		if (billingProcessor != null)
			billingProcessor.release();
	}

	static String parce(String string) {
		String result = string.replace(',', (char) 0x1);
		return result;
	}

	static public void requestDetails(String productID) {
		try {
			SkuDetails details = billingProcessor.getPurchaseListingDetails(productID);
			if (details != null) {
				nativeResultDetails(true, details.productId, details.title, details.description, details.priceText, details.currency, details.priceValue.floatValue());
			} else {
				nativeResultDetails(false, "", "", "", "", "", 0.f);
			}
		} catch (Exception e) {
			nativeResultDetails(false, "", "", "", "", "", 0.f);
		}
	}

	static public void purchase(String producTD) {
		try {
			billingProcessor.purchase(app, producTD);
		} catch (Exception e) {
		}
	}

	static public void consume(String producTD) {
		try {
			billingProcessor.consumePurchase(producTD);
		} catch (Exception e) {
		}
	}


	// IBillingHandler implementation

	@Override
	public void onBillingInitialized() {
		try {
			getNonConsumedProducts();
			Log.d(LOG_TAG, "onBillingInitialized");
		} catch (Exception e) {
		}
	}

	@Override
	public void onProductPurchased(String productId, TransactionDetails details) {
		try {
			boolean isValid = billingProcessor.isValidTransactionDetails(details);
			if (details != null && isValid) {
				nativeResultPurchase(details.purchaseInfo.purchaseData.productId, 0, "", true);
			} else {
				nativeResultPurchase(details.purchaseInfo.purchaseData.productId, 1, "not valid purchase", false);
			}
		} catch (Exception e) {
			nativeResultPurchase(details.purchaseInfo.purchaseData.productId, 1, "Exception on validate purchase. Message: " + e.getMessage(), false);
		}
	}

	@Override
	public void onBillingError(int errorCode, Throwable error) {
		nativeResultPurchase("", errorCode, error.getMessage(), false);
	}

	@Override
	public void onPurchaseHistoryRestored() {
		try {
			Log.d(LOG_TAG, "onPurchaseHistoryRestored");
		} catch (Exception e) {
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		billingProcessor.handleActivityResult(requestCode, resultCode, data);
	}

	private boolean restoreNonConsumed(String productId) {
//		try {
//			String purchasePayload = Constants.PRODUCT_TYPE_MANAGED + ":" + productId;
//			if (!Constants.PRODUCT_TYPE_MANAGED.equals(Constants.PRODUCT_TYPE_SUBSCRIPTION)) {
//				purchasePayload += ":" + UUID.randomUUID().toString();
//			}
//
//			Bundle bundle;
//			bundle = billingProcessor.billingService.getBuyIntent(Constants.GOOGLE_API_VERSION, billingProcessor.contextPackageName, productId, Constants.PRODUCT_TYPE_MANAGED, purchasePayload);
//
//			if (bundle != null) {
//				int response = bundle.getInt(Constants.RESPONSE_CODE);
//				boolean result = response == Constants.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED;
//				return result;
//			}
//			return true;
//		} catch (Exception e) {
//			Log.d(LOG_TAG, e.getMessage());
//			return false;
//		}
		return false;
	}

	public static boolean isPurchased(String productId) {
		return me.restoreNonConsumed(productId);
	}

	private native static void nativeResultDetails(boolean result, String productId, String title, String description, String priceText, String currency, float priceValue);
	private native static void nativeResultPurchase(String jproductId, int errorCode, String jerrorMessage, boolean purchaseSuccess);
}
