package org.cocos2dx.cpp;

import org.cocos2dx.cpp.ads.AdsAdmob;
import org.cocos2dx.lib.Cocos2dxActivity;

import android.content.Context;

import android.content.Intent;
import android.os.Bundle;
import android.net.Uri;

public class AppActivity extends Cocos2dxActivity
{
	private static final String LOG_TAG = "Fiasko";

	public static AppActivity instance;
	private static InApps inapp;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		instance = this;
    	try
    	{
        	inapp = new InApps();
        	inapp.onCreate(this);
			AdsAdmob.init();
	    } catch(Exception e) {
		}
    }

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
	}
	

	@Override
    public void onDestroy() {
		try{
			if( inapp != null ) inapp.onDestroy();
	        super.onDestroy();
		} catch(Exception e) { }
    }
	
	@Override
	protected void onPause() {
		try{
		    super.onPause();
		} catch(Exception e) { }
	}
	
	@Override
	protected void onResume() {
		try{
		    super.onResume();
		} catch(Exception e) { }
	}

	@Override
	protected void onStart()
	{
		try{
			super.onStart();
		} catch(Exception e) { }
	}

	@Override
	protected void onStop()
	{
		try{
			super.onStop();
		} catch(Exception e) { }
	}
	
	@Override
    protected void onNewIntent(Intent intent)
    {
		try{
	        super.onNewIntent(intent);
		} catch(Exception e) { }
    }
  
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try{
			if( inapp != null ) inapp.onActivityResult(requestCode, resultCode, data);
		} catch(Exception e) { }
    }
	
	public static void openUrl(String URL)
	{
		try{
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(URL));
			instance.startActivity(i);
		} catch(Exception e) { }
	}
	
	public static void openStoreUrl()
	{
		String packagename = instance.getPackageName();
		String url = "market://details?id=" + packagename;
		openUrl( url );
	}
}
