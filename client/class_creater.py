import sys

name = raw_input()
name = name.strip()
if not name:
    exit(-1)

parent = 'parent'
args = name.split(' ')
if len(args) > 1:
    name = args[0]
    parent = args[1]

parents = {}
parents['layer'] = 'LayerExt'
parents['node'] = 'NodeExt_'
parents['menu'] = 'MenuExt'
parents['scene'] = 'BaseScene'
parents['window'] = 'BaseWindow'
parents['widget'] = 'BaseWidget'

p_include = {}
p_include['layer'] = 'ml/NodeExt.h'
p_include['node'] = 'ml/NodeExt.h'
p_include['menu'] = 'ml/NodeExt.h'
p_include['BaseScene'] = 'scenes/BaseScene.h'
p_include['BaseWindow'] = 'scenes/BaseWindow.h'
p_include['BaseWidget'] = 'scenes/BaseWidget.h'
p_include['LayerExt'] = 'ml/NodeExt.h'
p_include['NodeExt_'] = 'ml/NodeExt.h'
p_include['scene'] = 'scenes/BaseScene.h'
p_include['window'] = 'scenes/BaseWindow.h'
p_include['widget'] = 'scenes/BaseWidget.h'

if parent.lower() in parents:
    parent = parents[parent.lower()]

parent_include = p_include[parent] if parent in p_include else parent + '.h'

args = {}
args['BaseWindow'] = 'const std::shared_ptr<BaseController>& controller'
# args['BaseWidget'] = 'BaseController* controller'

call_args = {}
call_args['BaseController* controller'] = 'controller'

arg = args[parent] if parent in args else ''
call_arg = call_args[arg] if arg in call_args else ''

h = '''/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __{0}_h__
#define __{0}_h__

#include "cocos2d.h"
#include "{4}"

class {0} : public {3}
{1}
public:
	DECLARE_AUTOBUILDER({0});
	virtual bool init({5}) override;
public:
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name) override;
	virtual void onLoaded() override;
protected:

private:

{2};

#endif
'''.format( name, "{", "}", parent, parent_include, arg)

cpp = '''/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "{0}.h"

using namespace cocos2d;

{0}::{0}()
{1}
{2}

{0}::~{0}()
{1}
{2}

bool {0}::init({4})
{1}
	if (!{3}::init({5}))
		return false;

	return true;
{2}

ccMenuCallback {0}::get_callback_by_description(const std::string & name)
{1}
	if (name == "") return [this](Ref*){1}
	{2};
	return {3}::get_callback_by_description(name);
{2}

void {0}::onLoaded()
{1}
	{3}::onLoaded();
{2}
'''.format( name, "{", "}", parent, arg, call_arg)


open(name + ".h", "w").write(h)
open(name + ".cpp", "w").write(cpp)
