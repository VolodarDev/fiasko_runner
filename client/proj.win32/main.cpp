#if TESTS != 1

#include "main.h"
#include "cocos2d.h"
#include "AppDelegate.h"
#include "support/AppArguments.h"
#include "ml/common.h"


USING_NS_CC;



int WINAPI _tWinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow)
{
	char buf[MAX_LOG_LENGTH];
	WideCharToMultiByte(CP_ACP, 0, lpCmdLine, -1, buf, sizeof(buf), nullptr, FALSE);
	std::vector<std::string> attrs;
	split(attrs, std::string(buf), ' ');

	AppArguments::inst.parse(nCmdShow, attrs);

	AppDelegate app;
	return Application::getInstance()->run();
}

#endif