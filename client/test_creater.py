class_name = raw_input()
class_name = class_name.strip()
if not class_name:
    exit(-1)


hpp = '''#if TESTS==1
#   ifndef ____class___h__
#   define ____class___h__
#include "TestBase.h"


class __class__ : public TestBase
{
    DEFINE(__class__)
public:
    virtual void execute()override;
};


#   endif
#endif
'''.replace('__class__', class_name)

cpp = '''#if TESTS==1
#include "__class__.h"


IMPLEMENT(__class__)

void __class__::execute()
{
}


#endif
'''.replace('__class__', class_name)

open('tests/' + class_name + ".h", "w").write(hpp)
open('tests/' + class_name + ".cpp", "w").write(cpp)


main = open('tests/main_tests.cpp').read().split('\n')
line_include = -1
line_run = -1

for index, line in enumerate(main):
    if line.startswith('#include'):
        line_include = index
    if line.find('().run();') > 0:
        line_run = index

main.insert(line_include + 1, '#include "{}.h"'.format(class_name))
main.insert(line_run + 2, '\t{}().run();'.format(class_name))

open('tests/main_tests.cpp', 'w').write('\n'.join(main))
