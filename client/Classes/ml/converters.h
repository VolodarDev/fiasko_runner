#ifndef __ml_CONVERTERS_h__
#define __ml_CONVERTERS_h__
#include <string>
#include "cocos2d.h"

bool strToBool(const std::string &value);
std::string boolToStr(bool value);

int strToInt(const std::string &value);
std::string intToStr(int value);

float strToFloat(const std::string &value);
std::string floatToStr(float value);
std::string floatToStr2(float value);

cocos2d::Point strToPoint(const std::string & value);
const std::string pointToStr(const cocos2d::Point & point);
const std::string pointToStrRound(const cocos2d::Point & point);

cocos2d::Size strToSize(const std::string & value);
const std::string sizeToStr(const cocos2d::Size & size);

cocos2d::Rect strToRect(const std::string & value);
const std::string rectToStr(const cocos2d::Rect & rect);

cocos2d::Color3B strToColor3B(const std::string & value);
std::string color3BToStr(const cocos2d::Color3B& value);
cocos2d::Color4B strToColor4B(const std::string & value);
std::string color4BToStr(const cocos2d::Color4B& value);

cocos2d::BlendFunc strToBlendFunc(const std::string & value);
std::string blendFuncToStr(const cocos2d::BlendFunc & blendFunc);

cocos2d::EventKeyboard::KeyCode strToKeyCode(const std::string & value);

#endif
