//
//  Crypt.hpp
//  dungeon
//
//  Created by Vladimir Tolmachev on 15.11.2019.
//

#ifndef Crypt_hpp
#define Crypt_hpp

#include <string>

namespace crypt
{
    void encode(std::string& data);
    void decode(std::string& data);


    std::string base64encode(const std::string& in);
    std::string base64decode(const std::string& in);

#ifdef TESTS
    namespace tests
    {
        bool run();
    }
#endif
}

#endif /* Crypt_hpp */
