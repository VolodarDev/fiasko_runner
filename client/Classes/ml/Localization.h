/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __ml_Localization__
#define __ml_Localization__

#include "Observer.h"
#include "Singlton.h"
#include "cocos2d.h"
#include "pugixml/pugixml.hpp"
#include <string>
#include <memory>
#include <map>

#ifdef LOCALE
#	undef LOCALE
#endif
#define LOCALE(id) Localization::shared().locale(id)

class Localization : public Singlton<Localization>
{
	typedef std::map<std::string, std::string> Pack;
public:
	Observer<std::function<void()> > eventChangeLanguage;
public:
	static bool isLife();
	Localization();
	~Localization();
	virtual void onCreate()override;

	void selectLanguage(const std::string& defaultLang);
	std::string locale( const std::string& id, int depth = 10 );
	void set( const std::string& language );
	std::string getCurrentLocalization()const;
protected:
	void load();
	
private:
	Pack _pack;
	std::string _current;
};

#endif /* defined(__JungleDefense__Localization__) */
