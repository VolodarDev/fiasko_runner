/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "cocos2d.h"
#include "Localization.h"
#include "Observer.h"
#include "loadxml/xmlLoader.h"
#include "UserData.h"

using namespace cocos2d;

const std::string DefaulLocalization("en");

static bool Localization_isLIfe( false );
bool Localization::isLife()
{
	return Localization_isLIfe;
}

Localization::Localization()
{
	Localization_isLIfe = true;
};

Localization::~Localization()
{
	Localization_isLIfe = false;
};

void Localization::onCreate()
{
	_pack.clear();
	_current.clear();
}

std::string Localization::locale( const std::string & id, int depth )
{
	if(depth <= 0)
		return id;
	std::string temp = id;
	std::string result;
	while(true)
	{
		size_t k0 = temp.find("#");
		size_t k1 = k0 != std::string::npos ? temp.substr(k0 + 1).find("#") : std::string::npos;
		if(k0 != std::string::npos && k1 != std::string::npos)
		{
			std::string key = temp.substr(k0+1, k1);
			auto value = locale(key, depth - 1);
			result += temp.substr(0,k0) + value;
			temp = temp.substr(k0+k1+2);
		}
		else
		{
			auto it = _pack.find(id);
			if(it != _pack.end())
			{
				result = xmlLoader::macros::parse(it->second);
				result = locale(result, depth - 1);
			}
			else
			{
				result += temp;
			}
			break;
		}
	};
	return result;
}

void Localization::set(const std::string& lang)
{
	_current = lang;
	load();

	UserData::getInstance()->lang_set( lang );
	xmlLoader::macros::set( "LOCALE", lang );
	xmlLoader::macros::set( "LOCALE_DIR", lang + "/" );
	eventChangeLanguage.pushevent();
}

std::string Localization::getCurrentLocalization()const
{
	return _current;
}

void Localization::load()
{
	pugi::xml_document doc;
	doc.load_file( "data/locales.xml" );
	auto root = doc.root().first_child();
	
	if(_current.empty())
	{
		_current = DefaulLocalization;
	}
	auto pack = root.child(_current.c_str());
	for(auto node : pack)
	{
		auto key = node.attribute("key").as_string();
		auto value = node.text().as_string();
		_pack[key] = value;
	}
}

void Localization::selectLanguage(const std::string& defaultLang)
{
	assert( UserData::getInstance() );
	std::string userlang = UserData::getInstance()->lang_get();
	if( userlang.empty() )
	{
		auto systemlang = Application::getInstance()->getCurrentLanguage();
		switch( systemlang )
		{
			case LanguageType::ENGLISH: set( "en" ); break;
			case LanguageType::RUSSIAN: set( "ru" ); break;

			default: set( defaultLang );
		}
	}
	else
	{
		set( userlang );
	}
	
}
