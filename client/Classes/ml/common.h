/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __Common_h__
#define __Common_h__
#include "ml/Generics.h"
#include "ml/converters.h"
#include "ml/macroses.h"
#include "ui/CocosGUI.h"

#define FOR_EACHXML(_node, _child) for(auto _child = _node.first_child(); _child; _child = _child.next_sibling())
#define FOR_EACHXML_BYTAG(_node, _child, _tag) for(auto _child = _node.child(_tag); _child; _child = _child.next_sibling(_tag)) 

#define sizearray(arr) sizeof(route) / sizeof (route[0])


float getAngle(const cocos2d::Point & a, const cocos2d::Point & b);

cocos2d::Point getVectorByDirection(float direction);
float getDirectionByVector(const cocos2d::Point & radius);
float lerpDegrees(float start, float end, float amount);

float getDistance(const cocos2d::Point & point, const cocos2d::Point & A, const cocos2d::Point & B);

float distanse_pointToLineSegment(
	const cocos2d::Point & segmentA,
	const cocos2d::Point & segmentB,
	const cocos2d::Point & point);

cocos2d::FiniteTimeAction * createRouteAction(const std::vector<cocos2d::Point> route, float objectSpeed);

bool checkPointInNode(const cocos2d::Node * node, const cocos2d::Point & pointInParentSpace, int depth = -1);

template <class STLcont>
std::string unite(const STLcont & cont, const char delimiter = ',')
{
	std::string result;
	for(const auto& value : cont)
	{
		result += value;
		result += delimiter;
	}
	if(result.empty() == false)
		result.pop_back();
	return result;
}

void split(std::list<std::string> & out, const std::string & values, const char delimiter = ',');
void split(std::vector<std::string> & out, const std::string & values, const char delimiter = ',');

template <typename T>
void split_t(std::vector<T> & out, const std::string & values, const char delimiter = ',')
{
    std::vector<std::string> ss;
    split(ss, values, delimiter);
    for(auto& s : ss)
        out.push_back(strTo<T>(s ));
}

template <typename T>
std::vector<T> split_t(const std::string & values, const char delimiter = ',')
{
    std::vector<T> out;
    std::vector<std::string> ss;
    split(ss, values, delimiter);
    for(auto& s : ss)
        out.push_back(strTo<T>(s ));
    return std::move(out);
}

template <typename T>
std::string join_t(const std::vector<T>& in, const char delimiter = ',')
{
	std::string result;
	for(auto& t : in)
	{
		result += toStr(t);
		result.push_back(delimiter);
	}
	if(result.empty() == false)
		result.pop_back();
	return result;
}


struct Stretch
{
	Stretch();
	enum class Mode
	{
		min_scale,
		max_scale,
		only_x,
		only_y,
		both_xy,
		unknow,
	};
	Mode mode;
	cocos2d::Size boundingSize;
	float maxScaleX;
	float maxScaleY;
	float minScaleX;
	float minScaleY;

	bool empty()const;
};
Stretch::Mode strToStretchMode(const std::string& mode);
Stretch strToStretch(const std::string& string);

void stretchNode(cocos2d::Node*node, const Stretch& stretch);

void fileLog(const std::string & s);

cocos2d::Point getRandPointInPlace(const cocos2d::Point & center, float radius);

std::string m_image_directory(const std::string & source, int posFromEnd, unsigned randomDiapason);
void computePointsByRadius(std::vector<cocos2d::Point> & out, float radius, unsigned countPoints, float startAngleInDegree = 0);

cocos2d::Node * getNodeByTagsPath(cocos2d::Node * root, const std::list<int> & tagspath);
cocos2d::Node * getNodeByPath(cocos2d::Node * root, const std::string & path_names);

bool isFileExist(const std::string& path);

template <class T>
T * getNodeByTagsPath(cocos2d::Node * root, const std::list<int> & tagspath)
{
	auto node = getNodeByTagsPath(root, tagspath);
	return dynamic_cast<T*>(node);
}

template <class T>
T * getNodeByPath(cocos2d::Node * root, const std::string & path_names)
{
	auto node = getNodeByPath(root, path_names);
	return dynamic_cast<T*>(node);
}

template <class T>
void connectExtension(cocos2d::Node* node, T** extension)
{
    if(extension)
    {
        intrusive_ptr<T> pointer;
        connectExtension<T>(node, pointer);
        *extension = pointer.ptr();
    }
}

template <class T>
void connectExtension(cocos2d::Node* node, intrusive_ptr<T>& extension)
{
    if (!node)
    {
        return;
    }
    auto ptr = dynamic_cast<T*>(node);
    if (ptr)
    {
        extension = ptr;
        return;
    }
    for (auto child : node->getChildren())
    {
        connectExtension(child, extension);
        if (extension)
        {
            return;
        }
    }
}


template <class T>
void connectExtension(cocos2d::Node* node, const std::string& name, T** extension)
{
	if(extension)
	{
		intrusive_ptr<T> pointer;
		connectExtension<T>(node, name, pointer);
		*extension = pointer.ptr();
	}
}

template <class T>
void connectExtension(cocos2d::Node* node, const std::string& name, intrusive_ptr<T>& extension)
{
	if (!node)
	{
		return;
	}
	if (node->getName() == name)
	{
		auto ptr = dynamic_cast<T*>(node);
		if (ptr)
		{
			extension = ptr;
			return;
		}
	}
	for (auto child : node->getChildren())
	{
		connectExtension(child, name, extension);
		if (extension)
		{
			return;
		}
	}
}

template <class T>
T* findNodeWithName(cocos2d::Node* root, const std::string& name)
{
    if (!root)
    {
        return nullptr;
    }
    if (root->getName() == name)
    {
        auto ptr = dynamic_cast<T*>(root);
        if (ptr)
        {
            return ptr;
        }
    }
    for (auto child : root->getChildren())
    {
        if (child->getName() == name)
        {
            auto ptr = dynamic_cast<T*>(child);
            if (ptr)
            {
                return ptr;
            }
        }
    }
    for (auto child : root->getChildren())
    {
        auto node = findNodeWithName<T>(child, name);
        if (node)
        {
            return node;
        }
    }
    return nullptr;
}

#endif
