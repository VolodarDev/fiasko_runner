/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __mlObjectFactory_h__
#define __mlObjectFactory_h__
#include "ml/Singlton.h"
#include "ml/macroses.h"


class mlObjectFactory : public Singlton<mlObjectFactory>
{
private:
	friend class Singlton<mlObjectFactory>;
	mlObjectFactory();

	class IObject : public cocos2d::Ref {
	public: virtual IntrusivePtr<cocos2d::Ref> build()=0;
	};

	template<class T>
	class Object : public IObject {
	public: virtual IntrusivePtr<cocos2d::Ref> build() { return IntrusivePtr<cocos2d::Ref>(T::create()); };
	};

	template<class T> 
	class ObjectPointer : public IObject {
	public: virtual IntrusivePtr<cocos2d::Ref> build() { return IntrusivePtr<cocos2d::Ref>(T::create(nullptr )); };
	};


	std::map< std::string, IntrusivePtr<IObject> > m_objects;

public:
	template <class CLASS>
	void book(const std::string & key, bool over=false)
	{
		assert(over || m_objects.find(key) == m_objects.end());
		auto ptr = IntrusivePtr< Object<CLASS> >(new Object<CLASS>);
		m_objects[key] = ptr;
	};

	template <class CLASS>
	void book_pointer(const std::string & key, bool over=false)
	{
		assert(over || m_objects.find(key) == m_objects.end());
		auto ptr = IntrusivePtr< ObjectPointer<CLASS> >(new ObjectPointer<CLASS>);
		m_objects[key] = ptr;
	};

	IntrusivePtr<cocos2d::Ref> build(const std::string & key);
	
	template < class CLASS >
	IntrusivePtr<CLASS> build(const std::string & key)
	{
		IntrusivePtr<cocos2d::Ref> ref = build(key);
		IntrusivePtr<CLASS> result(dynamic_cast<CLASS*>(ref.ptr()));
		return result;
	};

};

#endif
