//
//  Actions.cpp
//  cocos2d
//
//  Created by Vladimir Tolmachev on 05/01/2020.
//

#include "ml/Actions.h"
#include "ml/Generics.h"
#include "ml/loadxml/xmlProperties.h"
#include "ml/Localization.h"
#include "ui/CocosGUI.h"


#pragma mark -
#pragma mark ActionText

ActionText::ActionText()
{
}

ActionText::~ActionText()
{
}

bool ActionText::init(float duration, float endValue, bool floatTruncation, const std::string& prefix, const std::string& postfix)
{
    if (ActionInterval::initWithDuration(duration))
    {
        _prefix = prefix;
        _postfix = postfix;
        _floatTruncation = floatTruncation;
        _endValue = endValue;
        return true;
    }
    return false;
}
void ActionText::startWithTarget(cocos2d::Node* target)
{
    ActionInterval::startWithTarget(target);
    auto label = dynamic_cast<cocos2d::ui::Text*>(target);
    assert(label);
    _startValue = strTo<float>(label->getString());
}

void ActionText::update(float t)
{
    auto label = dynamic_cast<cocos2d::ui::Text*>(getTarget());
    assert(label);
    
    float value = _startValue + (_endValue - _startValue) * t;
    if (_floatTruncation)
    {
        label->setString(_prefix + toStr(int(value)) + _postfix);
    }
    else
    {
        label->setString(_prefix + toStr(value) + _postfix);
    }
}

cocos2d::ActionInterval* ActionText::reverse() const
{
    auto action = ActionText::create(getDuration(), _startValue, _floatTruncation, _prefix, _postfix);
    action->retain();
    action->autorelease();
    return action;
};

cocos2d::ActionInterval* ActionText::clone() const
{
    auto action = ActionText::create(getDuration(), _endValue, _floatTruncation, _prefix, _postfix);
    action->retain();
    action->autorelease();
    return action;
}


#pragma mark -
#pragma mark ActionEnable

ActionEnable* ActionEnable::create()
{
    ActionEnable* ret = new (std::nothrow) ActionEnable();

    if(ret)
    {
        ret->autorelease();
    }

    return ret;
}

void ActionEnable::update(float time)
{
    CC_UNUSED_PARAM(time);
    xmlLoader::setProperty(_target, xmlLoader::kEnabled, toStr(true));
}

cocos2d::ActionInstant* ActionEnable::reverse() const
{
    return ActionDisable::create();
}

cocos2d::ActionInstant* ActionEnable::clone() const
{
    // no copy constructor
    auto a = new (std::nothrow) ActionEnable();
    a->autorelease();
    return a;
}


#pragma mark -
#pragma mark ActionDisable

ActionDisable* ActionDisable::create()
{
    ActionDisable* ret = new (std::nothrow) ActionDisable();

    if(ret)
    {
        ret->autorelease();
    }

    return ret;
}

void ActionDisable::update(float time)
{
    CC_UNUSED_PARAM(time);
    xmlLoader::setProperty(_target, xmlLoader::kEnabled, toStr(false));
}

cocos2d::ActionInstant* ActionDisable::reverse() const
{
    return ActionDisable::create();
}

cocos2d::ActionInstant* ActionDisable::clone() const
{
    auto a = new (std::nothrow) ActionDisable();
    a->autorelease();
    return a;
}


#pragma mark -
#pragma mark ActionSetProperty

ActionSetProperty* ActionSetProperty::create(const std::string& propertyName, const std::string& propertyValue)
{
    ActionSetProperty* ret = new (std::nothrow) ActionSetProperty();

    if(ret)
    {
        ret->autorelease();
    }
    ret->_property = propertyName;
    ret->_value = propertyValue;

    return ret;
}

void ActionSetProperty::update(float time)
{
    CC_UNUSED_PARAM(time);
    xmlLoader::setProperty(_target, _property, _value);
}

cocos2d::ActionInstant* ActionSetProperty::reverse() const
{
    return ActionSetProperty::create(_property, _value);
}

cocos2d::ActionInstant* ActionSetProperty::clone() const
{
    return ActionSetProperty::create(_property, _value);
}


#pragma mark -
#pragma mark ActionType

ActionType::ActionType() {}
ActionType::~ActionType()
{
}
 
ActionType* ActionType::create(float duration, const std::string& text)
{
    auto action = new ActionType;
    if(action && action->init(duration, text))
    {
        action->autorelease();
        return action;
    }
    CC_SAFE_DELETE(action);
    return nullptr;
}

cocos2d::ActionInterval* ActionType::reverse() const
{
    return create(_duration, _sourceText);
}

cocos2d::ActionInterval* ActionType::clone() const
{
    return create(_duration, _sourceText);
}

void ActionType::startWithTarget(cocos2d::Node* target)
{
    _label.reset(dynamic_cast<cocos2d::Label*>(target));
    _text.reset(dynamic_cast<cocos2d::ui::Text*>(target));
    
    auto utf8 = Localization::shared().locale(_sourceText);
    std::u16string utf16String;
    if(cocos2d::StringUtils::UTF8ToUTF16(utf8, utf16String))
    {
        _localizedText = utf16String;
    }
}

void ActionType::update(float time)
{
    updateString(time);
    if(time == 1.f)
    {
        _label.reset(nullptr);
        _text.reset(nullptr);
    }
}

bool ActionType::init(float duration, const std::string& text)
{
    ActionInterval::initWithDuration(duration);
    _sourceText = text;
    return true;
}

void ActionType::updateString(float time)
{
    int chars = floor(_localizedText.size() * time);
    auto str = _localizedText.substr(0, chars);
    std::string utf8;
    if(cocos2d::StringUtils::UTF16ToUTF8(str, utf8))
    {
        if(_label)
            _label->setString(utf8);
        else if(_text)
            _text->setString(utf8);
    }
}


