//
//  AudioEngine.cpp
//  TowerDefence
//
//  Created by Vladimir Tolmachev on 04.02.14.
//
//

#include "ml/AudioEngine.h"
#include "audio/include/AudioEngine.h"
#include "ml/UserData.h"

using CocosAudioEngine = cocos2d::experimental::AudioEngine;
using AudioState = cocos2d::experimental::AudioEngine::AudioState;

void runTest();

AudioEngine::AudioEngine()
: _audioEnabled(true)
, _backgroundMusicID(CocosAudioEngine::INVALID_AUDIO_ID)
, _pauseCounter(0)
{}

void AudioEngine::onCreate()
{
    _audioEnabled = UserData::shared().is_audio_enabled();
    runTest();
}

AudioEngine::~AudioEngine()
{
    
}

void AudioEngine::enableAudio()
{
    _audioEnabled = true;
    UserData::shared().set_audio_enable(_audioEnabled);
    
    for(auto id : _effectIDs)
    {
        CocosAudioEngine::setVolume(id, 1.f);
    }
}

void AudioEngine::disableAudio()
{
    _audioEnabled = false;
    UserData::shared().set_audio_enable(_audioEnabled);

    for(auto id : _effectIDs)
    {
        CocosAudioEngine::setVolume(id, 0.f);
    }
}

bool AudioEngine::isAudioEnabled()
{
    return _audioEnabled;
}

void AudioEngine::playMusic(const std::string & path)
{
	std::string pathmusic = xmlLoader::macros::parse(path);
	if(pathmusic == _currentMusic)
		return;
	_currentMusic = pathmusic;
	
    if(cocos2d::FileUtils::getInstance()->isFileExist(pathmusic))
	{
		pathmusic = cocos2d::FileUtils::getInstance()->fullPathForFilename(pathmusic);
		float volume = _audioEnabled ? 0.5f : 0.f;
		if (_backgroundMusicID != CocosAudioEngine::INVALID_AUDIO_ID)
			CocosAudioEngine::stop(_backgroundMusicID);
		_backgroundMusicID = CocosAudioEngine::play2d(pathmusic, true, volume);
        _effectIDs.push_back(_backgroundMusicID);

		CocosAudioEngine::setFinishCallback(_backgroundMusicID, [this](int, std::string) {
			_backgroundMusicID = CocosAudioEngine::INVALID_AUDIO_ID;
            _effectIDs.remove(_backgroundMusicID);
		});
	}
}

int AudioEngine::playEffect(const std::string & path, bool lopped)
{
    if(_pauseCounter)
        return -1;
	std::string soundPath = xmlLoader::macros::parse(path);
    soundPath = cocos2d::FileUtils::getInstance()->fullPathForFilename(soundPath);

    auto volume = _audioEnabled ? 1 : 0.f;
    int id = CocosAudioEngine::play2d(soundPath, lopped, volume);
    if (id == CocosAudioEngine::INVALID_AUDIO_ID)
    {
        return id;
    }

    _effectIDs.push_back(id);
    CocosAudioEngine::setFinishCallback(id, [this](int id, std::string const & soundPath) {
        _effectIDs.remove(id);
    });

    return id;
}


void AudioEngine::stopEffect(int id)
{
	CocosAudioEngine::stop(id);
    if(std::find(_effectIDs.begin(), _effectIDs.end(), id) != _effectIDs.end())
        _effectIDs.remove(id);
}

void AudioEngine::pauseEffect(int id)
{
	CocosAudioEngine::pause(id);
}

void AudioEngine::resumeEffect(int id)
{
	CocosAudioEngine::resume(id);
}

void AudioEngine::stopMusic()
{
    if (_backgroundMusicID != CocosAudioEngine::INVALID_AUDIO_ID)
	    CocosAudioEngine::stop(_backgroundMusicID);
    if(std::find(_effectIDs.begin(), _effectIDs.end(), _backgroundMusicID) != _effectIDs.end())
        _effectIDs.remove(_backgroundMusicID);
	_backgroundMusicID = CocosAudioEngine::INVALID_AUDIO_ID;
    _currentMusic.clear();
}

void AudioEngine::stopAll()
{
    auto ids = _effectIDs;
    for(auto id : ids)
	{
		stopEffect(id);
	}
}

void AudioEngine::pauseAll()
{
	++_pauseCounter;
    if(_pauseCounter == 1)
    {
        CocosAudioEngine::pauseAll();
    }
}

void AudioEngine::resumeAll()
{
    if(_pauseCounter == 1)
    {
        CocosAudioEngine::resumeAll();
    }
	_pauseCounter = std::max(0, _pauseCounter-1);
}

/*
 *  class TestAudioEngine
 */
//MARK: -
//MARK: TestAudioEngine
#if COCOS2D_DEBUG > 0
class TestAudioEngine
{
public:
    bool run_all()
    {
        auto& audio = AudioEngine::shared();
        bool enabled = audio.isAudioEnabled();

        enableAudio();
        disableAudio();
        isAudioEnabled();
        playMusic();
        playEffect();
        stopEffect();
        pauseEffect();
        resumeEffect();
        stopMusic();
        stopAll();
        pauseAll();
        resumeAll();
        
        assert(audio._pauseCounter == 0);
        if(!enabled)
            audio.disableAudio();
        return true;
    }
    
    void enableAudio()
    {
        auto& audio = AudioEngine::shared();
        audio.enableAudio();
        assert(audio.isAudioEnabled());
        
        auto id = audio.playEffect("dev/button.mp3");
        assert(id != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(CocosAudioEngine::getVolume(id) > 0);
        
        audio.enableAudio();
        assert(CocosAudioEngine::getVolume(id) > 0);
        audio.stopEffect(id);
    }
    void disableAudio()
    {
        auto& audio = AudioEngine::shared();
        audio.enableAudio();
        auto id = audio.playEffect("dev/button.mp3");
        assert(id != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(CocosAudioEngine::getVolume(id) > 0);
        
        audio.disableAudio();
        assert(CocosAudioEngine::getVolume(id) == 0);
        assert(!audio.isAudioEnabled());
        
        audio.enableAudio();
        assert(CocosAudioEngine::getVolume(id) > 0);
        assert(audio.isAudioEnabled());
        audio.stopEffect(id);
    }
    void isAudioEnabled()
    {
        // tested in enableAudio
    }
    void playMusic()
    {
        auto& audio = AudioEngine::shared();
        audio.playMusic("dev/button.mp3");
        auto& id = audio._backgroundMusicID;
        assert(id != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(CocosAudioEngine::getVolume(id) > 0);
        assert(CocosAudioEngine::isLoop(id));
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id) != audio._effectIDs.end());
        assert(audio._currentMusic == "dev/button.mp3");

        audio.stopMusic();
        assert(id == CocosAudioEngine::INVALID_AUDIO_ID);
        assert(audio._currentMusic.empty());
    }
    void playEffect()
    {
        auto& audio = AudioEngine::shared();
        auto id = audio.playEffect("dev/button.mp3");
        assert(id != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(CocosAudioEngine::getState(id) == AudioState::INITIALIZING ||
               CocosAudioEngine::getState(id) == AudioState::PLAYING);
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id) != audio._effectIDs.end());
        audio.stopEffect(id);
    }
    void stopEffect()
    {
        auto& audio = AudioEngine::shared();
        auto id = audio.playEffect("dev/button.mp3");
        assert(id != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id) != audio._effectIDs.end());
        audio.stopEffect(id);
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id) == audio._effectIDs.end());
        assert(CocosAudioEngine::getState(id) == AudioState::ERROR);
    }
    void pauseEffect()
    {
        auto& audio = AudioEngine::shared();
        auto id = audio.playEffect("dev/button.mp3");
        assert(id != CocosAudioEngine::INVALID_AUDIO_ID);
        audio.pauseEffect(id);
        assert(CocosAudioEngine::getState(id) == AudioState::PAUSED ||
               CocosAudioEngine::getState(id) == AudioState::INITIALIZING);
        audio.stopEffect(id);
    }
    void resumeEffect()
    {
        auto& audio = AudioEngine::shared();
        auto id = audio.playEffect("dev/button.mp3");
        assert(id != CocosAudioEngine::INVALID_AUDIO_ID);
        audio.pauseEffect(id);
        assert(CocosAudioEngine::getState(id) == AudioState::PAUSED ||
               CocosAudioEngine::getState(id) == AudioState::INITIALIZING);
        audio.resumeEffect(id);
        assert(CocosAudioEngine::getState(id) == AudioState::INITIALIZING ||
               CocosAudioEngine::getState(id) == AudioState::PLAYING);
        audio.stopEffect(id);
    }
    void stopMusic()
    {
        // tested in playMusic
    }
    void stopAll()
    {
        auto& audio = AudioEngine::shared();
        auto id1 = audio.playEffect("dev/button.mp3");
        auto id2 = audio.playEffect("dev/button.mp3");
        auto id3 = audio.playEffect("dev/button.mp3");
        assert(id1 != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(id2 != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(id3 != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id1) != audio._effectIDs.end());
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id2) != audio._effectIDs.end());
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id3) != audio._effectIDs.end());
        audio.stopAll();
        assert(CocosAudioEngine::getState(id1) == AudioState::ERROR);
        assert(CocosAudioEngine::getState(id2) == AudioState::ERROR);
        assert(CocosAudioEngine::getState(id3) == AudioState::ERROR);
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id1) == audio._effectIDs.end());
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id2) == audio._effectIDs.end());
        assert(std::find(audio._effectIDs.begin(), audio._effectIDs.end(), id3) == audio._effectIDs.end());
    }
    void pauseAll()
    {
        auto& audio = AudioEngine::shared();
        auto id1 = audio.playEffect("dev/button.mp3");
        auto id2 = audio.playEffect("dev/button.mp3");
        auto id3 = audio.playEffect("dev/button.mp3");
        assert(id1 != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(id2 != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(id3 != CocosAudioEngine::INVALID_AUDIO_ID);
        audio.pauseAll();
        assert(CocosAudioEngine::getState(id1) == AudioState::PAUSED ||
               CocosAudioEngine::getState(id1) == AudioState::INITIALIZING);
        assert(CocosAudioEngine::getState(id2) == AudioState::PAUSED ||
               CocosAudioEngine::getState(id2) == AudioState::INITIALIZING);
        assert(CocosAudioEngine::getState(id3) == AudioState::PAUSED ||
               CocosAudioEngine::getState(id3) == AudioState::INITIALIZING);
        audio.resumeAll();
        audio.stopAll();
    }
    void resumeAll()
    {
        auto& audio = AudioEngine::shared();
        auto id1 = audio.playEffect("dev/button.mp3");
        auto id2 = audio.playEffect("dev/button.mp3");
        auto id3 = audio.playEffect("dev/button.mp3");
        assert(id1 != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(id2 != CocosAudioEngine::INVALID_AUDIO_ID);
        assert(id3 != CocosAudioEngine::INVALID_AUDIO_ID);
        audio.pauseAll();
        audio.resumeAll();
        assert(CocosAudioEngine::getState(id1) == AudioState::PLAYING ||
               CocosAudioEngine::getState(id1) == AudioState::INITIALIZING);
        assert(CocosAudioEngine::getState(id2) == AudioState::PLAYING ||
               CocosAudioEngine::getState(id2) == AudioState::INITIALIZING);
        assert(CocosAudioEngine::getState(id3) == AudioState::PLAYING ||
               CocosAudioEngine::getState(id3) == AudioState::INITIALIZING);
        audio.stopAll();
    }
};
#endif

void runTest()
{
#if COCOS2D_DEBUG > 0
    if(cocos2d::FileUtils::getInstance()->isFileExist("dev/button.mp3"))
    {
        assert(TestAudioEngine().run_all());
    }
#endif
}
