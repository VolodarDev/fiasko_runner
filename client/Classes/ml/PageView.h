/*
* Copyright 2014-2018 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __PageView_h__
#define __PageView_h__

#include "ml/NodeExt.h"


class PageView : public cocos2d::ui::PageView, public NodeExt
{
public:
	DECLARE_AUTOBUILDER(PageView);
	virtual bool init() override;
public:
	virtual bool loadXmlEntity(const std::string & tag, const pugi::xml_node & xmlnode)override;
};

#endif
