/******************************************************************************/
/*
 * Copyright 2014-2019 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef ml_Layout_hpp
#define ml_Layout_hpp

#include "ml/NodeExt.h"


class Layout : public cocos2d::ui::Layout, public NodeExt
{
public:
    DECLARE_AUTOBUILDER(Layout);
    virtual bool init() override;
    virtual bool setProperty(const std::string& name, const std::string& value) override;
protected:
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    virtual void update(float dt) override;
    cocos2d::Sprite* _backFill;
#endif
};

#endif /* Layout_hpp */
