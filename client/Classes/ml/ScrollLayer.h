/*
 *  Copyright 2018-2020 Vladimir Tolmachev
 *
 *   Author: Vladimir Tolmachev
 *  Project: Dungeon: Age of Heroes
 *   e-mail: tolm_vl@hotmail.com
 *
 * If you received the code is not the author, please contact me
 */
#ifndef __ScrollLayer_h__
#define __ScrollLayer_h__

#include "ml/NodeExt.h"


class ScrollLayer : public LayerExt
{
	DECLARE_AUTOBUILDER(ScrollLayer);
	virtual bool init()override;
public:
	virtual bool setProperty(const std::string & name, const std::string & value)override;
    virtual void setScale(float scale) override;
    virtual void setScale(float scaleX, float scaleY) override;
	void setContent(const cocos2d::Size& size);
	void setVisibled(const cocos2d::Size& size);
	void setMaxScale(float scale);
	void setMinScale(float scale);
	void setBottomBorder(float border);
    
    float getMaxScale()const;
    float getMinScale()const;

	virtual void update(float dt)override;
	virtual void onEnter()override;
	virtual void onExit()override;

    void enableEventTouches();
	bool disableEventTouches();
    bool isEventTouchesEnabled()const;

	bool touchesBegan(const std::vector<cocos2d::Touch*>& touches);
	bool touchesMoved(const std::vector<cocos2d::Touch*>& touches);
	bool touchesEnded(const std::vector<cocos2d::Touch*>& touches);
	void touchesCancelled(const std::vector<cocos2d::Touch*>& touches);

	const cocos2d::Vec2& getScrollPosition()const;
	void setScrollPosition(const cocos2d::Vec2& position);
	void setScroll(float scale, const cocos2d::Vec2& anchor);
	void stopAccelerate();
	
	void scrollToPosition(const cocos2d::Point& childPosition, float duration = 0);
	
	void addTouchable(NodePointer node, const std::function<void()>& callback);
protected:
	virtual void onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event *event)override;
	virtual void onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event *event)override;
	virtual void onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event *event)override;
	virtual void onTouchesCancelled(const std::vector<cocos2d::Touch*>&touches, cocos2d::Event *event)override;
	
	bool checkTouchableNode(const cocos2d::Point& touchLocation);

	cocos2d::Vec2 fitPosition(const cocos2d::Vec2& position)const;
	float fitScale(float scale)const;
	
	void accelerate(float dt);
	void accelerateOnTouchMove(const cocos2d::Vec2& offset);
private:
	enum State
	{
		wait = 0,
		scroll = 1,
		scaling = 2,
	}_state;
	bool _wasAction;
	IntrusivePtr<cocos2d::EventListenerTouchAllAtOnce> _touchListener;

	IntrusivePtr<cocos2d::Touch> _touchA;
	IntrusivePtr<cocos2d::Touch> _touchB;
	cocos2d::Point _startTouchA;
	cocos2d::Point _startTouchB;
	float _touchDistance;
	float _distanceOnStartScroll;
	float _bottomBorder;

	cocos2d::Point _visibled;
	cocos2d::Point _content;
	float _maxScale;
	float _minScale;

	cocos2d::Point _positionOnTouchBegan;
	cocos2d::Point _position;
	float _scale;
	
	cocos2d::Vec2 _accelerateDirection;
	float _accelerateVelocity;
	
	std::vector<std::pair<NodePointer, std::function<void()>>> _touchableNodes;
};

#endif
