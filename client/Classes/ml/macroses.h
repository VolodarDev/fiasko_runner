/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __ml_macroses_h__
#define __ml_macroses_h__

#include <iostream>
#include <memory>
#include "ml/types.h"


#define ML_ASSERT(exp) if(!(exp)){ \
CCLOG("ML_ASSERT: %s", #exp); \
std::cout << " - FILE: " << __FILE__ << std::endl; \
std::cout << " - FUNC: " << __FUNCTION__ << std::endl; \
std::cout << " - LINE: " << __LINE__ << std::endl; \
show_call_stack(); \
throw Exception(std::string(#exp) + "\nCall Stack:\n" + show_call_stack() + \
cocos2d::StringUtils::format("\nFILE: %s \nFUNC: %s \nLINE: %d", __FILE__, __FUNCTION__, __LINE__)); \
}

#define DECLARE_AUTOBUILDER_WITHOUT_CONSTRUCTOR(CLASS)                  \
public:                                                                 \
    template <class ... Types>                                          \
    static IntrusivePtr<CLASS> create(Types && ... _Args) {             \
        IntrusivePtr<CLASS> pointer = make_intrusive<CLASS>();          \
        if(!pointer || !pointer->init(std::forward<Types>(_Args)... ))  \
            pointer.reset(nullptr);                                     \
        return pointer;                                                 \
    }

#define DECLARE_AUTOBUILDER(CLASS)                                      \
public:                                                                 \
    typedef IntrusivePtr<CLASS> Pointer;                                \
    typedef IntrusivePtr<CLASS> CLASS##Pointer;                         \
    CLASS();                                                            \
    virtual ~CLASS();                                                   \
    DECLARE_AUTOBUILDER_WITHOUT_CONSTRUCTOR(CLASS)


#ifndef NDEBUG
#   define PROFILE_FUNCTION_BEGIN \
    auto start__ = std::chrono::system_clock::now();

#   define PROFILE_FUNCTION_END \
    auto end__ = std::chrono::system_clock::now(); \
    long long elapsed__ = std::chrono::duration_cast<std::chrono::microseconds>(end__ - start__).count(); \
    static long long summary__ = 0; \
    summary__ += elapsed__; \
    cocos2d::log("%s: Summary: %lld us\tFrame: %lld us", __FUNCTION__, summary__, elapsed__);

#else
#   define PROFILE_FUNCTION_BEGIN
#   define PROFILE_FUNCTION_END
#endif


#endif
