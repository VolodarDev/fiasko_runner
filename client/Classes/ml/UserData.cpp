/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "ml/UserData.h"
#include "ml/Crypt.h"
#include "Classes/support/FileSystemUtils.h"

std::string phoneID("1238977688sdaf8768646-asfd");

namespace __userdata
{
	static std::string kFile("sd.dat");
	static const std::string kUserSessionNumber("session");
	static bool isInitialised(false);
	pugi::xml_document Doc;

	void init()
	{
		isInitialised = true;
		std::string path = FileSystemUtils::getWritablePath();
		kFile = path + kFile;
	}
	bool validateDoc()
	{
		if(!Doc.root().first_child())
            return true;
		auto root = Doc.root().first_child();
		auto device = root.child("device");
		if(device)
		{
			return phoneID == device.attribute("id").as_string();
		}
		else
		{
			if(!root)
				root = Doc.root().append_child("root");
			device = root.append_child("device");
			device.append_attribute("id").set_value(phoneID.c_str());
		}
		return true;
	}

    void clearDoc()
    {
        Doc.reset();
    }

	void openDoc()
	{
		if(isInitialised == false)
            init();

		if(Doc.root().first_child())
			return;

        auto string = FileSystemUtils::getStringFromFile(kFile);
        crypt::decode(string);
        Doc.load(string.c_str());
        
		if(validateDoc() == false)
		{
            clearDoc();
		}
	}

	void saveDoc()
	{
		if(isInitialised == false)
			init();
		auto dir = kFile;
		auto k = dir.find_last_of("/");
		dir = dir.substr(0, k);
		
        unsigned int flag = pugi::format_no_escapes | pugi::format_no_declaration | pugi::format_indent;
        std::stringstream ss;
        Doc.save(ss, " ", flag);
        auto string = ss.str();
        
#ifndef NDEBUG
        auto sourceString = string;
#endif
        crypt::encode(string);
        
#ifndef NDEBUG
        std::string test = string;
        crypt::decode(test);
        assert(test == sourceString);
#endif

        cocos2d::FileUtils::getInstance()->createDirectory(dir);
        cocos2d::FileUtils::getInstance()->writeStringToFile(string, kFile);
        
	}
}

static UserData* instanse(nullptr);

UserData::UserData()
{
	instanse = this;
}

UserData::~UserData()
{
	instanse = nullptr;
}

void UserData::clear()
{
	__userdata::clearDoc();
	refreshXmlNode();
}

void UserData::load()
{
    __userdata::openDoc();
    unsigned session = getSessionNumber();
    ++session;
    write(__userdata::kUserSessionNumber, session);
}

void UserData::save()
{
    __userdata::saveDoc();
}

UserData* UserData::getInstance()
{
	return instanse;
}

void UserData::write(const std::string & key, const std::string & value)
{
	auto root = getCurrentXmlNode();
    assert(root);

	auto node = root.child(key.c_str());
	if(!node)
		node = root.append_child(key.c_str());
	auto attr = node.attribute("value");
	if(!attr)
		attr = node.append_attribute("value");
	attr.set_value(value.c_str());
    save();
}

std::string UserData::get(const std::string & key, const std::string & defaultvalue)
{
	auto root = getCurrentXmlNode();
    assert(root);

	auto node = root.child(key.c_str());
	auto attribute = node.attribute("value");
	if(attribute)
		return attribute.as_string();

	return defaultvalue;
}

unsigned UserData::getSessionNumber()
{
	return get<unsigned>(__userdata::kUserSessionNumber, 0);
}

void UserData::lang_set(const std::string& id)
{
	write("userlanguage", id);
}

std::string UserData::lang_get()
{
	return get("userlanguage");
}

void UserData::set_audio_enable(bool enabled)
{
    write("audio_enabled", enabled);
}

bool UserData::is_audio_enabled()
{
    return get("audio_enabled", true);
}

pugi::xml_node UserData::getCurrentXmlNode()
{
	if(!_xmlNode)
		refreshXmlNode();
	return _xmlNode;
}

void UserData::refreshXmlNode()
{
	auto root = __userdata::Doc.root().child("root");
	if(!root)
		root = __userdata::Doc.root().append_child("root");

	_xmlNode = root;
}

