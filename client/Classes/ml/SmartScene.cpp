/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: Defense of Greece
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "ml/SmartScene.h"

USING_NS_CC;

static int kOpacityBackLayerDefault(196);

void SmartScene::setOpacityBackLayer(int opacity)
{
	kOpacityBackLayerDefault = opacity;
}
const std::string kImageShadow("point.png");

SmartScene::SmartScene()
: _nowBlockedTopLayer(false)
, _autoPushShadow(true)
, _dispachPopLayers(false)
, _durationShadowFade(0.f)
, _opacityShadowFade(196)
, _colorShadow(0, 0, 0)
, _pushMode(PushLayerMode::nonset)
{
	_tempLayerData.exitPrevios = false;
}

SmartScene::~SmartScene()
{
}

bool SmartScene::init()
{
	Scene::init();
    NodeExt::init();
	_shadowResource = kImageShadow;

	return true;
}

void SmartScene::onEnter()
{
	Scene::onEnter();
	_dispachPopLayers = false;

	for(int i = 0; i < static_cast<int>(_stack.size()) - 1; ++i)
	{
		auto layers = _stack[i];
		for(auto layer : layers)
		{
			if(layer->isRunning())
				layer->onExit();
		}
	}
	if(_stack.empty() == false)
	{
		auto layers = _stack.back();
		for(auto layer : layers)
		{
			if(layer->isRunning() == false)
				layer->onEnter();
		}
	}
	_dispachPopLayers = true;
}

void SmartScene::onExit()
{
	_dispachPopLayers = false;
	if(_stack.empty() == false)
	{
		auto layers = _stack.back();
		for(auto layer : layers)
		{
			if(layer->isRunning())
				layer->onExit();
		}
	}

	Scene::onExit();
}

void SmartScene::addToMainStack(LayerPointer layer)
{
    if(_stack.empty())
    {
        _stack.emplace_back();
    }
    _stack[0].push_back(layer);
}

void SmartScene::pushLayer(Layer* layer, bool exitPrevios, bool waitShadow, bool useZOrderLayer)
{
	switch(_pushMode)
	{
		case PushLayerMode::nonset:break;
		case PushLayerMode::force_block: exitPrevios = true; break;
		case PushLayerMode::force_nonblock: exitPrevios = false; break;
		default:assert(0);
	}

	if(waitShadow)
	{
		assert(_tempLayerData.layer == nullptr);
		_tempLayerData.layer = layer;
		_tempLayerData.exitPrevios = exitPrevios;

		if(exitPrevios)
		{
			_tempLayerData.dummy = Layer::create();
			pushLayer(_tempLayerData.dummy, true, false);
		}
		return;
	}
	if(layer)
	{
		assert(_stack.empty() == false);
		auto top = _stack.back();
		assert(top.empty() == false);
		int z = useZOrderLayer ? layer->getLocalZOrder() : top.back()->getLocalZOrder() + 2;

		layer->setOnExitCallback(std::bind(&SmartScene::on_layerClosed, this, layer ));
		addChild(layer, z);


		if(exitPrevios)
		{
			_stack.push_back(std::deque<LayerPointer>());
			_stack.back().push_back(layer);
			_dispachPopLayers = false;
			auto layers = _stack[_stack.size() - 2];
			for(auto& layer : layers)
			{
				if(layer->isRunning())
					layer->onExit();
			}
			if(_autoPushShadow)
				pushShadow();
			_dispachPopLayers = true;
		}
		else
		{
			_stack.back().push_back(layer);
		}
		onLayerPushed(layer);
	}
}

void SmartScene::on_layerClosed(Layer* layer)
{
	if(_nowBlockedTopLayer)return;
	if(!_dispachPopLayers)return;

	bool exist(false);
	for(size_t i = 0; i < _stack.size(); ++i)
	{
		auto& layersDeque = _stack[i];
		for(size_t j = 0; j < layersDeque.size(); ++j)
		{
			if(layer == layersDeque[j])
			{
				exist = true;
				layersDeque.erase(layersDeque.begin() + j);
				break;
			}
		}
		if(exist && layersDeque.empty())
		{
//            assert(i > 0 && i == (_stack.size() - 1));
			for(auto& layer : _stack[i - 1])
			{
				if(layer->isRunning() == false)
					layer->onEnter();
			}
			if(_tempLayerData.dummy.ptr() != layer)
				popShadow();
			_stack.erase(_stack.begin() + i);
			break;
		}
	}
	if(exist)
		onLayerPoped(layer);
}

void SmartScene::clearStack()
{
	_stack.clear();
	removeAllChildren();
}

void SmartScene::pushShadow()
{
	auto dessize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	auto z = _stack.empty() == false ? _stack.back().back()->getLocalZOrder() - 1 : 1;
	SpritePointer shadow = createSprite(_shadowResource);
	shadow->setName("shadow");
	shadow->setScaleX(dessize.width);
	shadow->setScaleY(dessize.height);
	shadow->setColor(_colorShadow);
	shadow->setOpacity(0);
	shadow->setPosition(cocos2d::Point(dessize / 2 ));
	_shadows.push_back(shadow);
	shadow->runAction(Sequence::createWithTwoActions(FadeTo::create(_durationShadowFade, _opacityShadowFade), CallFunc::create([this](){onShadowAppearanceEnded(); }) ));
	addChild(shadow, z);
}

void SmartScene::popShadow()
{
	if(_shadows.empty() == false)
	{
		auto shadow = _shadows.back();
		shadow->runAction(Sequence::create(FadeOut::create(_durationShadowFade), RemoveSelf::create(), nullptr ));
		_shadows.pop_back();
	}
}

void SmartScene::onShadowAppearanceEnded()
{
	if(_tempLayerData.dummy)
	{
		auto dummy = _tempLayerData.dummy;
		auto layer = _tempLayerData.layer;

		_tempLayerData.layer.reset(nullptr);

		_autoPushShadow = false;
		dummy->removeFromParent();
		pushLayer(layer, _tempLayerData.exitPrevios, false);
		_autoPushShadow = true;
		_tempLayerData.dummy.reset(nullptr);
	}
}

LayerPointer SmartScene::getTopLayer()
{
    return !_stack.empty() ? _stack.back().back() : nullptr;
}

