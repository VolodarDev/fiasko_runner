/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __Events_h__
#define __Events_h__
#include "ml/IntrusivePtr.h"
#include "ml/ParamCollection.h"
#include "ml/macroses.h"
#include "ml/pugixml/pugixml.hpp"


class NodeExt;

class EventBase : public cocos2d::Ref
{
public:
    typedef IntrusivePtr<EventBase> Pointer;
public:
	static Pointer create(const std::string & type);

	virtual void execute(NodeExt * context) = 0;
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name) = 0;
	virtual bool loadXmlEntity(const std::string& tag, const pugi::xml_node& node);

	virtual cocos2d::Node* getTarget(NodeExt * context);
private:
	CC_SYNTHESIZE_PASS_BY_REF(std::list<int>, _targetTags, TagsToTarget);
	CC_SYNTHESIZE_PASS_BY_REF(std::string, _targetPath, PathToTarget);
};

class EventsList : public std::list<EventBase::Pointer>
{
public:
	void execute(NodeExt * context);
};

class EventAction : public EventBase
{
	DECLARE_AUTOBUILDER(EventAction);
	bool init();
public:
	virtual void execute(NodeExt * context);
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name);
private:
private:
	std::string _actionname;
	std::string _state;
};

class EventRunAction : public EventAction
{
	DECLARE_AUTOBUILDER(EventRunAction);
	bool init();
};

class EventStopAction : public EventAction
{
	DECLARE_AUTOBUILDER(EventStopAction);
	bool init();
};

class EventStopAllAction : public EventAction
{
	DECLARE_AUTOBUILDER(EventStopAllAction);
	bool init();
public:
	virtual void execute(NodeExt * context);
};

class EventSetProperty : public EventBase
{
	DECLARE_AUTOBUILDER(EventSetProperty);
	bool init();
public:
	virtual void execute(NodeExt * context);
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name);
private:
	int _property;
	std::string _stringproperty;
	std::string _value;
};

class EventPlaySound : public EventBase
{
	DECLARE_AUTOBUILDER(EventPlaySound);
	bool init();
public:
	virtual void execute(NodeExt * context) override;
	virtual void setParam(const std::string & name, const std::string & value) override;
	virtual std::string getParam(const std::string & name) override;
    virtual bool loadXmlEntity(const std::string& tag, const pugi::xml_node& node) override;
private:
	void play(float dt);
	void stop(float dt);
    
    std::string getSound() const;
private:
    std::vector<std::string> _sounds;
	unsigned int _soundID;
	bool _asMusic;
	bool _looped;
	float _predelay;
	float _duration;
};

class EventScene : public EventBase
{
	DECLARE_AUTOBUILDER(EventScene);
	bool init();
public:
	static std::function<IntrusivePtr<cocos2d::Scene>(const std::string&)> GetScene;
public:
	virtual void execute(NodeExt * context);
	virtual void setParam(const std::string & name, const std::string & value);
	virtual std::string getParam(const std::string & name);
private:
private:
	std::string _action;
	std::string _scene;
};

class EventCreateNode : public EventBase
{
	DECLARE_AUTOBUILDER(EventCreateNode);
	bool init();
public:
	virtual void execute(NodeExt * context)override;
	virtual void setParam(const std::string & name, const std::string & value)override;
	virtual std::string getParam(const std::string & name)override;
	virtual bool loadXmlEntity(const std::string& tag, const pugi::xml_node& node)override;
private:
	struct PositionInfo
	{
		cocos2d::Point offset;
		enum class PosType{ 
			byContext ,
			offset,
		} method;
	}_positionInfo;
	std::vector<NodePointer> _nodes;
	std::vector<std::string> _paths;
	int _additionalZOrder;
};


#endif
