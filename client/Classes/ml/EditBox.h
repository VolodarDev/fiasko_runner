/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#ifndef EditBox_hpp
#define EditBox_hpp

#include "ml/macroses.h"
#include "ui/CocosGUI.h"

class EditBox : public cocos2d::ui::EditBox
{
    DECLARE_AUTOBUILDER(EditBox);
public:
    virtual bool init() override;
};

#endif /* EditBox_hpp */
