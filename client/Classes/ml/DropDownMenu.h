#ifndef DropDownMenu_hpp
#define DropDownMenu_hpp

#include "ml/NodeExt.h"

class DropDownMenu : public cocos2d::ui::Widget, public NodeExt
{
public:
    DropDownMenu();
    virtual ~DropDownMenu();
    static DropDownMenu* create();
    
public:
    virtual bool init() override;
    void addItem(const std::string& text, const std::function<void()>& callback);
    
    virtual bool setProperty(const std::string& name, const std::string& value) override;

    virtual void addChild(Node* child)override;
    virtual void addChild(Node * child, int localZOrder)override;
    virtual void addChild(Node* child, int localZOrder, int tag) override;
    virtual void addChild(Node* child, int localZOrder, const std::string &name) override;
    virtual void onEnter() override;
    virtual void setContentSize(const cocos2d::Size& size) override;
protected:
    void switchState();
    void open();
    void close();
    void setImage();
    void arrange();
protected:
    bool _initialized;
    bool _arranging;
    cocos2d::ui::Button* _button;
    cocos2d::ui::Button* _buttonClose;
    cocos2d::ui::ScrollView* _scroll;
    cocos2d::ui::Layout* _content;
    
    float _menuHeight;
    std::string _imageNormal;
    std::string _imageOpen;
};

#endif /* DropDownMenu_hpp */
