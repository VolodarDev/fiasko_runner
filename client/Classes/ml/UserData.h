/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __ml_USER_DATA__
#define __ml_USER_DATA__

#include "ml/Singlton.h"
#include "ml/common.h"
#include "ml/loadxml/xmlLoader.h"


class UserData : public Singlton<UserData>
{
public:
	UserData();
	virtual ~UserData();
	static UserData* getInstance();

	virtual void clear();
    virtual void load();
	virtual void save();

	void write(const std::string & key, const std::string & value);
	std::string get(const std::string & key, const std::string & defaultValue = "");
	template <class T> void write(const std::string & key, T value)
	{
		write(key, toStr(value ));
	}
	template <class T> T get(const std::string & key, T defaultValue = 0)
	{
		return strTo<T>(get(key, toStr(defaultValue)));
	}


	unsigned getSessionNumber();
	
    void lang_set(const std::string& id);
	std::string lang_get();
    
    void set_audio_enable(bool enabled);
    bool is_audio_enabled();
    
protected:
	virtual pugi::xml_node getCurrentXmlNode();
	void refreshXmlNode();
private:
	pugi::xml_node _xmlNode;
};

#endif
