/*
 *  Copyright 2018-2020 Vladimir Tolmachev
 *
 *   Author: Vladimir Tolmachev
 *  Project: Dungeon: Age of Heroes
 *   e-mail: tolm_vl@hotmail.com
 *
 * If you received the code is not the author, please contact me
 */

#ifndef __ScrollViewVertical_h__
#define __ScrollViewVertical_h__

#include "ml/NodeExt.h"

class ScrollViewVertical : public cocos2d::ui::ScrollView, public NodeExt
{
public:
    DECLARE_AUTOBUILDER(ScrollViewVertical);
    virtual bool init() override;
public:
    virtual void onLoaded() override;
protected:
    virtual void doLayout() override;

private:
};

#endif
