//
// Created by Vladimir Tolmachev on 2019-09-23.
//

#include "ml/CocosActionBuilder.h"

CocosActionBuilder& CocosActionBuilder::call(const std::function<void()>& function)
{
    _actions.pushBack(cocos2d::CallFunc::create(function));
    return *this;
}
CocosActionBuilder& CocosActionBuilder::delay(float duration)
{
    _actions.pushBack(cocos2d::DelayTime::create(duration));
    return *this;
}
cocos2d::Sequence* CocosActionBuilder::build()
{
    return cocos2d::Sequence::create(_actions);
}