/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#ifndef FillSprite_h
#define FillSprite_h

#include "ml/NodeExt.h"


class FillSprite : public cocos2d::Sprite, public NodeExt
{
    DECLARE_AUTOBUILDER(FillSprite);
    virtual bool init() override;
protected:
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
};

#endif /* FillSprite_hpp */
