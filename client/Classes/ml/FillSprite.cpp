/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#include "ml/FillSprite.h"

FillSprite::FillSprite()
{
    
}
FillSprite::~FillSprite()
{
    
}


bool FillSprite::init()
{
    return Sprite::init() && NodeExt::init();
}

void FillSprite::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    if(isDirty() && getTexture())
    {
        cocos2d::Texture2D::TexParams texParam;
        texParam.wrapS = GL_REPEAT;
        texParam.wrapT = GL_REPEAT;
        texParam.minFilter = GL_LINEAR;
        texParam.magFilter = GL_LINEAR;
        getTexture()->setTexParameters(texParam);
        
        auto size = getContentSize();
        setTextureRect(cocos2d::Rect(cocos2d::Point::ZERO, size));
    }
    Sprite::draw(renderer, transform, flags);
}
