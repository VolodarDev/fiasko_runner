/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include "ml/ParamCollection.h"
#include "ml/common.h"

using namespace cocos2d;

struct Test : public Ref
{
	Test(){}
	Test(int){}
	Test(float){}
	Test(float, int, double, char, int, float){}
};

void test()
{
	auto p = 
		make_intrusive<Test>();
	p = make_intrusive<Test>(1);
	p = make_intrusive<Test>(1.f);
	p = make_intrusive<Test>(1,2,3,4,5,6);

}

float getAngle(const cocos2d::Point & a, const cocos2d::Point & b)
{
	return -atan2(a.x*b.y - b.x*a.y, a.x*b.x + a.y*b.y) * 180.f / float(M_PI);
}

cocos2d::Point getVectorByDirection(float direction)
{
	cocos2d::Point p;
	float rad = CC_DEGREES_TO_RADIANS(direction);
	p.x = cos(rad);
	p.y = -sin(rad);
	return p;
}

float getDirectionByVector(const cocos2d::Point & radius)
{
	cocos2d::Point axis(1, 0);
	return getAngle(axis, radius);
}

float lerpDegrees(float start, float end, float amount)
{
	float difference = std::fabs(end - start);
	if(difference > 180.f)
	{
		if(end > start)
			start += 360.f;
		else
			end += 360.f;
	}

	float value = start + (end - start) * amount;
	while(value >= 360.f) value -= 360.f;
	while(value < 0.f) value += 360.f;
	return value;
};

float getDistance(const cocos2d::Point & point, const cocos2d::Point & A, const cocos2d::Point & B)
{
	float x = point.x;
	float y = point.y;
	float x1 = A.x;
	float y1 = A.y;
	float x2 = B.x;
	float y2 = B.y;
	return std::abs((x - x1) * (y2 - y1) - (y - y1) * (x2 - x1)) / sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

float distanse_pointToLineSegment(
	const cocos2d::Point & segmentA,
	const cocos2d::Point & segmentB,
	const cocos2d::Point & point)
{
	const cocos2d::Point P0 = point;
	const cocos2d::Point P1 = segmentA;
	const cocos2d::Point P2 = segmentB;

	const cocos2d::Point p01 = P0 - P1;
	const cocos2d::Point p21 = P2 - P1;

	const float T = p21.dot(p01) / p21.lengthSquared();

	if(T < 0 || T > 1)
		return (float)1E+37;

	const cocos2d::Point P = P1 + p21 * T;
	const float distance = P.getDistance(point);
	return distance;
}


cocos2d::FiniteTimeAction * createRouteAction(const std::vector<cocos2d::Point> route, float objectSpeed)
{
	cocos2d::Vector<cocos2d::FiniteTimeAction*> actions;
	for(unsigned i = 1; i < route.size(); ++i)
	{
		float duration(0);
		duration = route[i - 1].getDistance(route[i]) / objectSpeed;
		actions.pushBack(cocos2d::MoveTo::create(duration, route[i] ));
	}
	return cocos2d::Sequence::create(actions);
};

bool checkPointInNode(const cocos2d::Node * node, const cocos2d::Point & pointInParentSpace, int depth);

void split(std::list<std::string> & out, const std::string & values, const char delimiter)
{
	if(values.empty())
		return;
	std::string string = values;
	do
	{
		size_t k = string.find_first_of(delimiter);
		if(k == -1)
		{
			out.push_back(string);
			break;
		}

		out.push_back(string.substr(0, k ));
		string = string.substr(k + 1);
		if(string.empty())break;
	}
	while(true);
}

void split(std::vector<std::string> & out, const std::string & values, const char delimiter)
{
	if(values.empty())
		return;
	std::string string = values;
	do
	{
		size_t k = string.find_first_of(delimiter);
		if(k == -1)
		{
			out.push_back(string);
			break;
		}

		out.push_back(string.substr(0, k ));
		string = string.substr(k + 1);
		if(string.empty())break;
	}
	while(true);
}


Stretch::Stretch()
: mode(Mode::unknow)
, maxScaleX(-1)
, maxScaleY(-1)
, minScaleX(-1)
, minScaleY(-1)
{}

bool Stretch::empty()const
{
	return mode == Mode::unknow;
}

Stretch::Mode strToStretchMode(const std::string& mode)
{
	if(mode == "x") return Stretch::Mode::only_x;
	if(mode == "y") return Stretch::Mode::only_y;
	if(mode == "xy") return Stretch::Mode::both_xy;
	if(mode == "max") return Stretch::Mode::max_scale;
	if(mode == "min") return Stretch::Mode::min_scale;
	assert(!"TODO:");
	return Stretch::Mode::unknow;
}

//"frame:1x1:min[max:1,min:0.5]"
Stretch strToStretch(const std::string& string)
{
	Stretch result;
	std::string mode;
	std::string size;
	size_t paramB = string.find_last_of("[");
	size_t paramE = string.find_last_of("]");
	size_t k(std::string::npos);
	if(paramB == std::string::npos)
	{
		k = string.find_last_of(":");
	}
	else
	{
		size_t pos = string.find(':', 0);
		while(pos < paramB)
		{
			k = pos;
			pos = string.find(':', pos + 1);
		}
	}

	if(k != std::string::npos)
	{
		size = string.substr(0, k);
		if(paramB == std::string::npos)
			mode = string.substr(k + 1);
		else
			mode = string.substr(k + 1, paramB - (k + 1));
	}

	if(paramB != std::string::npos)
	{
		std::string paramsString;
		if(paramE != std::string::npos)
			paramsString = string.substr(paramB + 1, paramE - (paramB + 1));
		else
			paramsString = string.substr(paramB + 1);

		ParamCollection pc(paramsString);
		result.maxScaleX = pc.isExist("maxx") ? strToFloat(pc.get("maxx" )) : result.maxScaleX;
		result.maxScaleY = pc.isExist("maxu") ? strToFloat(pc.get("maxu" )) : result.maxScaleY;
		result.minScaleX = pc.isExist("minx") ? strToFloat(pc.get("minx" )) : result.minScaleX;
		result.minScaleY = pc.isExist("miny") ? strToFloat(pc.get("miny" )) : result.minScaleY;
		if(pc.isExist("max" ))
			result.maxScaleX = result.maxScaleY = strToFloat(pc.get("max" ));
		if(pc.isExist("min" ))
			result.minScaleX = result.minScaleY = strToFloat(pc.get("min" ));
	}


	result.boundingSize = strToSize(size);
	result.mode = strToStretchMode(mode);
	return result;
}

void stretchNode(cocos2d::Node*node, const Stretch& stretch)
{
	if(node == nullptr)
		return;

	cocos2d::Size size = node->getContentSize();
	if(size.width == 0 || size.height == 0)
	{
		return;
	}
	float sx = stretch.boundingSize.width / size.width;
	float sy = stretch.boundingSize.height / size.height;
	float ssx = node->getScaleX();
	float ssy = node->getScaleY();
	float zx = ssx / fabs(ssx);
	switch(stretch.mode)
	{
		case Stretch::Mode::max_scale:
			ssy = ssx = std::max(sx, sy);
			break;
		case Stretch::Mode::min_scale:
			ssy = ssx = std::min(sx, sy);
			break;
		case Stretch::Mode::both_xy:
			ssx = sx;
			ssy = sy;
			break;
		case Stretch::Mode::only_x:
			ssx = sx;
			break;
		case Stretch::Mode::only_y:
			ssy = sy;
			break;
		case Stretch::Mode::unknow:
			break;
	}

	if(zx < 0)
	{
		ssy = -ssy;
	}

	if(stretch.maxScaleX != -1) ssx = std::min(ssx, stretch.maxScaleX);
	if(stretch.maxScaleY != -1) ssy = std::min(ssy, stretch.maxScaleY);
	if(stretch.minScaleX != -1) ssx = std::max(ssx, stretch.minScaleX);
	if(stretch.minScaleY != -1) ssy = std::max(ssy, stretch.minScaleY);

	node->setScale(ssx, ssy);
}

void computePointsByRadius(std::vector<cocos2d::Point> & out, float radius, unsigned countPoints, float startAngleInDegree)
{
	float delta = static_cast<float>(M_PI) * 2.0f / countPoints;
	float startAngleInRadian = startAngleInDegree * static_cast<float>(M_PI) / 180.f;
	out.resize(countPoints);
	for(unsigned i=0; i<countPoints; ++i)
	{
		float angle = startAngleInRadian + delta * i;
		out[i].x = radius * cos(angle);
		out[i].y = radius * sin(angle);
	}
}

cocos2d::Node * getNodeByTagsPath(cocos2d::Node * root, const std::list<int> & tagspath)
{
	std::list<int> tags = tagspath;
	cocos2d::Node * node(root);
	
	while(node && tags.empty() == false)
	{
		node = node->getChildByTag(tags.front());
		tags.pop_front();
	}

	return node;
}

cocos2d::Node * getNodeByPath(cocos2d::Node * root, const std::string & path_names)
{
	std::list<std::string> names;
	split(names, path_names, '/');
	cocos2d::Node * node(root);

	while(node && names.empty() == false)
	{
		std::string name = names.front();
		if(name == "..")
			node = node->getParent();
		else if(name == ".")
        {
            names.pop_front();
			continue;
        }
		else if(name.empty() && path_names[0] == '/')
		{
			node = node->getScene();
			if(node == nullptr)
				node = Director::getInstance()->getRunningScene();
		}
		else 
			node = node->getChildByName(name);

		names.pop_front();
	}
	//if(!node)
	//	CCLOG("cannot find node by path [%s]", path_names.c_str());
	return node;
}

bool isFileExist(const std::string& path)
{
	auto isIni = path.find("ini/") == 0;
	auto isXml = path.find(".xml") == path.size() - strlen(".xml");
	if(isIni && isXml)
	{
		auto datFile = "dat/" + path.substr(4, path.size() - 8) + ".dat";
		if(FileUtils::getInstance()->isFileExist(datFile ))
			return true;
	}

	return FileUtils::getInstance()->isFileExist(path);
}

bool checkPointInNode(const cocos2d::Node * node, const cocos2d::Point & pointInParentSpace, int depth)
{
	if(!node)
		return false;
	cocos2d::Rect bb = node->getBoundingBox();
	cocos2d::Point point = pointInParentSpace;
	
	cocos2d::Node const* parent = node;
	while(parent)
	{
		if(parent->isVisible() == false)
			return false;
		parent = parent->getParent();
	}
	if(point.x > bb.origin.x &&
		point.x < bb.origin.x + bb.size.width &&
		point.y > bb.origin.y &&
		point.y < bb.origin.y + bb.size.height)
	{
		return true;
	}

	//if(depth == 0)return nullptr;
	//
	//cocos2d::Vector<cocos2d::Node*> children = node->getChildren();
	//
	//for(int i = 0; i < children.size(); ++i)
	//{
	//	const cocos2d::Node * child = dynamic_cast<cocos2d::Node*>(children.at(i));
	//	assert(child);
	//	if(checkPointInNode(child, point, depth - 1 ))
	//		return true;
	//}

	return false;
};


void fileLog(const std::string & s)
{
	FILE * file = fopen("log.txt", "a+");
	if(!file)
	{
		file = fopen("log.txt", "w+");
	}
	if(!file) return;
	fputs(s.c_str(), file);
	fclose(file);
};

cocos2d::Point getRandPointInPlace(const cocos2d::Point & center, float radius_)
{
	cocos2d::Point r;
	float angle = CCRANDOM_0_1() * float(M_PI) * 2;
    float radius = CCRANDOM_0_1() * radius_;
	float ca = cos(angle);
	float sa = sin(angle);
	r.x = center.x + ca * radius;
	r.y = center.y + sa * radius;

	return r;
}

const static std::map<std::string, EventKeyboard::KeyCode> keyCodeMap =
{
	{ "ESCAPE", EventKeyboard::KeyCode::KEY_ESCAPE },
	{ "BACK", EventKeyboard::KeyCode::KEY_BACK },
	{ "BACKSPACE", EventKeyboard::KeyCode::KEY_BACKSPACE },
	{ "TAB", EventKeyboard::KeyCode::KEY_TAB },
	{ "RETURN", EventKeyboard::KeyCode::KEY_RETURN },
	{ "SHIFT", EventKeyboard::KeyCode::KEY_SHIFT },
	{ "LEFT_SHIFT", EventKeyboard::KeyCode::KEY_LEFT_SHIFT },
	{ "RIGHT_SHIFT", EventKeyboard::KeyCode::KEY_RIGHT_SHIFT },
	{ "CTRL", EventKeyboard::KeyCode::KEY_CTRL },
	{ "LEFT_CTRL", EventKeyboard::KeyCode::KEY_LEFT_CTRL },
	{ "RIGHT_CTRL", EventKeyboard::KeyCode::KEY_RIGHT_CTRL },
	{ "ALT", EventKeyboard::KeyCode::KEY_ALT },
	{ "LEFT_ALT", EventKeyboard::KeyCode::KEY_LEFT_ALT },
	{ "RIGHT_ALT", EventKeyboard::KeyCode::KEY_RIGHT_ALT },
	{ "LEFT_ARROW", EventKeyboard::KeyCode::KEY_LEFT_ARROW },
	{ "RIGHT_ARROW", EventKeyboard::KeyCode::KEY_RIGHT_ARROW },
	{ "UP_ARROW", EventKeyboard::KeyCode::KEY_UP_ARROW },
	{ "DOWN_ARROW", EventKeyboard::KeyCode::KEY_DOWN_ARROW },
	{ "SPACE", EventKeyboard::KeyCode::KEY_SPACE },
	{ "F1", EventKeyboard::KeyCode::KEY_F1 },
	{ "F2", EventKeyboard::KeyCode::KEY_F2 },
	{ "F3", EventKeyboard::KeyCode::KEY_F3 },
	{ "F4", EventKeyboard::KeyCode::KEY_F4 },
	{ "F5", EventKeyboard::KeyCode::KEY_F5 },
	{ "F6", EventKeyboard::KeyCode::KEY_F6 },
	{ "F7", EventKeyboard::KeyCode::KEY_F7 },
	{ "F8", EventKeyboard::KeyCode::KEY_F8 },
	{ "F9", EventKeyboard::KeyCode::KEY_F9 },
	{ "F10", EventKeyboard::KeyCode::KEY_F10 },
	{ "F11", EventKeyboard::KeyCode::KEY_F11 },
	{ "F12", EventKeyboard::KeyCode::KEY_F12 },
	{ "0", EventKeyboard::KeyCode::KEY_0 },
	{ "1", EventKeyboard::KeyCode::KEY_1 },
	{ "2", EventKeyboard::KeyCode::KEY_2 },
	{ "3", EventKeyboard::KeyCode::KEY_3 },
	{ "4", EventKeyboard::KeyCode::KEY_4 },
	{ "5", EventKeyboard::KeyCode::KEY_5 },
	{ "6", EventKeyboard::KeyCode::KEY_6 },
	{ "7", EventKeyboard::KeyCode::KEY_7 },
	{ "8", EventKeyboard::KeyCode::KEY_8 },
	{ "9", EventKeyboard::KeyCode::KEY_9 },
	{ "A", EventKeyboard::KeyCode::KEY_A },
	{ "B", EventKeyboard::KeyCode::KEY_B },
	{ "C", EventKeyboard::KeyCode::KEY_C },
	{ "D", EventKeyboard::KeyCode::KEY_D },
	{ "E", EventKeyboard::KeyCode::KEY_E },
	{ "F", EventKeyboard::KeyCode::KEY_F },
	{ "G", EventKeyboard::KeyCode::KEY_G },
	{ "H", EventKeyboard::KeyCode::KEY_H },
	{ "I", EventKeyboard::KeyCode::KEY_I },
	{ "J", EventKeyboard::KeyCode::KEY_J },
	{ "K", EventKeyboard::KeyCode::KEY_K },
	{ "L", EventKeyboard::KeyCode::KEY_L },
	{ "M", EventKeyboard::KeyCode::KEY_M },
	{ "N", EventKeyboard::KeyCode::KEY_N },
	{ "O", EventKeyboard::KeyCode::KEY_O },
	{ "P", EventKeyboard::KeyCode::KEY_P },
	{ "Q", EventKeyboard::KeyCode::KEY_Q },
	{ "R", EventKeyboard::KeyCode::KEY_R },
	{ "S", EventKeyboard::KeyCode::KEY_S },
	{ "T", EventKeyboard::KeyCode::KEY_T },
	{ "U", EventKeyboard::KeyCode::KEY_U },
	{ "V", EventKeyboard::KeyCode::KEY_V },
	{ "W", EventKeyboard::KeyCode::KEY_W },
	{ "X", EventKeyboard::KeyCode::KEY_X },
	{ "Y", EventKeyboard::KeyCode::KEY_Y },
	{ "Z", EventKeyboard::KeyCode::KEY_Z },
	{ "TILDE", EventKeyboard::KeyCode::KEY_TILDE },
	{ "ENTER", EventKeyboard::KeyCode::KEY_ENTER },
	{ "GRAVE", EventKeyboard::KeyCode::KEY_GRAVE }
};

EventKeyboard::KeyCode strToKeyCode(const std::string & value)
{
	auto iter = keyCodeMap.find(value);
	if(iter != keyCodeMap.end())
	{
		return iter->second;
	}
	assert(0);
	return EventKeyboard::KeyCode::KEY_NONE;
}
