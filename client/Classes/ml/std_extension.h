/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __std_extension_h__
#define __std_extension_h__
#include <algorithm>
#include <list>

namespace std
{

	template <class Container, class Type>
	typename Container::const_iterator find(Container & container, const Type & value)
	{
		typename Container::const_iterator iterator = std::find(container.begin(), container.end(), value);
		return iterator;
	}
	
	template <class Container, class Type>
	size_t index_of(Container & container, const Type & value)
	{
		auto iterator = find(container, value);
		return iterator != std::end(container) ? iterator - std::begin(container) : -1;
	}

	template <class Type>
	Type sign(const Type & signedvalue)
	{
		return signedvalue < 0 ? -1 : (signedvalue>0 ? 1 : 0);
	}

};


#endif
