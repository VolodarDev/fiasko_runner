/******************************************************************************/
/*
 * Copyright 2014-2015 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: Defense of Greece
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __SmartScene_h__
#define __SmartScene_h__
#include "ml/NodeExt.h"


class SmartScene : public cocos2d::Scene, public NodeExt
{
	DECLARE_AUTOBUILDER(SmartScene);
	virtual bool init() override;
public:
	enum class PushLayerMode
	{
		nonset = 0,
		force_block = 1,
		force_nonblock = 2,
	};
public:
	static void setOpacityBackLayer(int opacity);
	void pushLayer(cocos2d::Layer* layer, bool blockPrevios = true, bool waitShadow = false, bool useZOrderLayer = false);
    void on_layerClosed(cocos2d::Layer* layer);
	virtual void clearStack();

	virtual void onEnter()override;
	virtual void onExit()override;

	virtual void onLayerPushed(LayerPointer layer) {}
	virtual void onLayerPoped(LayerPointer layer) {}
    
    void addToMainStack(LayerPointer layer);
    LayerPointer getTopLayer();
protected:
	void pushShadow();
	void popShadow();
	void onShadowAppearanceEnded();
private:
	bool _nowBlockedTopLayer;
	bool _autoPushShadow;
	bool _dispachPopLayers;
	struct
	{
		LayerPointer layer;
		LayerPointer dummy;
		bool exitPrevios;
	}_tempLayerData;

	std::deque< std::deque<LayerPointer> > _stack;
	std::deque<SpritePointer> _shadows;
	CC_SYNTHESIZE(std::string, _shadowResource, ShadowResource);
	CC_SYNTHESIZE(float, _durationShadowFade, DurationShadowFade);
	CC_SYNTHESIZE(float, _opacityShadowFade, OpacityShadowFade);
    CC_SYNTHESIZE(cocos2d::Color3B, _colorShadow, ColorShadow);
	CC_SYNTHESIZE(PushLayerMode, _pushMode, PushMode);
};

#endif // #ifndef SmartScene
