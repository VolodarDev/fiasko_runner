/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#ifndef Button_hpp
#define Button_hpp

#include "ml/FillSprite.h"

class Button : public cocos2d::ui::Button, public NodeExt
{
    DECLARE_AUTOBUILDER(Button);

public:
    virtual bool init() override;
    virtual cocos2d::Node* getChildByName(const std::string& name) const override;
    virtual bool setProperty(const std::string& name, const std::string& value) override;
    virtual void setContentSize(const cocos2d::Size& size) override;
    virtual void onEnter() override;
    void setSoundOnClick(const std::string& path);
    void simulateClick();
    
protected:
    virtual void onPressStateChangedToNormal() override;
    virtual void onPressStateChangedToDisabled() override;
    void buildImage();
private:
    std::string _frameWithFill;
    intrusive_ptr<FillSprite> _fill;
    bool _buildState;
    bool _isTextButton;
};

#endif /* Button_hpp */
