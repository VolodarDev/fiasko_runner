/*
* Copyright 2014-2018 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "ml/PageView.h"

using namespace cocos2d;

PageView::PageView()
{
}

PageView::~PageView()
{
}

bool PageView::init()
{
	if (!ui::PageView::init() || !NodeExt::init())
		return false;

	setTouchEnabled(true);
	setIndicatorEnabled(true);
	return true;
}

bool PageView::loadXmlEntity(const std::string & tag, const pugi::xml_node & xmlnode)
{
	auto loadPage = [this](const pugi::xml_node & xmlnode)
	{
		auto widget = xmlLoader::load_node<ui::Widget>(xmlnode, 1);
		widget->setContentSize(getContentSize());
		this->addPage(widget);
	};
	
	if(tag == "pages")
	{
		for(auto child : xmlnode)
		{
			loadPage(child);
		}
	}
	else if(tag == "page")
	{
		loadPage(xmlnode);
	}
	else
	{
		return NodeExt::loadXmlEntity(tag, xmlnode);
	}
	return true;
}
