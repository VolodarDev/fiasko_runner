#include "ml/DropDownMenu.h"
#include "ml/Button.h"
#include "ml/Localization.h"

DropDownMenu::DropDownMenu()
: _initialized(false)
, _arranging(false)
, _button(nullptr)
, _scroll(nullptr)
, _content(nullptr)
, _menuHeight(300)
{
    
}

DropDownMenu::~DropDownMenu()
{
    
}

DropDownMenu* DropDownMenu::create()
{
    auto ptr = new DropDownMenu();
    if(ptr->init())
    {
        ptr->autorelease();
        return ptr;
    }
    delete ptr;
    return nullptr;
}

bool DropDownMenu::init()
{
    cocos2d::ui::Widget::init();
    
    auto button = Button::create();
    _button = button;
    _button->setName("button");
    _button->setZoomScale(-0.1f);
    _button->addClickEventListener([this](cocos2d::Ref*){
        switchState();
    });
    addChild(_button);
    
    _scroll = cocos2d::ui::ScrollView::create();
    _scroll->setName("scroll");
    _scroll->setDirection(cocos2d::ui::ScrollView::Direction::VERTICAL);
    _scroll->setAnchorPoint(cocos2d::Vec2(0.0f, 1.f));
    _scroll->setBounceEnabled(true);
    _scroll->setClippingEnabled(true);
    _scroll->setVisible(false);
    addChild(_scroll);
    
    _content = cocos2d::ui::Layout::create();
    _content->setName("content");
    _content->setLayoutType(cocos2d::ui::Layout::Type::VERTICAL);
    _content->setContentSize(cocos2d::Size(0, 0));
    _content->setAnchorPoint(cocos2d::Vec2(0.5f, 0.f));
    _content->setClippingEnabled(false);
    _scroll->addChild(_content);
    
    _buttonClose = cocos2d::ui::Button::create("square.png");
    _buttonClose->setName("button_close");
    _buttonClose->setScale(99999);
    _buttonClose->setOpacity(0);
    _buttonClose->setVisible(false);
    _buttonClose->addClickEventListener([this](cocos2d::Ref*){
        close();
    });
    _buttonClose->setSwallowTouches(false);
    addChild(_buttonClose, -9999);
    
    _initialized = true;
    return true;
}

bool DropDownMenu::setProperty(const std::string& name, const std::string& value)
{
    if(name == "menu_height")
        _menuHeight = strTo<float>(value);
    else if(name == "image_normal")
    {
        _imageNormal = value;
        setImage();
    }
    else if(name == "image_open")
    {
        _imageOpen = value;
        setImage();
    }
    else if(name == xmlLoader::ksText)
    {
        _button->setTitleText(Localization::shared().locale(value));
    }
    else if(name == xmlLoader::ksFont || name == xmlLoader::ksFontTTF)
    {
    }
    else if(name == xmlLoader::ksFontSize)
    {
        _button->setTitleFontSize(strTo<float>(value));
    }
    else
        return NodeExt::setProperty(name, value);
    return true;
}

void DropDownMenu::setContentSize(const cocos2d::Size& size)
{
    cocos2d::ui::Widget::setContentSize(size);
    if(_button)
    {
        _button->setContentSize(size);
        arrange();
    }
}

void DropDownMenu::switchState()
{
    if(_scroll->isVisible())
        close();
    else
        open();
}

void DropDownMenu::open()
{
    _scroll->setVisible(true);
    _buttonClose->setVisible(true);
    setImage();
}

void DropDownMenu::close()
{
    _scroll->setVisible(false);
    _buttonClose->setVisible(false);
    setImage();
}

void DropDownMenu::setImage()
{
    if(_scroll->isVisible() && !_imageOpen.empty())
        xmlLoader::setProperty(_button, xmlLoader::kImageNormal, _imageOpen);
    if(!_scroll->isVisible() && !_imageNormal.empty())
        xmlLoader::setProperty(_button, xmlLoader::kImageNormal, _imageNormal);
}

void DropDownMenu::addItem(const std::string& text, const std::function<void()>& callback)
{
    auto button = cocos2d::ui::Button::create("square.png");
    button->setTitleText(text);
    button->setSwallowTouches(false);
    button->addClickEventListener([this, callback](Ref*){
        if(callback)
            callback();
        close();
    });
    addChild(button);
    
    arrange();
}

void DropDownMenu::arrange()
{
    if(_arranging)
        return;
    _arranging = true;
    
    setContentSize(_button->getContentSize());
    _button->setPosition(cocos2d::Point(_button->getContentSize()/2));
    _scroll->setPosition(cocos2d::Point(0, 0));

    xmlLoader::setProperty(_content, xmlLoader::kDoLayout, "auto_size");
    
    float maxWidth = 0;
    for(auto child : _content->getChildren())
    {
        maxWidth = std::max(maxWidth, child->getContentSize().width);
    }
    
    cocos2d::Size scrollSize;
    scrollSize.width = std::max(_button->getContentSize().width, maxWidth);
    scrollSize.height = std::min(-_content->getContentSize().height, _menuHeight);
    cocos2d::Size innerSize = cocos2d::Size(scrollSize.width, -_content->getContentSize().height);
    
    _scroll->setContentSize(scrollSize);
    _scroll->setInnerContainerSize(innerSize);
    _content->setContentSize(cocos2d::Size::ZERO);
    _content->setPositionY(innerSize.height);
    
    _arranging = false;
}

void DropDownMenu::onEnter()
{
    cocos2d::ui::Widget::onEnter();
    arrange();
}

void DropDownMenu::addChild(Node* child)
{
    DropDownMenu::addChild(child, child->getLocalZOrder(), child->getTag());
}

void DropDownMenu::addChild(Node * child, int localZOrder)
{
    DropDownMenu::addChild(child, localZOrder, child->getTag());
}

void DropDownMenu::addChild(Node *child, int zOrder, int tag)
{
    if(_initialized)
    {
        _content->addChild(child, zOrder, tag);
        arrange();
    }
    else
        cocos2d::ui::Widget::addChild(child, zOrder, tag);
}

void DropDownMenu::addChild(Node* child, int zOrder, const std::string &name)
{
    if(_initialized)
    {
        _content->addChild(child, zOrder, name);
        arrange();
    }
    else
        cocos2d::ui::Widget::addChild(child, zOrder, name);
}
