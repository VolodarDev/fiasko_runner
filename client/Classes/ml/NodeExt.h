/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __NodeExt_h__
#define __NodeExt_h__
#include "ml/ParamCollection.h"
#include "ml/common.h"
#include "ml/loadxml/xmlLoader.h"


struct PropertyLink
{
	std::string name;
	std::string target;
	std::string property;
};

class SmartScene;
class EventsList;

class NodeExt
{
public:
	virtual bool runEvent(const std::string & eventname);
	ActionPointer getAction(const std::string & name);
	virtual cocos2d::Node* as_node_pointer();

	cocos2d::Node * getChildByPath(const std::string & path_names);
	cocos2d::Node * getChildByPath(const std::list<int> path_tags);
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name);

	ParamCollection& getParamCollection();
	const ParamCollection& getParamCollection()const;
protected:
	NodeExt();
	virtual ~NodeExt();
	bool init();
	
public:
	virtual void load(const std::string & directory, const std::string & xmlFile)final;
	virtual void load(const std::string & pathToXmlFile)final;
	virtual void load(const pugi::xml_node & root);
	virtual void onLoaded();
public:
	virtual bool setProperty(int intproperty, const std::string & value);
	virtual bool setProperty(const std::string & stringproperty, const std::string & value);
	virtual bool loadXmlEntity(const std::string & tag, const pugi::xml_node & xmlnode);
	void loadActions(const pugi::xml_node & xmlnode);
	void loadEvents(const pugi::xml_node & xmlnode);
	void loadParams(const pugi::xml_node & xmlnode);
	void loadPropertyLinks(const pugi::xml_node & xmlnode);
    
    SmartScene* getSmartScene();
private:
	std::map<std::string, EventsList> _events;
	std::map<std::string, ActionPointer> _actions;
	std::map<std::string, PropertyLink> _propertyLinks;
	ParamCollection _params;
#if DEV == 1
    bool _loaded;
#endif
};

class LayerExt : public cocos2d::Layer, public NodeExt
{
	DECLARE_AUTOBUILDER(LayerExt);
	virtual bool init() override;
public:
	virtual void onEnter()override;

	virtual void appearance();
	virtual void disappearance(bool force = false);
    virtual void setOnBackButtonHandler(const std::function<void()>& callback);
    virtual bool setProperty(const std::string & stringproperty, const std::string & value) override;
protected:
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name)override;
private:
	bool _firstEntrance;
	CC_SYNTHESIZE(bool, _dispatchKeyBack, DispatchKeyBack);
    std::function<void()> _callbackOnKeyButton;
};

class NodeExt_ : public cocos2d::Node, public NodeExt
{
	DECLARE_AUTOBUILDER(NodeExt_);
	bool init() { return cocos2d::Node::init(); }
};

class SpriteExt : public cocos2d::Sprite, public NodeExt
{
	DECLARE_AUTOBUILDER(SpriteExt);
	bool init() { return cocos2d::Sprite::init(); }
};

class MenuExt : public cocos2d::Menu, public NodeExt
{
public:
	DECLARE_AUTOBUILDER(MenuExt);
	bool init() { return cocos2d::Menu::init(); }
};

#endif // #ifndef NodeExt
