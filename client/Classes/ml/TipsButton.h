/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#ifndef TipsButton_hpp
#define TipsButton_hpp

#include "ml/Button.h"

class TipsButton : public Button
{
    DECLARE_AUTOBUILDER(TipsButton);
public:
    virtual bool init() override;
    virtual void onEnter() override;
};


#endif /* TipsButton_hpp */
