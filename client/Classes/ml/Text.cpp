//
//  Text.cpp
//  cocos2d
//
//  Created by Vladimir Tolmachev on 20.10.2019.
//

#include "ml/Text.h"
#include "ml/Localization.h"

Text::Text()
{
}
Text::~Text()
{
    
}

bool Text::init()
{
    cocos2d::ui::Text::init();
    return true;
}

bool Text::setProperty(const std::string& name, const std::string& value)
{
    return NodeExt::setProperty(name, value);
}

void Text::setString(const std::string& text)
{
    cocos2d::ui::Text::setString(Localization::shared().locale(text));
    stretchNode(this, _stretch);
}

void Text::setStretch(Stretch stretch)
{
    _stretch = stretch;
    stretchNode(this, _stretch);
}
