/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#include "ml/TipsButton.h"

TipsButton::TipsButton()
{
}

TipsButton::~TipsButton()
{
}

bool TipsButton::init()
{
    Button::init();
    
    return true;
}

void TipsButton::onEnter()
{
    Button::onEnter();
    auto tips = cocos2d::Node::getChildByName("tips");
    if(tips)
    {
        tips->setVisible(false);
        
        addTouchEventListener([this](cocos2d::Ref*, const cocos2d::ui::Widget::TouchEventType& event){
            auto tips = cocos2d::Node::getChildByName("tips");
            if(event == cocos2d::ui::Widget::TouchEventType::BEGAN)
                tips->setVisible(true);
            else if(event == cocos2d::ui::Widget::TouchEventType::ENDED)
                tips->setVisible(false);
            else if(event == cocos2d::ui::Widget::TouchEventType::CANCELED)
                tips->setVisible(false);
        });
    }
    
}
