#include <stdio.h>
#include <sstream>
#include "ml/common.h"

using namespace cocos2d;

std::string boolToStr(bool value)
{
	return value ? "yes" : "no";
};

std::string intToStr(int value)
{
	static char buffer[32];
	buffer[0] = 0x0;
	sprintf(buffer, "%d", value);
	
	return buffer;
};

std::string floatToStr(float value)
{
	static char buffer[32];
	buffer[0] = 0x0;
	sprintf(buffer, "%.2f", value);
	return buffer;
};

std::string floatToStr2(float value)
{
	static char buffer[32];
	buffer[0] = 0x0;
	sprintf(buffer, "%f", value);
	return buffer;
};

bool strToBool(const std::string & value)
{
	if (value.empty())
		return false;
	bool result(false);
	result = result || value == "yes";
	result = result || value == "Yes";
	result = result || value == "true";
	result = result || value == "True";
	return result;
}

int strToInt(const std::string & value)
{
	return value.empty() ? 0 :
		atoi(value.c_str());
}

cocos2d::Point strToPoint(const std::string & value)
{
	cocos2d::Size frame;

	if (Director::getInstance()->getOpenGLView())
		frame = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();

	std::string string = value;

	cocos2d::Point add;
	size_t addk = string.find("add:");
	if (addk != std::string::npos)
	{
		assert(addk != 0);
		auto s = string.substr(addk + 4);
		add = strToPoint(s);
		string = string.substr(0, addk);
	}

	size_t framek = string.find("frame:");
	if (framek == 0)
	{
		string = string.substr(framek + 6);
		cocos2d::Point point = strToPoint(string);
		point.x *= frame.width;
		point.y *= frame.height;
		return point + add;
	}

	size_t rb = string.find("right:");
	if (rb == 0)
	{
		string = string.substr(rb + 6);
		cocos2d::Point point = strToPoint(string);
		point.x = frame.width + point.x;
		return point + add;
	}
	size_t lt = string.find("top:");
	if (lt == 0)
	{
		string = string.substr(lt + 4);
		cocos2d::Point point = strToPoint(string);
		point.y = frame.height + point.y;
		return point + add;
	}
	size_t rt = string.find("righttop:");
	if (rt == 0)
	{
		string = string.substr(rt + 9);
		cocos2d::Point point = strToPoint(string);
		point.x = frame.width + point.x;
		point.y = frame.height + point.y;
		return point + add;
	}
	size_t hb = string.find("halfbottom:");
	if (hb == 0)
	{
		string = string.substr(hb + 11);
		cocos2d::Point point = strToPoint(string);
		point.x = frame.width / 2 + point.x;
		return point + add;
	}
	size_t ht = string.find("halftop:");
	if (ht == 0)
	{
		string = string.substr(ht + 8);
		cocos2d::Point point = strToPoint(string);
		point.x = frame.width / 2 + point.x;
		point.y = frame.height + point.y;
		return point + add;
	}
	size_t lh = string.find("lefthalf:");
	if (lh == 0)
	{
		string = string.substr(lh + 9);
		cocos2d::Point point = strToPoint(string);
		point.y = frame.height / 2 + point.y;
		return point + add;
	}
	size_t rh = string.find("righthalf:");
	if (rh == 0)
	{
		string = string.substr(rh + 10);
		cocos2d::Point point = strToPoint(string);
		point.x = frame.width + point.x;
		point.y = frame.height / 2 + point.y;
		return point + add;
	}

	size_t k = string.find("x");
	if (k == std::string::npos)
		return cocos2d::Point(0, 0) + add;

	cocos2d::Point p;
	p.x = strToFloat(string.substr(0, k));
	p.y = strToFloat(string.substr(k + 1));

	return p + add;
}


const std::string pointToStr(const cocos2d::Point & point)
{
	return floatToStr(point.x) + "x" + floatToStr(point.y);
}

const std::string pointToStrRound(const cocos2d::Point & point)
{
    return intToStr(point.x) + "x" + intToStr(point.y);
}

cocos2d::Size strToSize(const std::string & value)
{
	cocos2d::Point p = strToPoint(value);
	return cocos2d::Size(p);
}

const std::string sizeToStr(const cocos2d::Size & size)
{
	return floatToStr(size.width) + "x" + floatToStr(size.height);
}

cocos2d::Rect strToRect(const std::string & value)
{
	cocos2d::Rect rect;

	auto k = value.find(",");
	if (k != std::string::npos)
	{
		auto origin = value.substr(0, k);
		auto size = value.substr(k + 1);
		rect.origin = strToPoint(origin);
		rect.size = cocos2d::Size(strToPoint(size));
	}
	return rect;
}

const std::string rectToStr(const cocos2d::Rect & rect)
{
	std::string result = pointToStr(rect.origin) + "," + pointToStr(cocos2d::Point(rect.size.width, rect.size.height));
	return result;
}

cocos2d::Color3B strToColor3B(const std::string & value)
{
    assert(value.empty() || value.size() == 6);
    if (value.empty()) return cocos2d::Color3B::WHITE;
    
    const std::string r = value.substr(0, 2);
    const std::string g = value.substr(2, 2);
    const std::string b = value.substr(4, 2);
    int R, G, B;
    sscanf(r.c_str(), "%x", &R);
    sscanf(g.c_str(), "%x", &G);
    sscanf(b.c_str(), "%x", &B);
    
    return cocos2d::Color3B(GLubyte(R), GLubyte(G), GLubyte(B));
}

std::string color3BToStr(const cocos2d::Color3B& value)
{
    std::string out = "FFFFFF";
    sprintf(&out[0], "%02x%02x%02x", value.r, value.g, value.b);
    return out;
}

cocos2d::Color4B strToColor4B(const std::string & value)
{
	assert(value.empty() || value.size() == 8);
	if (value.empty()) return cocos2d::Color4B::WHITE;

	const std::string r = value.substr(0, 2);
	const std::string g = value.substr(2, 2);
	const std::string b = value.substr(4, 2);
	const std::string a = value.substr(6, 2);
	int R, G, B, A;
	sscanf(r.c_str(), "%x", &R);
	sscanf(g.c_str(), "%x", &G);
	sscanf(b.c_str(), "%x", &B);
	sscanf(a.c_str(), "%x", &A);

	return cocos2d::Color4B(GLubyte(R), GLubyte(G), GLubyte(B), GLubyte(A));
}

std::string color4BToStr(const cocos2d::Color4B& value)
{
    std::string out = "FFFFFFFF";
    sprintf(&out[0], "%02x%02x%02x%02x", value.r, value.g, value.b, value.a);
    return out;
}

cocos2d::BlendFunc strToBlendFunc(const std::string & value)
{
	if (0);
	else if (value == "additive")return BlendFunc::ADDITIVE;
	else if (value == "disable")return BlendFunc::DISABLE;
	else if (value == "alphapremultiplied")return BlendFunc::ALPHA_PREMULTIPLIED;
	else if (value == "alphanonpremultiplied")return BlendFunc::ALPHA_NON_PREMULTIPLIED;
//	else CCLOG("Warning: strToBlendFunc not know blending by string [%s]", value.c_str());
	
	std::vector<std::string> out;
	split(out, value);
	if(out.size() != 2)
		return BlendFunc::DISABLE;
	
	auto src = out[0];
	auto dst = out[1];
	BlendFunc blend = BlendFunc::DISABLE;
	
#define check(m, s) else if(m == #s) blend.m = s
	
	if(0){}
	check(src, GL_ZERO);
	check(src, GL_ONE);
	check(src, GL_SRC_COLOR);
	check(src, GL_ONE_MINUS_SRC_COLOR);
	check(src, GL_SRC_ALPHA);
	check(src, GL_ONE_MINUS_SRC_ALPHA);
	check(src, GL_DST_ALPHA);
	check(src, GL_ONE_MINUS_DST_ALPHA);
	check(src, GL_DST_COLOR);
	check(src, GL_ONE_MINUS_DST_COLOR);
	check(src, GL_SRC_ALPHA_SATURATE);

	if(0){}
	check(dst, GL_ZERO);
	check(dst, GL_ONE);
	check(dst, GL_SRC_COLOR);
	check(dst, GL_ONE_MINUS_SRC_COLOR);
	check(dst, GL_SRC_ALPHA);
	check(dst, GL_ONE_MINUS_SRC_ALPHA);
	check(dst, GL_DST_ALPHA);
	check(dst, GL_ONE_MINUS_DST_ALPHA);
	check(dst, GL_DST_COLOR);
	check(dst, GL_ONE_MINUS_DST_COLOR);
	check(dst, GL_SRC_ALPHA_SATURATE);
	
#undef check
	return blend;
}

std::string blendFuncToStr(const cocos2d::BlendFunc & blendFunc)
{
	if (0);
	else if (blendFunc == BlendFunc::ADDITIVE) return "additive";
	else if (blendFunc == BlendFunc::DISABLE) return "disable";
	else if (blendFunc == BlendFunc::ALPHA_PREMULTIPLIED) return "alphapremultiplied";
	else if (blendFunc == BlendFunc::ALPHA_NON_PREMULTIPLIED) return "alphanonpremultiplied";
	return "";
}

float strToFloat(const std::string & value)
{
	std::string::size_type k = value.find("..");
	if (k != std::string::npos)
	{
		const float l = strToFloat(value.substr(0, k));
		const float r = strToFloat(value.substr(k + 2));
		const float v = static_cast<float>(CCRANDOM_0_1() * (r - l) + l);
		assert(l <= r);
		assert(v >= l && v <= r);
		return v;
	}
	std::stringstream ss(value);
	float result(0);
	if (value.empty() == false)
		ss >> result;
	return result;
}
