//
//  AudioEngine.h
//  TowerDefence
//
//  Created by Vladimir Tolmachev on 04.02.14.
//  Edited by Vladimir Tolmachev on 2019.
//

#ifndef __ml_AudioEngine__
#define __ml_AudioEngine__

#include <list>
#include <string>
#include "ml/Singlton.h"

class AudioEngine : public Singlton<AudioEngine>
{
	friend class Singlton<AudioEngine>;
    friend class TestAudioEngine;
public:
	virtual ~AudioEngine();
public:
	void enableAudio();
	void disableAudio();
    bool isAudioEnabled();
	
	void playMusic(const std::string & path);
	int  playEffect(const std::string & path, bool lopped = false);
	void stopEffect(int id);
	void pauseEffect(int id);
	void resumeEffect(int id);
	
	void stopMusic();
	
	void stopAll();
	void pauseAll();
	void resumeAll();
private:
	AudioEngine();
	virtual void onCreate();
private:
	std::string _currentMusic;
	bool _audioEnabled;
	std::list<int> _effectIDs;
	int _backgroundMusicID;
	
	int _pauseCounter;
};


#endif /* defined(__TowerDefence__AudioEngine__) */
