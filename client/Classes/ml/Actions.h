//
//  Actions.hpp
//  cocos2d
//
//  Created by Vladimir Tolmachev on 05/01/2020.
//

#ifndef Actions_hpp
#define Actions_hpp

#include "cocos2d.h"
#include "ml/macroses.h"

namespace cocos2d
{
    namespace ui
    {
        class Text;
    }
}

class ActionText: public cocos2d::ActionInterval
{
    DECLARE_AUTOBUILDER(ActionText);
    bool init(float duration, float endValue, bool floatTruncation = true, const std::string& prefix = "", const std::string& postfix = "");
    
public:
    virtual void startWithTarget(cocos2d::Node* target) override;
    virtual void update(float t) override;
    virtual cocos2d::ActionInterval* reverse() const override;
    virtual cocos2d::ActionInterval* clone() const override;
    
private:
    bool _floatTruncation;
    float _startValue;
    float _endValue;
    std::string _prefix;
    std::string _postfix;
};

class ActionEnable : public cocos2d::ActionInstant
{
private:
    ActionEnable() {}
    virtual ~ActionEnable() {}
public:
    static ActionEnable* create();

    virtual void update(float time) override;
    virtual ActionInstant* reverse() const override;
    virtual ActionInstant* clone() const override;
private:
    CC_DISALLOW_COPY_AND_ASSIGN(ActionEnable);
};

class ActionDisable : public cocos2d::ActionInstant
{
private:
    ActionDisable() {}
    virtual ~ActionDisable() {}
public:
    static ActionDisable* create();

    virtual void update(float time) override;
    virtual ActionInstant* reverse() const override;
    virtual ActionInstant* clone() const override;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(ActionDisable);
};

class ActionSetProperty : public cocos2d::ActionInstant
{
private:
    ActionSetProperty() {}
    virtual ~ActionSetProperty() {}
public:
    static ActionSetProperty* create(const std::string& propertyName, const std::string& propertyValue);

    virtual void update(float time) override;
    virtual ActionInstant* reverse() const override;
    virtual ActionInstant* clone() const override;

private:
    CC_DISALLOW_COPY_AND_ASSIGN(ActionSetProperty);
    
    std::string _property;
    std::string _value;
};

class ActionType : public cocos2d::ActionInterval
{
private:
    ActionType();
    virtual ~ActionType();
    bool init(float duration, const std::string& text) ;
    void updateString(float time);
public:
    static ActionType* create(float duration, const std::string& text);

    virtual ActionInterval* reverse() const override;
    virtual ActionInterval* clone() const override;

    virtual void startWithTarget(cocos2d::Node* target) override;
    virtual void update(float time) override;
private:
    CC_DISALLOW_COPY_AND_ASSIGN(ActionType);
private:
    IntrusivePtr<cocos2d::Label> _label;
    IntrusivePtr<cocos2d::ui::Text> _text;
    std::string _sourceText;
    std::u16string _localizedText;
};

#endif /* Actions_hpp */
