/******************************************************************************/
/*
 * Copyright 2014-2019 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "ml/Layout.h"

using namespace cocos2d;

Layout::Layout()
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
: _backFill(nullptr)
#endif
{
    
}

Layout::~Layout()
{
    
}

bool Layout::init()
{
    cocos2d::ui::Layout::init();
    NodeExt::init();
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    scheduleUpdate();
#endif
    return true;
}

#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
void Layout::update(float dt)
{
    if(_backFill)
    {
        _backFill->setContentSize(getContentSize() - Size(2,2));
    }
}
#endif

bool Layout::setProperty(const std::string& name, const std::string& value)
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
    if(name == "show_backfill")
    {
        if(strTo<bool>(value))
        {
            if(!_backFill)
            {
                _backFill = cocos2d::Sprite::create("point.png");
                _backFill->setAnchorPoint(Point::ZERO);
                _backFill->setContentSize(getContentSize() - Size(2,2));
                _backFill->setOpacity(128);
                _backFill->setPosition(1, 1);
                addChild(_backFill);
            }
        }
        else
        {
            if(_backFill)
            {
                removeChild(_backFill);
                _backFill = nullptr;
            }
        }
    }
#else
    if(0);
#endif
    else
    {
        return NodeExt::setProperty(name, value);
    }
    return true;
}

