/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "ml/mlObjectFactory.h"
#include "ml/Button.h"
#include "ml/DropDownMenu.h"
#include "ml/EditBox.h"
#include "ml/Events.h"
#include "ml/FillSprite.h"
#include "ml/Layout.h"
#include "ml/NodeExt.h"
#include "ml/PageView.h"
#include "ml/ScrollLayer.h"
#include "ml/ScrollViewVertical.h"
#include "ml/Text.h"

#define EXT(name, base) \
    class name : public base, public NodeExt \
    { \
        DECLARE_AUTOBUILDER(name); \
        virtual bool init() override { return base::init() && NodeExt::init(); } \
    }; \
    name::name(){}\
    name::~name(){}


EXT(uiScrollView, cocos2d::ui::ScrollView);
EXT(uiImage, cocos2d::ui::ImageView);


mlObjectFactory::mlObjectFactory()
{
	book_pointer<cocos2d::ProgressTimer>("progresstimer");
	book<cocos2d::Scene>("scene");
    book<cocos2d::Node>("node");
	book<cocos2d::Sprite>("sprite");
	book<cocos2d::Menu>("menu");
	book<cocos2d::Layer>("layer");
	book<cocos2d::ParticleSystemQuad>("particle");
	book<cocos2d::ParallaxNode>("paralax");
	
	book<uiImage>("ui_image");
	book<uiScrollView>("ui_scroll");
	book<Button>("ui_button");
	book<Layout>("ui_layout");
	book<cocos2d::ui::Slider>("ui_slider");
	book<Text>("ui_text");
	book<cocos2d::ui::TextField>("ui_textfield");
    book<EditBox>("ui_editbox");
	book<PageView>("ui_pageview");
    book<DropDownMenu>("ui_drop_down_menu");
    book<ScrollViewVertical>("scroll_vertical");

	book<LayerExt>("layerext");
	book<NodeExt_>("nodeext");
	book<MenuExt>("menuext");
	book<SpriteExt>("spriteext");
    book<ScrollLayer>("scrolllayer");
    book<FillSprite>("fill");

	book<EventAction>("action");
	book<EventRunAction>("runaction");
	book<EventStopAction>("stopaction");
	book<EventStopAllAction>("stopallaction");
    book<EventStopAllAction>("stopallactions");
	book<EventSetProperty>("setproperty");
	book<EventPlaySound>("playsound");
	book<EventScene>("scenestack");
	book<EventCreateNode>("createnode");
}

IntrusivePtr<cocos2d::Ref> mlObjectFactory::build(const std::string & key)
{
	if (key == "progresstimer")
	{
		return cocos2d::ProgressTimer::create(cocos2d::Sprite::create());
	}
	bool isreg = m_objects.find(key) != m_objects.end();
	if(!isreg)
		CCLOG("mlObjectFactory say: type [%s] not registred", key.c_str());
	assert(isreg);
	return isreg ? m_objects[key]->build() : nullptr;
};
