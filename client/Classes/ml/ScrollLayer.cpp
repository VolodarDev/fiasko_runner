/*
 *  Copyright 2018-2020 Vladimir Tolmachev
 *
 *   Author: Vladimir Tolmachev
 *  Project: Dungeon: Age of Heroes
 *   e-mail: tolm_vl@hotmail.com
 *
 * If you received the code is not the author, please contact me
 */
#include "ml/ScrollLayer.h"

using namespace cocos2d;


ScrollLayer::ScrollLayer()
: _state(State::wait)
, _wasAction(false)
, _touchA(nullptr)
, _touchB(nullptr)
, _touchDistance(0)
, _distanceOnStartScroll(0.f)
, _bottomBorder(0.f)
, _maxScale(2.f)
, _minScale(0.f)
, _scale(1.f)
, _accelerateVelocity(0.f)
{
}

ScrollLayer::~ScrollLayer()
{}

bool ScrollLayer::init()
{
	do
	{
		CC_BREAK_IF(!Layer::init());

		_scale = getScale();
		_position = getPosition();

        enableEventTouches();

		auto listener = EventListenerMouse::create();
		listener->onMouseScroll = [this](cocos2d::Event* event) {
			EventMouse* mouseEvent = dynamic_cast<EventMouse*>(event);
            assert(mouseEvent);
			float z = mouseEvent->getScrollY();
			cocos2d::Point p = mouseEvent->getLocation();
			p.y = Director::getInstance()->getOpenGLView()->getDesignResolutionSize().height - p.y;

			float scale = getScale();
			scale += z *0.2f;
			setScroll(scale, p);
		};
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

		return true;
	} while (false);
	return false;
}

bool ScrollLayer::setProperty(const std::string & name, const std::string & value)
{
	if (name == "contentsize")
		setContent(strTo<cocos2d::Size>(value));
	else if (name == "visiblesize")
		setVisibled(strTo<cocos2d::Size>(value));
    else if (name == "maxscale")
        setMaxScale(strTo<float>(value));
    else if (name == "minscale")
        setMinScale(strTo<float>(value));
	else
		return LayerExt::setProperty(name, value);
	return true;
}

void ScrollLayer::setScale(float scale)
{
    Node::setScale(scale);
    _scale = scale;
}

void ScrollLayer::setScale(float scaleX, float scaleY)
{
    assert(scaleX == scaleY);
    Node::setScale(scaleX, scaleY);
    _scale = scaleX;
}

void ScrollLayer::setContent(const cocos2d::Size& size)
{
	_content = cocos2d::Point(size);
}

void ScrollLayer::setVisibled(const cocos2d::Size& size)
{
	_visibled = cocos2d::Point(size);
	setContentSize(size);
}

void ScrollLayer::setMaxScale(float scale)
{
	_maxScale = scale;
}

void ScrollLayer::setMinScale(float scale)
{
	_minScale = scale;
}

void ScrollLayer::setBottomBorder(float border)
{
	_bottomBorder = border;
}

float ScrollLayer::getMaxScale()const
{
    return _maxScale;
}

float ScrollLayer::getMinScale()const
{
    return _minScale;
}

void ScrollLayer::update(float dt)
{
	accelerate(dt);
	setPosition(_position);
	setScale(_scale);
}

void ScrollLayer::accelerate(float dt)
{
	if(_accelerateVelocity <= 0.f)
		return;
	
	if(_state == State::wait)
	{
		static float down = 6;
		auto offset = _accelerateDirection * _accelerateVelocity * dt;
		_accelerateVelocity -= down;
		
		_position += offset;
		setScroll(_scale, _position);
	}
}

void ScrollLayer::stopAccelerate()
{
	_accelerateVelocity = 0.f;
}

void ScrollLayer::accelerateOnTouchMove(const cocos2d::Vec2& offset)
{
	_accelerateVelocity = offset.getLength() * 15;
	_accelerateDirection = offset.getNormalized();
}

void ScrollLayer::addTouchable(NodePointer node, const std::function<void()>& callback)
{
	assert(node && callback);
	_touchableNodes.emplace_back(node, callback);
}

bool ScrollLayer::checkTouchableNode(const cocos2d::Point& touchLocation)
{
	if(_positionOnTouchBegan.getDistance(_position) > 50)
		return false;
	
	for(auto& pair : _touchableNodes)
	{
		auto node = pair.first;
		
//		bool visibled = isVisible();
//		cocos2d::Node* parent = node;
//		while((parent = parent->getParent()) != nullptr)
//			visibled = visibled && parent->isVisible();
//		if(!visibled)
//			continue;
		
		Vec2 && touchPoint = PointApplyAffineTransform(touchLocation, node->getWorldToNodeAffineTransform());
		cocos2d::Size size = node->getContentSize();
		
		bool touched = touchPoint.x > 0 && touchPoint.x < size.width && touchPoint.y > 0 && touchPoint.y < size.height;
		if(touched)
		{
			pair.second();
			return true;
		}
	}
	return false;
}

void ScrollLayer::onEnter()
{
	Layer::onEnter();
	scheduleUpdate();
	setScroll(_scale, _position);
	_state = State::wait;
}

void ScrollLayer::onExit()
{
	Layer::onExit();
	_state = State::wait;
	_touchA.reset(nullptr);
	_touchB.reset(nullptr);
}

void ScrollLayer::enableEventTouches()
{
    _touchListener = EventListenerTouchAllAtOnce::create();
    _touchListener->onTouchesBegan = CC_CALLBACK_2(ScrollLayer::onTouchesBegan, this);
    _touchListener->onTouchesMoved = CC_CALLBACK_2(ScrollLayer::onTouchesMoved, this);
    _touchListener->onTouchesEnded = CC_CALLBACK_2(ScrollLayer::onTouchesEnded, this);
    _touchListener->onTouchesCancelled = CC_CALLBACK_2(ScrollLayer::onTouchesCancelled, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_touchListener, this);
}

bool ScrollLayer::disableEventTouches()
{
    _eventDispatcher->removeEventListener(_touchListener);
    _touchListener = nullptr;
    return true;
}

bool ScrollLayer::isEventTouchesEnabled()const
{
    return _touchListener != nullptr;
}

void ScrollLayer::onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event *event)
{
	touchesBegan(touches);
}

void ScrollLayer::onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event *event)
{
	touchesMoved(touches);
}

void ScrollLayer::onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event *event)
{
	touchesEnded(touches);
}

void ScrollLayer::onTouchesCancelled(const std::vector<cocos2d::Touch*>&touches, cocos2d::Event *event)
{
	touchesCancelled(touches);
}


bool ScrollLayer::touchesBegan(const std::vector<Touch*>& touches)
{
	for (auto touch : touches)
	{
		switch (_state)
		{
			case State::wait:
				if (!_touchA)
				{
					_touchA = touch;
					_startTouchA = touch->getLocation();
				}
				else if (!_touchB)
				{
					_touchB = touch;
					_startTouchB = touch->getLocation();
				}
				_state = State::scroll;
				_positionOnTouchBegan = _position;
				_wasAction = false;
				break;
			case State::scroll:
				if (!_touchA)
				{
					_touchA = touch;
				}
				else if (!_touchB)
				{
					_touchB = touch;
				}
				assert(_touchA && _touchB);
				_startTouchA = _touchA->getLocation();
				_startTouchB = _touchB->getLocation();
				_touchDistance = _startTouchB.getDistance(_startTouchA);
				_distanceOnStartScroll = _touchDistance;
				_state = State::scaling;
				break;
			default:
				break;
		}
	}
	return _wasAction;
}

bool ScrollLayer::touchesMoved(const std::vector<Touch*>& touches)
{
	switch (_state)
	{
		case State::scroll:
		{
			auto touch = touches[0];
			
			auto delta = touch->getDelta();
			
			_position += delta;
			setScroll(_scale, _position);
			accelerateOnTouchMove(delta);
			
			_wasAction = _wasAction || _positionOnTouchBegan.getDistance(_position) > 50;
			break;
		}
		case State::scaling:
		{
			if (_touchA && _touchB)
			{
				auto locA = _touchA->getLocation();
				auto locB = _touchB->getLocation();
				auto center = (locA + locB) / 2;
				
				float distance = locA.getDistance(locB);
                float ds = _touchDistance > 0 ? distance / _touchDistance : 0;
				_touchDistance = distance;
				float scale = _scale;
				scale *= ds;
				setScroll(scale, center);
				
				_wasAction = _wasAction || std::fabs(_distanceOnStartScroll - distance) > 50;
				break;
			}
		}
		default:
			break;
	}
	return _wasAction;
}

bool ScrollLayer::touchesEnded(const std::vector<Touch*>& touches)
{
	for (auto touch : touches)
	{
		switch (_state)
		{
			case State::scroll:
				_touchA.reset(nullptr);
				_touchB.reset(nullptr);
				_state = State::wait;
				checkTouchableNode(touch->getLocation());
				break;
			case State::scaling:
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
				_touchA.reset(nullptr);
				_touchB.reset(nullptr);
				_state = State::wait;
#else
				if (touch == _touchA || (_touchA && _touchA->getID() == touch->getID()))
				{
					_touchA.reset(nullptr);
					_state = State::scroll;
				}
				else if (touch == _touchB || (_touchB && _touchB->getID() == touch->getID()))
				{
					_touchB.reset(nullptr);
					_state = State::scroll;
				}
#endif
				break;
			default:
				break;
		}
	}
	return _wasAction;
}

void ScrollLayer::touchesCancelled(const std::vector<Touch*>& touches)
{
	_state = State::wait;
	_touchA.reset(nullptr);
	_touchB.reset(nullptr);
}

const Vec2& ScrollLayer::getScrollPosition()const
{
	return _position;
}

void ScrollLayer::setScrollPosition(const Vec2& position)
{
	_position = fitPosition(position);
}

void ScrollLayer::setScroll(float newscale, const Vec2& anchor)
{
	float scale = _scale;
	float fitscale = fitScale(newscale);
	Vec2 position = _position - anchor;

	position *= 1.f / scale;
	position *= fitscale;
	position += anchor;
	setScale(fitscale);
	position = fitPosition(position);
	setScale(scale);

	_position = position;
	_scale = fitscale;
}

Vec2 ScrollLayer::fitPosition(const Vec2& position)const
{
	float scale = getScale();
	auto min = Vec2(
		_visibled.x - _content.x * scale,
		_visibled.y - _content.y * scale + _bottomBorder
	);
	auto max = Vec2::ZERO;

	auto pos = position;
	pos.x = std::max(pos.x, min.x);
	pos.x = std::min(pos.x, max.x);
	pos.y = std::max(pos.y, min.y);
	pos.y = std::min(pos.y, max.y);
	return pos;
}

float ScrollLayer::fitScale(float scale)const
{
	float minx = _visibled.x / _content.x;
	float miny = _visibled.y / _content.y;
	scale = std::max(scale, minx);
	scale = std::max(scale, miny);
	scale = std::max(scale, _minScale);
	scale = std::min(scale, _maxScale);
	return scale;
}

void ScrollLayer::scrollToPosition(const cocos2d::Point& childPosition, float duration)
{
	_accelerateVelocity = 0.f;
	
	auto position = -childPosition;
	auto scale = getScale();
	position = position *= scale;
	auto frame = getContentSize();
	position += frame / 2;
	if(duration <= 0)
	{
		setScrollPosition(position);
	}
	else
	{
		auto from = _position;
		auto to = position;
		auto callback = [this, from, to](float t)
		{
			auto pos = from + (to-from) * t;
			setScrollPosition(pos);
		};
		auto action = ActionFloat::create(duration, 0, 1, callback);
		auto ease = EaseInOut::create(action, 2);
		runAction(ease);
	}
}
