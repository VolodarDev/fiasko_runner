/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#include "ml/CallStack.h"
#include "cocos2d.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#import "Foundation/Foundation.h"
std::string show_call_stack()
{
    NSLog(@"Stack trace : %@",[NSThread callStackSymbols]);
    id string = [[NSThread callStackSymbols] componentsJoinedByString:@"\n"];
    return [string UTF8String];
}

#else

std::string show_call_stack()
{
    return "No";
}

#endif
