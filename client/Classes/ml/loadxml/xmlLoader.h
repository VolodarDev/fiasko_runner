/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __xmlLoader_h__
#define __xmlLoader_h__
#include "ml/loadxml/xmlProperties.h"
#include "ml/pugixml/pugixml.hpp"


class ParamCollection;

class EventBase;

namespace xmlLoader
{
	typedef std::shared_ptr<pugi::xml_document> XmlDocPointer;
	XmlDocPointer loadDoc(const std::string& path);
	XmlDocPointer loadDoc(const char* path);
	void saveDoc(const std::string& file, const XmlDocPointer& doc);

	namespace macros
	{
		const std::string delimiter_l("@{");
		const std::string delimiter_r("}");

		void set(const std::string & name, const std::string & value);
		void set(const std::vector<std::string>& values);
		const std::string get(const std::string & name);
		void clear();
		void erase(const std::string & name);

		const std::string parse(const std::string & string);
        void log();
	}

	NodePointer load_node(const std::string & file, const std::string& topType = "", int depth = 0);
	NodePointer load_node(const char* file, const std::string& topType = "", int depth = 0);
	NodePointer load_node(pugi::xml_node xmlnode, const std::string& topType = "", int depth = 0);
	void load(cocos2d::Node* node, pugi::xml_node xmlnode, int depth = 0);
	void load(cocos2d::Node* node, const std::string & path, int depth = 0);
	void load(cocos2d::Node* node, const char* path, int depth = 0);
	void load_children(cocos2d::Node* node, pugi::xml_node xmlnode, int depth = 0);

	template <class T>
	IntrusivePtr<T> load_node(const std::string & file, int depth=0)
	{
		auto node = load_node(file, "", depth);
		IntrusivePtr<T> result;
		result.reset(dynamic_cast<T*>(node.ptr()));
		return result;
	};
	template <class T>
	IntrusivePtr<T> load_node(const char* file);

	template <class T>
	IntrusivePtr<T> load_node(const pugi::xml_node & xmlnode, int depth=0)
	{
		auto node = load_node(xmlnode, "", depth);
		IntrusivePtr<T> result;
		result.reset(dynamic_cast<T*>(node.ptr()));
		return result;
	};
	IntrusivePtr<EventBase> load_event(const pugi::xml_node xmlnode);

	ActionPointer load_action(const pugi::xml_node xmlnode);
	ActionPointer load_action_from_file(const std::string & path);
	ActionPointer load_action_from_file(const char* path);
	ActionPointer load_action(const std::string & desc);

	void load_paramcollection(ParamCollection& params, const pugi::xml_node xmlnode);

    bool tryLoadPlist(const std::string& image);

	namespace k
	{
        extern const std::string ActionSequence;
        extern const std::string ActionSpawn;
        extern const std::string ActionDelayTime;
        extern const std::string ActionScaleTo;
        extern const std::string ActionScaleBy;
        extern const std::string ActionSkewTo;
        extern const std::string ActionSkewBy;
        extern const std::string ActionMoveTo;
        extern const std::string ActionMoveBy;
        extern const std::string ActionRotateTo;
        extern const std::string ActionRotateBy;
        extern const std::string ActionJumpTo;
        extern const std::string ActionJumpBy;
        extern const std::string ActionBlink;
        extern const std::string ActionFadeTo;
        extern const std::string ActionFadeIn;
        extern const std::string ActionFadeOut;
        extern const std::string ActionTintTo;
        extern const std::string ActionTintBy;
        extern const std::string ActionRepeatForever;
        extern const std::string ActionRepeat;
        extern const std::string ActionEaseIn;
        extern const std::string ActionEaseOut;
        extern const std::string ActionEaseInOut;
        extern const std::string ActionBounceIn;
        extern const std::string ActionBounceOut;
        extern const std::string ActionBounceInOut;
        extern const std::string ActionBackIn;
        extern const std::string ActionBackOut;
        extern const std::string ActionBackInOut;
        extern const std::string ActionSineIn;
        extern const std::string ActionSineOut;
        extern const std::string ActionSineInOut;
        extern const std::string ActionBezier;
        extern const std::string ActionAnimate;
        extern const std::string ActionRemoveSelf;
        extern const std::string ActionText;
        extern const std::string ActionShow;
        extern const std::string ActionHide;
        extern const std::string ActionEnable;
        extern const std::string ActionDisable;
        extern const std::string ActionSetProperty;

		namespace xmlTag
		{
			const std::string ParamCollection("paramcollection");
			const std::string Link("link");
			const std::string PropertyLinks("propertylinks");
		};
	}
};

#endif
