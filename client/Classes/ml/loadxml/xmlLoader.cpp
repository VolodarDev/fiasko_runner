/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "../NodeExt.h"
#include "../mlObjectFactory.h"
#include "ml/Animation.h"
#include "ml/Events.h"
#include "ml/Actions.h"

using namespace cocos2d;

#ifdef BOOL 
#	undef BOOL
#endif

namespace xmlLoader
{
    namespace k
    {
        const std::string ActionSequence("Sequence");
        const std::string ActionSpawn("Spawn");
        const std::string ActionDelayTime("DelayTime");
        const std::string ActionScaleTo("ScaleTo");
        const std::string ActionScaleBy("ScaleBy");
        const std::string ActionSkewTo("SkewTo");
        const std::string ActionSkewBy("SkewBy");
        const std::string ActionMoveTo("MoveTo");
        const std::string ActionMoveBy("MoveBy");
        const std::string ActionRotateTo("RotateTo");
        const std::string ActionRotateBy("RotateBy");
        const std::string ActionJumpTo("JumpTo");
        const std::string ActionJumpBy("JumpBy");
        const std::string ActionBlink("Blink");
        const std::string ActionFadeTo("FadeTo");
        const std::string ActionFadeIn("FadeIn");
        const std::string ActionFadeOut("FadeOut");
        const std::string ActionTintTo("TintTo");
        const std::string ActionTintBy("TintBy");
        const std::string ActionRepeatForever("RepeatForever");
        const std::string ActionRepeat("Repeat");
        const std::string ActionEaseIn("EaseIn");
        const std::string ActionEaseOut("EaseOut");
        const std::string ActionEaseInOut("EaseInOut");
        const std::string ActionBounceIn("BounceIn");
        const std::string ActionBounceOut("BounceOut");
        const std::string ActionBounceInOut("BounceInOut");
        const std::string ActionBackIn("BackIn");
        const std::string ActionBackOut("BackOut");
        const std::string ActionBackInOut("BackInOut");
        const std::string ActionSineIn("SineIn");
        const std::string ActionSineOut("SineOut");
        const std::string ActionSineInOut("SineInOut");
        const std::string ActionBezier("Bezier");
        const std::string ActionAnimate("Animate");
        const std::string ActionRemoveSelf("RemoveSelf");
        const std::string ActionText("Text");
        const std::string ActionShow("Show");
        const std::string ActionHide("Hide");
        const std::string ActionEnable("Enable");
        const std::string ActionDisable("Disable");
        const std::string ActionSetProperty("SetProperty");
    }

	const bool __logging = false;

	std::map<std::string, XmlDocPointer > _cache;

	XmlDocPointer loadDoc(const std::string& path)
	{
		auto doc = std::make_shared<pugi::xml_document>();
		doc->load_file(path.c_str());
		return doc;
	}
	
	void saveDoc(const std::string& file, const XmlDocPointer& doc)
	{
#if DEV == 1
		unsigned int flag = pugi::format_no_declaration | pugi::format_indent;
#else
        unsigned int flag = pugi::format_raw | pugi::format_no_escapes | pugi::format_no_declaration;
#endif
		if(file.empty() == false)
		{
			std::stringstream ss;
			doc->save(ss, "    ", flag);
			auto str = ss.str();
			FileUtils::getInstance()->writeStringToFile(str, file);
		}
	}

	NodePointer load_child(cocos2d::Node* node, pugi::xml_node xmlnode, int depth);

	NodePointer load_node(const std::string & file, const std::string& topType, int depth)
	{
		if(__logging) CCLOG("load node from doc [%s]", file.c_str());
		auto doc = loadDoc(file);
		auto root = doc->root().first_child();
		NodePointer result = root ? load_node(root, topType, depth) : nullptr;
		return result;
	}

	IntrusivePtr<cocos2d::Node> load_node(pugi::xml_node xmlnode, const std::string& topType, int depth)
	{
		if(!xmlnode)
		{
			if(__logging) CCLOG("warning: try loading node from emptt xmlnode");
		}

		ParamCollection macroses(xmlnode.attribute("macroses").as_string());
		xmlnode.remove_attribute("macroses");
		for(auto pair : macroses)
			macros::set(pair.first, pair.second);

		const std::string& type = topType.empty() ? macros::parse(xmlnode.attribute(ksType.c_str()).as_string()) : topType;
		const std::string& template_ = macros::parse(xmlnode.attribute(ksTemplate.c_str()).as_string());
        if(xmlnode.attribute("validate"))
            return nullptr;
		
		IntrusivePtr<cocos2d::Node> result;
		if(template_.empty())
		{
			if(type.empty() == false)
			{
				if(__logging) CCLOG("try create object [%s]", type.c_str());
				result = mlObjectFactory::shared().build<cocos2d::Node>(type);
				if(result)
					load(result.ptr(), xmlnode, depth + 1);
			}
			else
			{
				if(__logging) CCLOG("warning: try loading node with empty type");
			}
		}
		else
		{
			result = load_node(template_, type, depth + 1);
			pugi::xml_node xmlnodem = xmlnode;
			xmlnodem.remove_attribute(ksTemplate.c_str());
			load(result, xmlnodem, depth + 1);
		}
		if(depth == 0 && dynamic_cast<NodeExt*>(result.ptr()))
		{
			dynamic_cast<NodeExt*>(result.ptr())->onLoaded();
		}
		return result;
	}

	void load(cocos2d::Node* node, const std::string & path, int depth)
	{
		if(__logging) CCLOG("load from doc [%s]", path.c_str());
		auto doc = loadDoc(path);
		load(node, doc->root().first_child(), depth);
	}

	void load(cocos2d::Node* node, pugi::xml_node xmlnode, int depth)
	{
		//const std::string& type = xmlnode.attribute(ksType.c_str()).as_string();
		std::string template_ = xmlnode.attribute(ksTemplate.c_str()).as_string();
		ParamCollection macroses(xmlnode.attribute("macroses").as_string());
		xmlnode.remove_attribute("macroses");
		for(auto pair : macroses)
		{
			macros::set(pair.first, pair.second);
		}
		
		if(template_.empty() == false)
		{
			load(node, template_, depth + 1);
		}
		auto nodeext = dynamic_cast<NodeExt*>(node);
		if(nodeext)
			bookDirectory(nodeext);

		for(auto attr = xmlnode.first_attribute(); attr; attr = attr.next_attribute())
		{
			const std::string name = attr.name();
			xmlLoader::setProperty(node, name, attr.value());
		}
		while(true)
		{
			auto attr = xmlnode.first_attribute();
			if(attr)
				xmlnode.remove_attribute(attr);
			else
				break;
		}

		for(auto xmlentity : xmlnode)
		{
			const std::string& tag = xmlentity.name();
			if(tag == "children")
				load_children(node, xmlentity, depth);
			else if(tag == "actions")
			{
				NodeExt * nodeext = dynamic_cast<NodeExt*>(node);
				assert(nodeext);
				nodeext->loadActions(xmlentity);
			}
			else if(tag == "events")
			{
				NodeExt * nodeext = dynamic_cast<NodeExt*>(node);
				assert(nodeext);
				nodeext->loadEvents(xmlentity);
			}
			else if(tag == "macroses")
			{
				for(auto xmlmacros : xmlentity)
				{
					auto name = xmlmacros.attribute("name").as_string();
					auto value = xmlmacros.attribute("value").as_string();
					macros::set(name, value);
				}
			}
			else if (tag == "node")
			{
				load_child(node, xmlentity, depth);
			}
			else
			{
				bool result(false);
				NodeExt * nodeext = dynamic_cast<NodeExt*>(node);
				if(nodeext)
				{
					result = nodeext->loadXmlEntity(tag, xmlentity);
				}

				if(!result)
				{
					if(__logging) log("xml node will not reading. path=[%s]", xmlentity.path().c_str());
				}
			}

		}

		if(nodeext)
		{
			unbookDirectory(nodeext);
		}
	}

	NodePointer getorbuild_node(cocos2d::Node * node, pugi::xml_node xmlnode, int depth)
	{
		const std::string& type = macros::parse(xmlnode.attribute(ksType.c_str()).as_string());
		const std::string& name = macros::parse(xmlnode.attribute(ksName.c_str()).as_string());
		const std::string& path = macros::parse(xmlnode.attribute(ksPath.c_str()).as_string());
		const std::string& template_ = macros::parse(xmlnode.attribute(ksTemplate.c_str()).as_string());
        if(name.find(":validate") != std::string::npos)
            return nullptr;
#if DEV != 1
        if(name.find(":dev") != std::string::npos)
            return nullptr;
#endif

		ParamCollection macroses(xmlnode.attribute("macroses").as_string());
		xmlnode.remove_attribute("macroses");
		for(auto pair : macroses)
		{
			macros::set(pair.first, pair.second);
		}

		NodePointer child(nullptr);
		if(child == nullptr && path.empty() == false)
		{
			child = getNodeByPath(node, path);
		}
		if(child && template_.empty() == false)
		{
			load(child, template_, depth);
		}
		if(child == nullptr && template_.empty() == false)
		{
			child = load_node(template_, type, depth);
			xmlnode.remove_attribute(ksTemplate.c_str());
		}
		if(child && type.empty() == false)
		{
			//CCLOG("Warning! type redifinited name[%s] type[%s]", name.c_str(), type.c_str());
		}
		if(child == nullptr)
		{
			if(__logging) CCLOG("try create child [%s]", type.c_str());
			child = mlObjectFactory::shared().build<cocos2d::Node>(type);
		}
		if(!child)
		{
			CCLOG("xmlLoader: getorbuild_node");
			CCLOG("name: [%s]", name.c_str());
			CCLOG("type: [%s]", type.c_str());
			CCLOG("path: [%s]", path.c_str());
			CCLOG("template: [%s]", template_.c_str());
		}
        assert(child);
		return child;
	}

	NodePointer load_child(cocos2d::Node* node, pugi::xml_node xmlnode, int depth)
	{
		auto child = getorbuild_node(node, xmlnode, depth);
		if (child == nullptr)
			return nullptr;
		load(child, xmlnode, depth);
		if (child->getParent() == nullptr)
		{
			node->addChild(child, child->getLocalZOrder());
		}
		return child;
	}

	void load_children(cocos2d::Node* node, pugi::xml_node xmlnode, int depth)
	{
		FOR_EACHXML_BYTAG(xmlnode, xmlchild, "node")
		{
            if(xmlnode.attribute("validate"))
                continue;
			load_child(node, xmlchild, depth);
		}
	}

	IntrusivePtr<EventBase> load_event(const pugi::xml_node xmlnode)
	{
		const std::string& type = xmlnode.name();

		auto event = EventBase::create(type);
		for(auto attr = xmlnode.first_attribute(); attr; attr = attr.next_attribute())
		{
			auto name = attr.name();
			auto value = macros::parse(attr.value());
			event->setParam(name, value);
		}
		for(auto child = xmlnode.first_child(); child; child = child.next_sibling())
		{
			auto name = child.name();
			event->loadXmlEntity(name, child);
		}
		return event;
	}

	template <typename T> static T parseValue(const std::vector <std::string> &attrs, size_t index)
	{
		const std::string& value = attrs[index];
		return strTo <T>(value);
	}

	template <typename T> static T parseOptionalValue(const std::vector <std::string> &attrs, size_t index, const T& defaultValue)
	{
		return index < attrs.size() ? parseValue <T>(attrs, index) : defaultValue;
	}

	ActionPointer load_action_from_file(const std::string & path)
	{
		auto doc = loadDoc(path);
		auto root = doc->root().first_child();
		return load_action(root);
	}

	ActionPointer load_action(const std::string & desc)
	{
		auto remove_variant = [](const std::string & desc)
		{
			auto k = desc.find("@(");
			auto l = k != -1 ? desc.find(")", k + 2) : -1;
			if (k != std::string::npos && l != std::string::npos)
			{
				std::vector<std::string> variants;
				split(variants, desc.substr(k + 2, l - k - 2), '/');
				assert(!variants.empty());
				auto variant = variants[rand() % variants.size()];
				return desc.substr(0, k) + variant + desc.substr(l + 1);
			}
			return desc;
		};

		auto remove_spaces = [](const std::string & desc)
		{
			std::string result = desc;
			const std::string spaces(" \n\t\r");
			std::string::size_type k(0);
			while((k = result.find_last_of(spaces)) != std::string::npos)
			{
				result.erase(k, 1);
			}
			return result;
		};
		auto getType = [](const std::string & desc)
		{
			std::string type;
			auto k = desc.find("[");
            if(k != std::string::npos)
				type = desc.substr(0, k);
			return type;
		};
		auto getParams = [](const std::string & desc)
		{
			std::string params;
			auto k = desc.find("[");
			if(k != std::string::npos)
			{
				int count(1);
				int l = static_cast<int>(k) + 1;
				for(; l < (int)desc.size() && count != 0; ++l)
				{
					if(desc[l] == '[')++count;
					else if(desc[l] == ']')--count;
				}
				params = desc.substr(k + 1, l - k - 2);
			}
			return params;
		};
		auto getAttrs = [](const std::string & params)
		{
			std::vector<std::string> attr;
			int count = 0;
			int l = 0;
			for(unsigned r = 0; r < params.size(); ++r)
			{
				if(params[r] == '[')++count;
				else if(params[r] == ']')--count;
				if(count == 0 && params[r] == ',')
				{
					attr.push_back(params.substr(l, r - l ));
					l = r + 1;
				}
			}
			attr.push_back(params.substr(l ));
			return attr;
		};
		auto buildAnimation = [](float duration, const std::string value)
		{
			auto _folder = [](std::string & string)
			{
				std::string result;
				auto k = string.find("folder:");
				if(k == 0 || k == 1)
				{
					unsigned l = 0;
					for(l = 0; l < string.size(); ++l)
					{
						if(string[k + l] == ',')
							break;
					}
					result = string.substr(k + 7, l - 7);
					string = string.substr(k + l + 1);
				}
				return result;
			};
			auto _frames = [](std::string & string, const std::string& folder)
			{
				auto _list = [string, folder]()mutable
				{
					std::list<std::string> list;
					std::vector<std::string> frames;
					auto k = string.find("frames:");
					if(k == 0 || k == 1)
					{
						string = string.substr(k + 7);
					}
					if(string.back() == ']')
						string.pop_back();
					split(list, string);
					for(auto & frame : list)
					{
						frames.push_back(folder + frame);
					}
					return frames;
				};
				auto _indexes = [string, folder]()mutable
				{
					std::string _indexes("indexes:");
					std::list<std::string> list;
					std::vector<std::string> frames;
					auto k = string.find(_indexes);
					if(k == 0 || k == 1)
						string = string.substr(k + _indexes.size());
					if(string.back() == ']')
						string.pop_back();
					split(list, string);
					assert(list.size() >= 2);

					std::string frame = list.front();
					std::string ext;
					k = frame.find_last_of(".");
					ext = frame.substr(k);
					frame = frame.substr(0, k);
					list.pop_front();

					std::string indexformat;
					std::vector<int> indexes;
					while(list.empty() == false)
					{
						std::string string = list.front();
						auto k = string.find(":");
						if(k == std::string::npos)
						{
							int index = strTo<int>(string);
							indexes.push_back(index);

							if(indexformat.size() < string.size())
								indexformat = string;
						}
						else
						{
							std::string a = string.substr(0, k);
							std::string b = string.substr(k + 1);
							if(indexformat.size() < a.size()) indexformat = a;
							if(indexformat.size() < b.size()) indexformat = b;
							int l = strTo<int>(a);
							int r = strTo<int>(b);
							for(int i = l; i != r; (r > l ? ++i : --i))
							{
								indexes.push_back(i);
							}
							indexes.push_back(r);
						}
						list.pop_front();
					}

					std::string format("%0" + toStr((int)indexformat.size()) + "d");
					for(auto i : indexes)
					{
						char buffer[8];
						sprintf(buffer, format.c_str(), i);
						std::string frameext = frame + buffer + ext;
						std::string name = folder + frameext;
						auto frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
						if (!frame)
                        {
                            tryLoadPlist(name);
							frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(name);
                        }

						if(frame)
						{
							frames.push_back(name);
						}
						else if(FileUtils::getInstance()->isFileExist(name ))
						{
							auto sprite = createSprite(name);
							if(sprite)
							{
								SpriteFrame* frame = SpriteFrame::create(name, cocos2d::Rect(0, 0, sprite->getContentSize().width, sprite->getContentSize().height ));
								if(frame)
									frames.push_back(name);
							}
						}
					}

					return frames;
				};
				if(string.find("frames:") != std::string::npos)
					return _list();
				else if(string.find("indexes:") != std::string::npos)
					return _indexes();
				assert(0);
				return std::vector<std::string>();
			};

			static std::map<std::string, Animation*> _cash;
#ifndef _DEBUG
			auto iter = _cash.find(value);
			if(iter != _cash.end())
				return iter->second->clone();
#endif

			auto str = value;
			auto folder = _folder(str);
			auto frames = _frames(str, folder);

			auto animation = createAnimation(frames, duration);
			if(animation)
			{
				animation->retain();
				_cash[value] = animation;
				animation = animation->clone();
			}
			return animation;
		};

		const std::string& cleared_desc = macros::parse(remove_spaces(remove_variant(desc)));
		const std::string& type = getType(cleared_desc);
		const std::string& params = getParams(cleared_desc);
		const std::vector<std::string>& attr = getAttrs(params);

		auto FLOAT = [&attr] (int index) { return parseValue <float>(attr, index); };
		auto INT = [&attr] (int index) { return parseValue <int>(attr, index); };
		//auto BOOL = [&attr] (int index) { return parseValue <bool>(attr, index); };

		//auto OPTIONAL_FLOAT = [&attr](size_t index, float defaultValue) { return parseOptionalValue <float>(attr, index, defaultValue); };
		//auto OPTIONAL_INT = [&attr](size_t index, int defaultValue) { return parseOptionalValue <int>(attr, index, defaultValue); };
		auto OPTIONAL_BOOL = [&attr](size_t index, bool defaultValue) { return parseOptionalValue <bool>(attr, index, defaultValue); };
		//auto OPTIONAL_STRING = [&attr](size_t index) { return index < attr.size() ? attr[index] : ""; };

		auto action_interval = [](const std::string & desc) { return static_cast<ActionInterval*>(load_action(desc).ptr()); };
		//auto action_finitetime = [](const std::string & desc) { return static_cast<FiniteTimeAction*>(load_action(desc).ptr()); };

		if(type == k::ActionSequence || type == k::ActionSpawn)
		{
			const std::vector<std::string> & sactions = getAttrs(params);
			Vector<FiniteTimeAction*>actions;
			for(auto& saction : sactions)
			{
				auto action = load_action(saction);
				auto fta = dynamic_cast<FiniteTimeAction*>(action.ptr());
				if(fta)
					actions.pushBack(fta);
			}

			if(type == k::ActionSequence)
				return Sequence::create(actions);
			else
				return Spawn::create(actions);
		}
		else if(type == k::ActionDelayTime) { return DelayTime::create(FLOAT(0 )); }
		else if(type == k::ActionScaleTo) { return ScaleTo::create(FLOAT(0), FLOAT(1), FLOAT(2 )); }
		else if(type == k::ActionScaleBy) { return ScaleBy::create(FLOAT(0), FLOAT(1), FLOAT(2 )); }
		else if(type == k::ActionSkewTo) { return SkewTo::create(FLOAT(0), FLOAT(1), FLOAT(2 )); }
		else if(type == k::ActionSkewBy) { return SkewBy::create(FLOAT(0), FLOAT(1), FLOAT(2 )); }
		else if(type == k::ActionMoveTo) { return MoveTo::create(FLOAT(0), cocos2d::Point(FLOAT(1), FLOAT(2) )); }
		else if(type == k::ActionMoveBy) { return MoveBy::create(FLOAT(0), cocos2d::Point(FLOAT(1), FLOAT(2) )); }
		else if(type == k::ActionRotateTo) { return RotateTo::create(FLOAT(0), FLOAT(1 )); }
		else if(type == k::ActionRotateBy) { return RotateBy::create(FLOAT(0), FLOAT(1 )); }
		else if(type == k::ActionJumpTo) { return JumpTo::create(FLOAT(0), cocos2d::Point(FLOAT(1), FLOAT(2 )), FLOAT(3), INT(4 )); }
		else if(type == k::ActionJumpBy) { return JumpBy::create(FLOAT(0), cocos2d::Point(FLOAT(1), FLOAT(2 )), FLOAT(3), INT(4 )); }
		else if(type == k::ActionBlink) { return Blink::create(FLOAT(0), INT(1 )); }
		else if(type == k::ActionFadeTo) { return FadeTo::create(FLOAT(0), INT(1 )); }
		else if(type == k::ActionFadeIn) { return FadeIn::create(FLOAT(0 )); }
		else if(type == k::ActionFadeOut) { return FadeOut::create(FLOAT(0 )); }
		else if(type == k::ActionTintTo) { return TintTo::create(FLOAT(0), INT(1), INT(2), INT(3 )); }
		else if(type == k::ActionTintBy) { return TintBy::create(FLOAT(0), INT(1), INT(2), INT(3 )); }

		else if(type == k::ActionRepeatForever) { return RepeatForever::create(action_interval(attr[0] )); }
		else if(type == k::ActionRepeat) { return Repeat::create(action_interval(attr[0]), INT(1 )); }
		else if(type == k::ActionEaseIn) { return EaseIn::create(action_interval(attr[0]), FLOAT(1 )); }
		else if(type == k::ActionEaseOut) { return EaseOut::create(action_interval(attr[0]), FLOAT(1 )); }
		else if(type == k::ActionEaseInOut) { return EaseInOut::create(action_interval(attr[0]), FLOAT(1 )); }
		else if(type == k::ActionBounceIn) { return EaseBounceIn::create(action_interval(attr[0] )); }
		else if(type == k::ActionBounceOut) { return EaseBounceOut::create(action_interval(attr[0] )); }
		else if(type == k::ActionBounceInOut) { return EaseBounceInOut::create(action_interval(attr[0] )); }
		else if(type == k::ActionBackIn) { return EaseBackIn::create(action_interval(attr[0] )); }
		else if(type == k::ActionBackOut) { return EaseBackOut::create(action_interval(attr[0] )); }
		else if(type == k::ActionBackInOut) { return EaseBackInOut::create(action_interval(attr[0] )); }
		else if(type == k::ActionSineIn) { return EaseSineIn::create(action_interval(attr[0] )); }
		else if(type == k::ActionSineOut) { return EaseSineOut::create(action_interval(attr[0] )); }
		else if(type == k::ActionSineInOut) { return EaseSineInOut::create(action_interval(attr[0] )); }
		else if(type == k::ActionAnimate) { return Animate::create(buildAnimation(FLOAT(0), attr[1] )); }
		else if(type == k::ActionBezier) {
			EaseBezierAction * bezier = EaseBezierAction::create(action_interval(attr[0] ));
			bezier->setBezierParamer(FLOAT(1), FLOAT(2), FLOAT(3), FLOAT(4 ));
			return bezier;
		}

		//action instant
		else if(type == k::ActionRemoveSelf) { return RemoveSelf::create(); }
		else if(type == k::ActionShow) { return Show::create(); }
		else if(type == k::ActionHide) { return Hide::create(); }
		else if(type == k::ActionEnable) { return ActionEnable::create(); }
		else if(type == k::ActionDisable) { return ActionDisable::create(); }
        else if(type == k::ActionSetProperty) { return ActionSetProperty::create(attr[0], attr[1]); }
		else
		{
#if DEV == 1
			std::string message = "undefinited action type [" + type + "] \n";
			message += "action string: \n";
			message += cleared_desc;
			if(__logging) log("%s", message.c_str());
			MessageBox(message.c_str(), "Error creating action");
			assert(0);
#endif
		}

		return nullptr;
	}

	ActionPointer load_action(const pugi::xml_node xmlnode)
	{
		auto body = xmlnode.attribute("value").as_string();
		return load_action(body);
	}

# pragma mark -
# pragma mark macros
	namespace macros
	{
		static ParamCollection _macroses;
        
        const std::string detectFormulas(const std::string& string)
        {
            
            static std::string randint = "randint(";
            auto l = string.find(randint);
            auto r = string.find(")");
            if(l != std::string::npos && r != std::string::npos)
            {
                auto k = l + randint.size();
                auto sintervar = string.substr(k, r - k);
                std::vector<int> interval;
                split_t(interval, sintervar);
                assert(interval.size() == 2 && interval[1] > interval[0]);
                auto d = interval[1] - interval[0] + 1;
                auto randint = rand() % d + interval[0];
                auto result = string.substr(0, l) + toStr(randint) + string.substr(r+1);
                return detectFormulas(result);
            }
            
            static std::string div = "/";
            size_t k = string.find(div);
            if(k != std::string::npos)
            {
                auto sleft = string.substr(0, k);
                auto sright = string.substr(k  +1);
                float left = strTo<float>(get(sleft));
                float right = strTo<float>(get(sright));
                return toStr(left / right);
            }
            return string;
        }

		void set(const std::string & name, const std::string & value)
		{
			_macroses[name] = value;
		}

		void set(const std::vector<std::string>& values)
		{
			assert(values.size() % 2 == 0);
			for (size_t i=0;i<values.size()-1;++i)
			{
				set(values[i], values[i + 1]);
			}
		}

		const std::string get(const std::string & name)
		{
			auto iter = _macroses.find(name);
			if(iter != _macroses.end())
				return iter->second;
            
			return detectFormulas(name);
		}

		void clear()
		{
			_macroses.clear();
		}

		void erase(const std::string & name)
		{
			auto iter = _macroses.find(name);
			if(iter != _macroses.end())
				_macroses.erase(iter);
		}

		const std::string parse(const std::string& string)
		{
			std::string result;
			int k = 0;
			int l = -1;
			int r = -1;
			int counter = 0;
			int size = static_cast<int>(string.size());
			for(int i=0; i<size; ++i)
			{
				if(i < size-1 && string[i] == delimiter_l[0] && string[i+1] == delimiter_l[1])
				{
					++counter;
					if(counter == 1)
					{
						l = i+2;
					}
				}
				if(counter > 0 && string[i] == delimiter_r[0])
				{
					--counter;
					r = i;
					if(counter==0)
					{
						auto value = string.substr(l, r-l);
						value = parse(get(value));
						result += string.substr(k, l - k - 2) + value;
						k = r + 1;
					}
				}
			}
			if(k < size)
				result += string.substr(k);

			return result;
		}
        
        void log()
        {
#if defined(COCOS2D_DEBUG) && COCOS2D_DEBUG > 0
            for(auto& pair : _macroses)
            {
                CCLOG("macros: [%s]=[%s]", pair.first.c_str(), pair.second.c_str());
            }
#endif
        }

	}

	void load_paramcollection(ParamCollection& params, const pugi::xml_node xmlnode)
	{
		for(auto child : xmlnode)
		{
			std::string name = child.attribute("name").as_string();
			std::string value = child.attribute("value").as_string();
			if(name.empty())
				name = child.name();
			if(value.empty())
				value = child.text().as_string();
			params[name] = macros::parse(value);
		}
	}
    
    void createResourcesList(std::string atlas)
    {
        auto scene = cocos2d::Director::getInstance()->getRunningScene();
        if(!scene)
            return;
        auto path = cocos2d::FileUtils::getInstance()->fullPathForFilename("ini/scenes/loader/resources.xml");
        auto xml = loadDoc(path);
        auto root = xml->root().first_child();
        pugi::xml_node sceneNode;
        atlas = "plists/" + atlas;
        for(auto child : root)
        {
            std::string name = child.attribute("name").as_string();
            if(name == scene->getName())
            {
                sceneNode = child;
                break;
            }
        }
        if(!sceneNode)
        {
            sceneNode = root.append_child("scene");
            sceneNode.append_attribute("name").set_value(scene->getName().c_str());
            sceneNode.append_child("atlases");
        }
        std::set<std::string> current;
        for(auto child : sceneNode.child("atlases"))
        {
            current.insert(child.attribute("path").as_string());
        }
        if(current.count(atlas) > 0)
            return;
        auto node = sceneNode.child("atlases").append_child("atlas");
        node.append_attribute("path").set_value(atlas.c_str());
        saveDoc(path, xml);
    }
    
    bool tryLoadPlist(const std::string& path)
    {
        auto k = path.find('/');
        cocos2d::Rect rect = cocos2d::Rect::ZERO;
        if(k != -1)
        {
            FileUtils::getInstance()->setPopupNotify(false);
            auto atlasPath = path.substr(0, k) + ".plist";
            auto exist = FileUtils::getInstance()->isFileExist(atlasPath);
            FileUtils::getInstance()->setPopupNotify(true);
            
            if(exist)
            {
                cocos2d::SpriteFrameCache::getInstance()->addSpriteFramesWithFile(atlasPath);
#if DEV == 1
                createResourcesList(atlasPath);
#endif
                return true;
            }
        }
        return false;
    }
};

