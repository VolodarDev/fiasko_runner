/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#ifndef __XML_PROPERTIES__
#define __XML_PROPERTIES__
#include "ml/types.h"


class NodeExt;

#define __DECLARE_INT_PROPERTY(name, value) const int k##name = __LINE__; const std::string ks##name(value);

namespace xmlLoader
{
    __DECLARE_INT_PROPERTY(Type, "type");
    __DECLARE_INT_PROPERTY(Pos, "pos");
    __DECLARE_INT_PROPERTY(PosOffset, "pos_offset");
    __DECLARE_INT_PROPERTY(PosX, "x");
    __DECLARE_INT_PROPERTY(PosY, "y");
    __DECLARE_INT_PROPERTY(Scale, "scale");
    __DECLARE_INT_PROPERTY(Rotation, "rotation");
    __DECLARE_INT_PROPERTY(Stretch, "strech");
    __DECLARE_INT_PROPERTY(Size, "size");
	__DECLARE_INT_PROPERTY(Skew, "skew");
    __DECLARE_INT_PROPERTY(Visible, "visible");
    __DECLARE_INT_PROPERTY(LocalZ, "z");
    __DECLARE_INT_PROPERTY(GlobalZ, "globalzorder");
    __DECLARE_INT_PROPERTY(Center, "center");
    __DECLARE_INT_PROPERTY(Tag, "tag");
    __DECLARE_INT_PROPERTY(CascadeColor, "cascadecolor");
    __DECLARE_INT_PROPERTY(CascadeOpacity, "cascadeopacity");
    __DECLARE_INT_PROPERTY(Name, "name");
    __DECLARE_INT_PROPERTY(Id, "id");
    __DECLARE_INT_PROPERTY(Image, "image");
    __DECLARE_INT_PROPERTY(Blending, "blending");
    __DECLARE_INT_PROPERTY(Wrap, "wrap");
    __DECLARE_INT_PROPERTY(Opacity, "opacity");
    __DECLARE_INT_PROPERTY(Color, "color");
    __DECLARE_INT_PROPERTY(Animation, "animation");
    __DECLARE_INT_PROPERTY(Action, "action");
    __DECLARE_INT_PROPERTY(ImageNormal, "imageN");
    __DECLARE_INT_PROPERTY(ImageSelected, "imageS");
    __DECLARE_INT_PROPERTY(ImageDisabled, "imageD");
    __DECLARE_INT_PROPERTY(Text, "text");
    __DECLARE_INT_PROPERTY(Font, "font");
    __DECLARE_INT_PROPERTY(FontSystem, "fontsystem");
    __DECLARE_INT_PROPERTY(FontTTF, "fontttf");
    __DECLARE_INT_PROPERTY(FontSize, "fontsize");
    __DECLARE_INT_PROPERTY(TextWidth, "textwidth");
    __DECLARE_INT_PROPERTY(TextHeight, "textheight");
    __DECLARE_INT_PROPERTY(TextArea, "textarea");
    __DECLARE_INT_PROPERTY(LineSpacing, "linespacing");
    __DECLARE_INT_PROPERTY(TextAlign, "textalign");
    __DECLARE_INT_PROPERTY(TextVAlign, "v_align");
    __DECLARE_INT_PROPERTY(EnableShadow, "enableshadow");
    __DECLARE_INT_PROPERTY(DisableShadow, "disableshadow");
    __DECLARE_INT_PROPERTY(EnableOutline, "enableoutline");
    __DECLARE_INT_PROPERTY(DisableOutline, "disableoutline");
    __DECLARE_INT_PROPERTY(EnableGlow, "enableglow");
    __DECLARE_INT_PROPERTY(DisableGlow, "disableglow");
    __DECLARE_INT_PROPERTY(MenuCallBack, "callback");
    __DECLARE_INT_PROPERTY(Enabled, "enabled");
    __DECLARE_INT_PROPERTY(ScaleEffect, "scale_effect");
    __DECLARE_INT_PROPERTY(Sound, "sound");
    __DECLARE_INT_PROPERTY(Path, "path");
    __DECLARE_INT_PROPERTY(Template, "template");
    //cocos2d::ProgressTimer
    __DECLARE_INT_PROPERTY(ProgressType, "progresstype");
    __DECLARE_INT_PROPERTY(Resource, "resource");
    __DECLARE_INT_PROPERTY(PositionType, "position_type");
    __DECLARE_INT_PROPERTY(Percent, "percent");
    __DECLARE_INT_PROPERTY(MidPoint, "midpoint");
    __DECLARE_INT_PROPERTY(BarChangeRate, "barchangerate");
    //mlSlider
    __DECLARE_INT_PROPERTY(ProgressImage, "progressimage");
    __DECLARE_INT_PROPERTY(ShaderProgram, "shaderprogram");
    __DECLARE_INT_PROPERTY(Duration, "duration");
    __DECLARE_INT_PROPERTY(9Scale, "scale_9");
    __DECLARE_INT_PROPERTY(Clipping, "clipping");
    __DECLARE_INT_PROPERTY(Direction, "direction");
    __DECLARE_INT_PROPERTY(InnerPos, "inner_pos");
    __DECLARE_INT_PROPERTY(InnerSize, "inner_size");
    __DECLARE_INT_PROPERTY(InertiaScroll, "inertiascroll");
    __DECLARE_INT_PROPERTY(Bounce, "bounce");
    __DECLARE_INT_PROPERTY(ScrollBarWidth, "scrollbarwidth");
    __DECLARE_INT_PROPERTY(ScrollBarColor, "scrollbarcolor");
    __DECLARE_INT_PROPERTY(ScrollBarColor4, "scrollbarcolor4");
    __DECLARE_INT_PROPERTY(ScrollBarAutoHide, "scrollbarautohide");
    __DECLARE_INT_PROPERTY(HotLocalisation, "hotlocalisation");
    __DECLARE_INT_PROPERTY(SwallowTouches, "swallowtouches");
    __DECLARE_INT_PROPERTY(LayoutType, "layout_type"); // vertical / horisontal / none
    __DECLARE_INT_PROPERTY(DoLayout, "do_layout");
    __DECLARE_INT_PROPERTY(SliderBar, "slider_bar");
    __DECLARE_INT_PROPERTY(SliderProgressBar, "slider_progress_bar");
    __DECLARE_INT_PROPERTY(SliderBallN, "slider_ballN");
    __DECLARE_INT_PROPERTY(SliderBallS, "slider_ballS");
    __DECLARE_INT_PROPERTY(SliderBallD, "slider_ballD");
    __DECLARE_INT_PROPERTY(TextColor, "textcolor");
    __DECLARE_INT_PROPERTY(PlaceHolder, "place_holder");
    __DECLARE_INT_PROPERTY(PlaceHolderColor, "place_holder_color");
    __DECLARE_INT_PROPERTY(CursorEnabled, "cursor_enabled");
    __DECLARE_INT_PROPERTY(IndicatorEnabled, "indicator_enabled");
    __DECLARE_INT_PROPERTY(IndicatorImage, "indicator_image");
    __DECLARE_INT_PROPERTY(IndicatorPosition, "indicator_position");
    __DECLARE_INT_PROPERTY(IndicatorSpace, "indicator_space");
    __DECLARE_INT_PROPERTY(IndicatorColorSelected, "indicator_color_selected");
    __DECLARE_INT_PROPERTY(IndicatorColorNonActive, "indicator_color_non_active");


    //declare other properties as UserProperties + int constant
    __DECLARE_INT_PROPERTY(UserProperties, "");
    
    void bookDirectory(NodeExt* node);
    void unbookDirectory(NodeExt* node);
    
    int strToPropertyType(const std::string &property);
    void setProperty(cocos2d::Node* node, const std::string &property, const std::string &value);
    bool setProperty(cocos2d::Node* node, const int property, const std::string &value);
    
    void bookProperty(const std::string & name, const int iname);
    
    cocos2d::ccMenuCallback get_callback_by_description(const std::string& description);
    
    /*
     * Set image to sprite
     * path can be path to file or path to frame in atlas
     * In DEV mode use files
     * In RELEASE mode use atlases
     */
};

void setTexture(cocos2d::Sprite* sprite, const std::string& path);
void setTexture(cocos2d::Sprite* sprite, const char* path);
cocos2d::Sprite* createSprite(const std::string& frameOrTexturePath);


#ifdef __DECLARE_INT_PROPERTY
#    undef __DECLARE_INT_PROPERTY
#endif

#endif

