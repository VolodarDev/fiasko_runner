/******************************************************************************/
/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: ml
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/
/******************************************************************************/


#include "ml/Button.h"
#include "ml/EditBox.h"
#include "ml/Localization.h"
#include "ml/ShadersCache.h"
#include "ml/Text.h"

void setImage(const std::string& path,
			  const std::function<void(cocos2d::SpriteFrame*)>& setFrame,
			  const std::function<void(const std::string&)>& setTextureFile,
			  const std::function<void(const cocos2d::Rect&)>& check9scale = nullptr,
			  const std::function<void(const std::string&, const cocos2d::ui::Widget::TextureResType& type)>& widgetSet = nullptr);

namespace xmlLoader
{
	struct NineScaleInfo
	{
		int left;
		int right;
		int top;
		int bottom;
	};
	std::map<std::string, NineScaleInfo> _nineScaleInfo;

	cocos2d::Rect getRect9Scale(const std::string& image, bool relative, const cocos2d::Rect& rectOfTexture = cocos2d::Rect::ZERO)
	{
		cocos2d::Rect rect = rectOfTexture;
		cocos2d::Size size = rect.size;
		if (rectOfTexture.equals(cocos2d::Rect::ZERO))
		{
			auto texture = cocos2d::Director::getInstance()->getTextureCache()->getTextureForKey(image);
			if(texture)
			{
				size = texture->getContentSize();
			}
		}
		auto& info = _nineScaleInfo.at(image);
		rect.origin.x = info.left;
		rect.origin.y = info.bottom;
		rect.size.width = size.width - info.left - info.right;
		rect.size.height = size.height - info.top - info.bottom;

		if (relative)
		{
			rect.origin.x /= size.width;
			rect.origin.y /= size.height;
			rect.size.width /= size.width;
			rect.size.height /= size.height;
		}
		return rect;
	}
	void check9Scale(cocos2d::Sprite* sprite, const std::string& image, const cocos2d::Rect& rect = cocos2d::Rect::ZERO)
	{
		if (_nineScaleInfo.count(image) > 0)
			sprite->setCenterRectNormalized(getRect9Scale(image, true, rect));
	}
	void check9Scale(cocos2d::ui::Button* button, const std::string& image, const cocos2d::Rect& rect = cocos2d::Rect::ZERO)
	{
		if (_nineScaleInfo.count(image) > 0)
		{
			button->setScale9Enabled(true);
			button->setCapInsets(getRect9Scale(image, false, rect));
		}
	}
	void check9Scale(cocos2d::ui::Slider* slider, const std::string& image, const cocos2d::Rect& rect = cocos2d::Rect::ZERO)
	{
		if (_nineScaleInfo.count(image) > 0)
		{
			slider->setScale9Enabled(true);
			slider->setCapInsets(getRect9Scale(image, false, rect));
		}
	}

	void loadNineScaleInfo()
	{
		auto doc = loadDoc(std::string("ini/textures.xml"));
		auto root = doc->root().first_child();
		for (auto xml : root)
		{
			NineScaleInfo nsi;
			nsi.left = xml.attribute("left").as_int();
			nsi.right = xml.attribute("right").as_int();
			nsi.top = xml.attribute("bottom").as_int();
			nsi.bottom = xml.attribute("top").as_int();
			auto path = xml.attribute("path").as_string();
			_nineScaleInfo[path] = nsi;
		}
	}

	static std::deque<NodeExt*> _directories;
	static std::map<std::string, const int> properties;
	struct __autofillproperties
	{
		void fill()
		{
			bookProperty(ksType, kType);
			bookProperty(ksId, kId);
			bookProperty(ksVisible, kVisible);
            bookProperty(ksPos, kPos);
            bookProperty(ksPosOffset, kPosOffset);
			bookProperty(ksPosX, kPosX);
			bookProperty(ksPosY, kPosY);
			bookProperty(ksScale, kScale);
			bookProperty(ksStretch, kStretch) ;
			bookProperty(ksSize, kSize);
			bookProperty(ksSkew, kSkew);
			bookProperty(ksRotation, kRotation);
			bookProperty(ksLocalZ, kLocalZ) ;
			bookProperty(ksGlobalZ, kGlobalZ);
			bookProperty(ksCenter, kCenter) ;
			bookProperty(ksTag, kTag) ;
			bookProperty(ksCascadeOpacity, kCascadeOpacity);
			bookProperty(ksCascadeColor, kCascadeColor);
			bookProperty(ksImage, kImage);
			bookProperty(ksBlending, kBlending);
			bookProperty(ksWrap, kWrap);
			bookProperty(ksOpacity, kOpacity);
			bookProperty(ksColor, kColor);
			bookProperty(ksAction, kAction);
			bookProperty(ksAnimation, kAnimation);
			bookProperty(ksName, kName);
			bookProperty(ksImageNormal, kImageNormal) ;
			bookProperty(ksImageSelected, kImageSelected);
			bookProperty(ksImageDisabled, kImageDisabled);
			bookProperty(ksText, kText);
			bookProperty(ksFont, kFont);
			bookProperty(ksFontTTF, kFontTTF);
			bookProperty(ksFontSize, kFontSize);
			bookProperty(ksMenuCallBack, kMenuCallBack);
			bookProperty(ksEnabled, kEnabled);
			bookProperty(ksTextWidth, kTextWidth);
			bookProperty(ksTextHeight, kTextHeight);
			bookProperty(ksTextArea, kTextArea);
			bookProperty(ksLineSpacing, kLineSpacing);
			bookProperty(ksTextAlign, kTextAlign);
			bookProperty(ksTextVAlign, kTextVAlign);
			bookProperty(ksEnableShadow, kEnableShadow);
			bookProperty(ksEnableOutline, kEnableOutline);
			bookProperty(ksEnableGlow, kEnableGlow);
			bookProperty(ksDisableShadow, kDisableShadow);
			bookProperty(ksScaleEffect, kScaleEffect);
			bookProperty(ksSound, kSound);
			bookProperty(ksTemplate, kTemplate);
			bookProperty(ksPath, kPath);
			bookProperty(ksProgressType, kProgressType);
			bookProperty(ksResource, kResource);
			bookProperty(ksPercent, kPercent);
			bookProperty(ksMidPoint, kMidPoint);
			bookProperty(ksBarChangeRate, kBarChangeRate);
			bookProperty(ksProgressImage, kProgressImage);
			bookProperty(ksShaderProgram, kShaderProgram);
			bookProperty(ksDuration, kDuration);
			bookProperty(ks9Scale, k9Scale);
			bookProperty(ksClipping, kClipping);
			bookProperty(ksDirection, kDirection);
			bookProperty(ksInnerPos, kInnerPos);
			bookProperty(ksInnerSize, kInnerSize);
			bookProperty(ksInertiaScroll, kInertiaScroll);
			bookProperty(ksBounce, kBounce);
			bookProperty(ksScrollBarWidth, kScrollBarWidth);
			bookProperty(ksScrollBarColor, kScrollBarColor);
			bookProperty(ksScrollBarColor4, kScrollBarColor4);
			bookProperty(ksScrollBarAutoHide, kScrollBarAutoHide);
			bookProperty(ksHotLocalisation, kHotLocalisation);
			bookProperty(ksSwallowTouches, kSwallowTouches);
			bookProperty(ksLayoutType, kLayoutType);
            bookProperty(ksDoLayout, kDoLayout);
			bookProperty(ksSliderBar, kSliderBar);
			bookProperty(ksSliderProgressBar, kSliderProgressBar);
			bookProperty(ksSliderBallN, kSliderBallN);
			bookProperty(ksSliderBallS, kSliderBallS);
			bookProperty(ksSliderBallD, kSliderBallD);
			bookProperty(ksTextColor, kTextColor);
			bookProperty(ksPlaceHolder, kPlaceHolder);
            bookProperty(ksPlaceHolderColor, kPlaceHolderColor);
			bookProperty(ksCursorEnabled, kCursorEnabled);
            bookProperty(ksPositionType, kPositionType);
			
			bookProperty(ksIndicatorEnabled, kIndicatorEnabled);
			bookProperty(ksIndicatorImage, kIndicatorImage);
			bookProperty(ksIndicatorPosition, kIndicatorPosition);
            bookProperty(ksIndicatorSpace, kIndicatorSpace);
            bookProperty(ksIndicatorColorSelected, kIndicatorColorSelected);
            bookProperty(ksIndicatorColorNonActive, kIndicatorColorNonActive);

			loadNineScaleInfo();
		}
	};
	__autofillproperties __autofillproperties__;

	void bookProperty(const std::string & name, const int iname)
	{
#ifdef _DEBUG
		for(auto& pair : properties)
		{
			int second = pair.second;
			assert(pair.second != iname);
			assert(pair.first != name);
		}
#endif
		properties.insert(std::pair<std::string, const int>(name, iname));
	}

	int strToPropertyType(const std::string &property)
	{
		static bool first(true);
		if(first)
		{
			__autofillproperties__.fill();
			first = false;
		}
		return properties[property];
	}

	std::string propertyTypeToStr(const int property)
	{
		for(auto pair : properties)
		{
			if(pair.second == property)
			{
				return pair.first;
			}
		}
		return "";
	}

	void setProperty(cocos2d::Node* node, const std::string &property, const std::string &value)
	{
		if(property == ksTemplate)
			return;

		const int iproperty = strToPropertyType(property);
		if(false == setProperty(node, iproperty, value))
		{
			auto nodeext = dynamic_cast<NodeExt*>(node);
			if(nodeext)
				nodeext->setProperty(property, xmlLoader::macros::parse(value));
		}
	}
	
	bool setProperty(cocos2d::Node* node, const int property, const std::string & rawvalue)
	{
		bool result(false);
		assert(node);
		static Localization& language = Localization::shared();

		cocos2d::Sprite * sprite = dynamic_cast<cocos2d::Sprite*>(node);
		auto button = dynamic_cast<Button*>(node);
		auto progress = dynamic_cast<cocos2d::ProgressTimer*>(node);
		auto text = dynamic_cast<cocos2d::Label*>(node);
		auto uiText = dynamic_cast<Text*>(node);
		auto scrollview = dynamic_cast<cocos2d::ui::ScrollView*>(node);
		auto layout = dynamic_cast<cocos2d::ui::Layout*>(node);
		auto slider = dynamic_cast<cocos2d::ui::Slider*>(node);
		auto textfield = dynamic_cast<cocos2d::ui::TextField*>(node);
        auto editbox = dynamic_cast<EditBox*>(node);

		std::string value = xmlLoader::macros::parse(rawvalue);

		cocos2d::Point point;
		cocos2d::Size size;
		NodeExt * nodeext(nullptr);

		nodeext = dynamic_cast<NodeExt*>(node);
		if(nodeext)
			result = nodeext->setProperty(property, value);
		
		if(result == false)
		{
			result = true;
			switch(property)
			{
				//for node:
				case kType:
					break;
				case kName:
				case kId:
				{
					std::string name = value;
#if DEV == 1
					auto k = name.find(":dev");
					if(k != std::string::npos)
					{
						name = name.substr(0, k) + name.substr(k + 4);
					}
#endif
					node->setName(name);
					break;
				}
				case kVisible:
					node->setVisible(strTo<bool>(value));
					break;
                case kPos:
                    node->setPosition(strTo<cocos2d::Point>(value));
                    break;
                case kPosOffset:
                    node->setPosition(node->getPosition() + strTo<cocos2d::Point>(value));
                    break;
				case kPosX: 
					node->setPositionX(strTo<float>(value));
					break;
				case kPosY: 
					node->setPositionY(strTo<float>(value));
					break;
				case kScale:
				{
					auto defaultScaleX = node->getScaleX();
					auto x = 1.f;
					auto y = 1.f;
					if(value.find("x") != std::string::npos)
					{
						point = strTo<cocos2d::Point>(value);
						x = point.x;
						y = point.y;
					}
					else
					{
						x = y = strTo<float>(value);
					}
					node->setScale(x, y);
					
					if(text)
					{
						auto textWidth = text->getWidth();
						auto def = textWidth * defaultScaleX;
						auto cur = def / x;
						text->setWidth(cur);
					}
					auto uiText = dynamic_cast<cocos2d::ui::Text*>(node);
					if(uiText)
					{
						auto textArea = uiText->getTextAreaSize();
						auto def = textArea.width * defaultScaleX;
						textArea.width = def / x;
						uiText->setTextAreaSize(textArea);
					}
					break;
				}
				case kRotation:
					node->setRotation(strTo<float>(value));
					break;
				case kCenter:
					node->setAnchorPoint(strTo<cocos2d::Point>(value));
					break;
				case kStretch:
				{
					auto stretch = strTo<Stretch>(value);
					stretchNode(node, stretch);
					if(uiText)
						uiText->setStretch(stretch);
					break;
				}
				case kSize:
                {
                    if(value == "auto")
                    {
                        assert(sprite);
                        if(sprite->getSpriteFrame())
                            size = sprite->getSpriteFrame()->getOriginalSize();
                        else if(sprite->getTexture())
                            size = sprite->getTexture()->getContentSizeInPixels();
                        sprite->setContentSize(size);
                    }
                    else
                    {
                        size.width = strTo<cocos2d::Point>(value).x;
                        size.height = strTo<cocos2d::Point>(value).y;
                        if (button)
                        {
                            button->setContentSize(size);
                        }
                        else
                        {
                            node->setContentSize(size);
                        }
                    }
					break;
                }
				case kSkew:
				{
                    auto x = strTo<cocos2d::Point>(value).x;
					auto y = strTo<cocos2d::Point>(value).y;
					node->setSkewX(x);
					node->setSkewY(y);
					break;
				}
				case kTag:
					node->setTag(strTo<int>(value));
					break;
				case kCascadeColor:
					node->setCascadeColorEnabled(strTo<bool>(value));
					if(scrollview)
						scrollview->getInnerContainer()->setCascadeColorEnabled(strTo<bool>(value));
					break;
				case kCascadeOpacity:
				{
					node->setCascadeOpacityEnabled(strTo<bool>(value));
					if(scrollview)
						scrollview->getInnerContainer()->setCascadeOpacityEnabled(strTo<bool>(value));
					break;
				}
				case kLocalZ:
					node->setLocalZOrder(strTo<int>(value));
					break;
				case kGlobalZ:
					node->setGlobalZOrder(strTo<int>(value));
					break;
					//for sprite:
				case kImage:
					if(sprite)
					{
						setTexture(sprite, value);
					}
					else if(progress)
					{
						auto sprite = createSprite(value);
						if(sprite)
							progress->setSprite(sprite);
					}
					else if(dynamic_cast<cocos2d::ui::ImageView*>(node))
					{
						auto ui_image = dynamic_cast<cocos2d::ui::ImageView*>(node);
						auto setFrame = [ui_image, value](cocos2d::SpriteFrame* frame){
                            ui_image->loadTexture(value, cocos2d::ui::Widget::TextureResType::PLIST);
						};
						auto setTexture = [ui_image](const std::string& pathToImage){
							ui_image->loadTexture(pathToImage, cocos2d::ui::Widget::TextureResType::LOCAL);
						};
						setImage(value, setFrame, setTexture);
					}
					break;
					//for scroll menu:
				case kBlending:
					assert(sprite || progress);
					if(sprite)
						sprite->setBlendFunc(strTo<cocos2d::BlendFunc>(value));
					else if(progress && progress->getSprite())
						progress->getSprite()->setBlendFunc(strTo<cocos2d::BlendFunc>(value));
					break;
				case kWrap:
					assert(sprite);
					{
						//GL_MIRRORED_REPEAT or GL_REPEAT
						cocos2d::Texture2D::TexParams wrap = {
							GL_LINEAR,GL_LINEAR,
							GL_REPEAT, GL_REPEAT,
						};
						cocos2d::Texture2D::TexParams normal = {
							GL_LINEAR, GL_LINEAR,
							GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,
						};
						if (sprite)
						{
							float w = sprite->getContentSize().width;
							float h = sprite->getContentSize().height;
							sprite->setTextureRect(cocos2d::Rect(0, 0, w, h));
							sprite->getTexture()->setTexParameters(strTo<bool>(value) ? wrap : normal);
						}
						break;
					}
				case kOpacity:
					node->setOpacity(strTo<int>(value));
					break;
				case kColor:
					node->setColor(strTo<cocos2d::Color3B>(value));
					break;
				case kAction:
				case kAnimation:
					node->runAction(xmlLoader::load_action(value));
					break;
				case kImageNormal:
					if(button)
					{
						auto frame = [button, value](cocos2d::SpriteFrame* frame)
						{
							button->loadTextureNormal(value, cocos2d::ui::Widget::TextureResType::PLIST);
						};
						auto texture = [button](const std::string& texture)
						{
							button->loadTextureNormal(texture, cocos2d::ui::Widget::TextureResType::LOCAL);
						};
						auto check9Scale = [button, value](const cocos2d::Rect& rect)
						{
							xmlLoader::check9Scale(button, value, rect);
						};
						setImage(value, frame, texture, check9Scale);
					}
                    else if(editbox)
                    {
                        tryLoadPlist(value);
                        editbox->initWithSizeAndBackgroundSprite(editbox->getContentSize(), value, cocos2d::ui::Widget::TextureResType::PLIST);
                    }
					break;
				case kImageSelected:
					if(button)
					{
						auto frame = [button, value](cocos2d::SpriteFrame* frame)
						{
							button->loadTexturePressed(value, cocos2d::ui::Widget::TextureResType::PLIST);
						};
						auto texture = [button](const std::string& texture)
						{
							button->loadTexturePressed(texture, cocos2d::ui::Widget::TextureResType::LOCAL);
						};
						auto check9Scale = [button, value](const cocos2d::Rect& rect)
						{
							xmlLoader::check9Scale(button, value, rect);
						};
						setImage(value, frame, texture, check9Scale);
					}
					break;
				case kImageDisabled:
				{
					if(button)
					{
						auto frame = [button, value](cocos2d::SpriteFrame* frame)
						{
							button->loadTextureDisabled(value, cocos2d::ui::Widget::TextureResType::PLIST);
						};
						auto texture = [button](const std::string& texture)
						{
							button->loadTextureDisabled(texture, cocos2d::ui::Widget::TextureResType::LOCAL);
						};
						auto check9Scale = [button, value](const cocos2d::Rect& rect)
						{
							xmlLoader::check9Scale(button, value, rect);
						};
						setImage(value, frame, texture, check9Scale);
					}
					break;
				}
				case kSwallowTouches:
				{
					if (button)
						button->setSwallowTouches(strTo<bool>(value));
					else if (scrollview)
						scrollview->setSwallowTouches(strTo<bool>(value));
					else
						assert(0);
					break;
				}
				case kLayoutType:
				{
					auto type = cocos2d::ui::Layout::Type::ABSOLUTE;
					if (value.empty() == false)
					{
						if (value[0] == 'v')
							type = cocos2d::ui::Layout::Type::VERTICAL;
						else if (value[0] == 'h')
							type = cocos2d::ui::Layout::Type::HORIZONTAL;
						else if (value[0] == 'n')
							type = cocos2d::ui::Layout::Type::ABSOLUTE;
						else if (value[0] == 'r')
							type = cocos2d::ui::Layout::Type::RELATIVE;
					}
					if (scrollview)
						scrollview->getInnerContainer()->setLayoutType(type);
					else if (layout)
						layout->setLayoutType(type);
					break;
				}
                case kDoLayout:
                {
                    auto layout = dynamic_cast<cocos2d::ui::Layout*>(node);
                    if (layout)
                    {
                        layout->forceDoLayout();
                        if(value == "auto_size" && (
                                                    layout->getLayoutType() == cocos2d::ui::Layout::Type::HORIZONTAL ||
                                                    layout->getLayoutType() == cocos2d::ui::Layout::Type::VERTICAL))
                        {
                            auto elements = layout->getChildren();
                            cocos2d::ui::Widget* element = nullptr;
                            for(auto iter = elements.rbegin(); iter != elements.rend(); ++iter)
                            {
                                auto child = *iter;
                                if(!child->isVisible())
                                    continue;
                                element = dynamic_cast<cocos2d::ui::Widget*>(child);
                                break;
                            }
                            if(element && layout->getLayoutType() == cocos2d::ui::Layout::Type::HORIZONTAL)
                            {
                                auto size = layout->getContentSize();
                                size.width = element->getRightBoundary();
                                layout->setContentSize(size);
                            }
                            else if(element && layout->getLayoutType() == cocos2d::ui::Layout::Type::VERTICAL)
                            {
                                auto size = layout->getContentSize();
                                size.height = element->getBottomBoundary();
                                layout->setContentSize(size);
                            }
                        }
                        else if(value=="arrange")
                        {
                            layout->requestDoLayout();
                        }
						layout->forceDoLayout();
                    }
                    break;
                }
				case kMenuCallBack:
				{
					cocos2d::ccMenuCallback callback(nullptr);
					for(auto iter = _directories.rbegin(); iter != _directories.rend(); ++iter)
					{
                        auto node = *iter;
 						callback = node->get_callback_by_description(value);
						if(callback)
							break;
					}
					if(button)
						button->addClickEventListener(callback);
					break;
				}
				case kEnabled:
					if(button)
						button->setEnabled(strTo<bool>(value));
					else
						result = false;
					break;
				case kSound:
                    if(button)
                    {
                        assert(value.empty() || cocos2d::FileUtils::getInstance()->isFileExist(value));
                        button->setSoundOnClick(value);
                    }
                    else
                    {
                        result = false;
                    }
					break;
				case kHotLocalisation:
				{
//					if(text)
//						text->useLocation(strTo<bool>(value));
					break;
				}
				case kText:
				{
					std::string string = macros::parse(value);
					string = language.locale(string);
					string = macros::parse(string);
					if(uiText)
					{
						uiText->setString(string);
					}
					else if(text)
					{
//						text->setString(string);
//						text->setSourceString(rawvalue);
					}
					else if (button)
					{
						button->setTitleText(string);
					}
                    else
                    {
                        result = false;
                    }
					break;
				}
				case kFont:
                {
					assert(text || uiText || button || textfield || editbox);
                    cocos2d::FileUtils::getInstance()->setPopupNotify(false);
                    auto fontName = value;
					if(uiText)
					{
						uiText->setFontName(fontName);
					}
					else if(text)
					{
						if(fontName.find(".fnt") != std::string::npos)
							text->setBMFontFilePath(fontName);
//						else if(value.find(".ttf") != std::string::npos)
//							text->setTTFFontName(fontName);
						else
							text->setSystemFontName(fontName);
					}
					else if (button)
					{
						button->setTitleFontName(fontName);
					}
					else if (textfield)
					{
						textfield->setFontName(fontName);
					}
                    else if(editbox)
                    {
                        editbox->setFontName(fontName.c_str());
                        editbox->setPlaceholderFontName(fontName.c_str());
                    }
                    else
                    {
                        result = false;
                    }
                    cocos2d::FileUtils::getInstance()->setPopupNotify(true);
					break;
                }
				case kFontTTF:
                {
                    cocos2d::FileUtils::getInstance()->setPopupNotify(false);
                    auto fontName = value;
					if (uiText)
						uiText->setFontName(fontName);
//					else if (text)
//						text->setTTFFontName(fontName);
					else if (button)
						button->setTitleFontName(fontName);
					else if (textfield)
						textfield->setFontName(fontName);
                    else if(editbox)
                    {
                        editbox->setFontName(fontName.c_str());
                        editbox->setPlaceholderFontName(fontName.c_str());
                    }
                    else
                    {
                        result = false;
                    }
                    cocos2d::FileUtils::getInstance()->setPopupNotify(true);
					break;
                }
				case kFontSize:
					if(uiText)
						uiText->setFontSize(strTo<int>(value));
//					else if (text)
//						text->setFontSize(strTo<int>(value));
					else if (button)
						button->setTitleFontSize(strTo<int>(value));
					else if (textfield)
						textfield->setFontSize(strTo<int>(value));
                    else if(editbox)
                    {
                        editbox->setFontSize(strTo<int>(value));
                        editbox->setPlaceholderFontSize(strTo<int>(value));
                    }
                    else
                    {
                        result = false;
                    }
					break;
				case kTextWidth:
					assert(text || button || uiText);
					if(uiText)
					{
						auto size = uiText->getTextAreaSize();
						size.width = strTo<float>(value) / uiText->getScaleX();
						uiText->setTextAreaSize(size);
					}
					else if(text)
					{
						text->setWidth(strTo<float>(value) / text->getScaleX());
					}
					else if(button && button->getTitleLabel())
					{
						button->getTitleLabel()->setWidth(strTo<float>(value));
					}
					
					break;
				case kTextHeight:
					assert(uiText);
					if(uiText)
					{
						auto size = uiText->getTextAreaSize();
						size.height = strTo<float>(value);
						uiText->setTextAreaSize(size);
					}
					break;
				case kTextArea:
					assert(uiText);
					if(uiText)
					{
						uiText->setTextAreaSize(strTo<cocos2d::Size>(value));
					}
					break;
				case kLineSpacing:
					assert(text || button || uiText);
					if(text)
						text->setLineSpacing(strTo<float>(value));
					else if(button)
						button->getTitleLabel()->setLineSpacing(strTo<float>(value));
					break;
				case kTextColor:
					assert(button);
					if(button)
						button->getTitleLabel()->setColor(strTo<cocos2d::Color3B>(value));
					break;
				case kTextAlign:
				{
					assert(text || uiText);
					cocos2d::TextHAlignment align;
					if(value == "center")
                        align = cocos2d::TextHAlignment::CENTER;
					else if(value == "right")
                        align = cocos2d::TextHAlignment::RIGHT;
					else align = cocos2d::TextHAlignment::LEFT;
					if(uiText)
						uiText->setTextHorizontalAlignment(align);
					else if(text)
						text->setAlignment(align);
					break;
				}
				case kTextVAlign:
				{
					assert(text || uiText);
					
					cocos2d::TextVAlignment align;
					if(!value.empty() && value.front() == 'c')
						align = cocos2d::TextVAlignment::CENTER;
					else if(!value.empty() && value.front() == 't')
						align = cocos2d::TextVAlignment::TOP;
					else
						align = cocos2d::TextVAlignment::BOTTOM;
					
					if(uiText)
						uiText->setTextVerticalAlignment(align);
					else if(text)
						text->setVerticalAlignment(align);
					break;
				}
				case kEnableShadow:
				{
					assert(text || uiText);
					//"color:000000FF,offset:2x-2,blurradius:2"
					ParamCollection pc(value);
					auto color = strTo<cocos2d::Color4B>(pc.get("color", "000000FF"));
					auto offset = strTo<cocos2d::Size>(pc.get("offset", "2x-2"));
					auto blurradius = strTo<int>(pc.get("blurradius", "0"));
					if(uiText)
					{
						uiText->enableShadow(color, offset, blurradius);
					}
					else if(text)
					{
						text->enableShadow(color, offset, blurradius);
					}
					break;
				}
				case kEnableOutline:
				{
					assert(text || uiText);
					ParamCollection pc(value);
					auto color = strTo<cocos2d::Color4B>(pc.get("color", "000000FF"));
					auto width = strTo<int>(pc.get("width", "1"));
					if(uiText)
						uiText->enableOutline(color, width);
					else if(text)
						text->enableOutline(color, width);
					break;
				}
				case kEnableGlow:
					assert(text || uiText);
					if(uiText)
						uiText->enableGlow(strTo<cocos2d::Color4B>(value));
					else if(text)
						text->enableGlow(strTo<cocos2d::Color4B>(value));
					break;
				case kDisableShadow:
					assert(text || uiText);
					if(uiText)
						uiText->disableEffect(cocos2d::LabelEffect::SHADOW);
					else if(text)
						text->disableEffect(cocos2d::LabelEffect::SHADOW);
					break;
				case kDisableOutline:
					assert(text || uiText);
					if(uiText)
						uiText->disableEffect(cocos2d::LabelEffect::OUTLINE);
					else if(text)
						text->disableEffect(cocos2d::LabelEffect::OUTLINE);
					break;
				case kDisableGlow:
					assert(text || uiText);
					if(uiText)
						uiText->disableEffect(cocos2d::LabelEffect::GLOW);
					else if(text)
						text->disableEffect(cocos2d::LabelEffect::GLOW);
					break;
				case kScaleEffect:
				{
					auto isEnabled = strTo<bool>(value);
					if(button)
					{
						button->setZoomScale(isEnabled  ? - 0.1f : 0.f);
						button->setPressedActionEnabled(isEnabled);
					}
					break;
				}
				case kProgressType:
					if(progress)
						progress->setType(value == "radial" ? cocos2d::ProgressTimer::Type::RADIAL : cocos2d::ProgressTimer::Type::BAR);
					else
						result = false;
					break;
				case kPercent:
					if(progress)
						progress->setPercentage(strTo<float>(value));
					else
						result = false;
					break;
				case kMidPoint:
					if(progress)
						progress->setMidpoint(strTo<cocos2d::Point>(value));
					else
						result = false;
					break;
				case kBarChangeRate:
					if(progress)
						progress->setBarChangeRate(strTo<cocos2d::Point>(value));
					else
						result = false;
					break;
				case kProgressImage:
					break;
                case kResource:
                {
                    auto particle = dynamic_cast<cocos2d::ParticleSystemQuad*>(node);
                    if(particle)
                    {
                        particle->initWithFile(value);
                    }
                    break;
                }
                case kPositionType:
                {
                    auto particle = dynamic_cast<cocos2d::ParticleSystemQuad*>(node);
                    if(particle)
                    {
                        auto pt = cocos2d::ParticleSystem::PositionType::FREE;
                        if(!value.empty() && value.front() == 'r')
                            pt = cocos2d::ParticleSystem::PositionType::RELATIVE;
                        else if(!value.empty() && value.front() == 'g')
                            pt = cocos2d::ParticleSystem::PositionType::GROUPED;
                        particle->setPositionType(pt);
                    }
                    break;
                }
				case kShaderProgram:
				{
					cocos2d::GLProgram* program(nullptr);
					if(value == "grayscale")
						program = cocos2d::GLProgramCache::getInstance()->getGLProgram(cocos2d::GLProgram::SHADER_NAME_POSITION_GRAYSCALE);
					else if(value == "default")
						program = cocos2d::GLProgramCache::getInstance()->getGLProgram(cocos2d::GLProgram::SHADER_NAME_POSITION_TEXTURE_COLOR_NO_MVP);
					else
						program = CustomShadersCache::shared().program(value);

					if(program)
						node->setGLProgram(program);
					else
						CCLOG("cannot create GL shader program by name or path: [%s]", value.c_str());
					break;
				}
				case k9Scale:
				{
					auto ui_image = dynamic_cast<cocos2d::ui::ImageView*>(node);
					assert(ui_image);
					if(ui_image)
					{
						ui_image->setScale9Enabled(true);
						ui_image->setCapInsets(strTo<cocos2d::Rect>(value));
					}
					break;
				}
				case kClipping:
				{
					auto layout = dynamic_cast<cocos2d::ui::Layout*>(node);
					if(layout)
					{
						layout->setClippingEnabled(strTo<bool>(value));
					}
					else
					{
						assert(0);
					}
					break;
				}
				case kDirection:
				{
					auto pageView = dynamic_cast<cocos2d::ui::PageView*>(node);
					cocos2d::ui::ScrollView::Direction direction = cocos2d::ui::ScrollView::Direction::HORIZONTAL;
					if(value.empty() == false)
					{
						if(value[0] == 'v')
							direction = cocos2d::ui::ScrollView::Direction::VERTICAL;
						else if(value[0] == 'h')
							direction = cocos2d::ui::ScrollView::Direction::HORIZONTAL;
						else if(value[0] == 'b')
							direction = cocos2d::ui::ScrollView::Direction::BOTH;
						else
							assert(0 && "unknow direction");
						
						if(scrollview)
							scrollview->setDirection(direction);
						else if(pageView)
							pageView->setDirection(direction);
						else
							assert(0);
					}
					break;
				}
				case kInnerSize:
				{
					if(scrollview)
					{
						auto size = strTo<cocos2d::Size>(value);
						scrollview->setInnerContainerSize(size);
					}
					else
					{
						assert(0);
					}
					break;
				}
				case kInnerPos:
				{
					if(scrollview)
					{
						auto pos = strTo<cocos2d::Point>(value);
						scrollview->setInnerContainerPosition(pos);
					}
					else
					{
						assert(0);
					}
					break;
				}
				case kInertiaScroll:
					if(scrollview)
						scrollview->setInertiaScrollEnabled(strTo<bool>(value));
					break;
				case kBounce:
					if(scrollview)
						scrollview->setBounceEnabled(strTo<bool>(value));
					break;
				case kScrollBarAutoHide:
					if(scrollview)
						scrollview->setScrollBarAutoHideEnabled(strTo<bool>(value));
					break;
				case kScrollBarWidth:
					if(scrollview)
						scrollview->setScrollBarWidth(strTo<float>(value));
					break;
				case kScrollBarColor:
					if(scrollview)
						scrollview->setScrollBarColor(strTo<cocos2d::Color3B>(value));
					break;
				case kScrollBarColor4:
					if(scrollview)
					{
						auto color = strTo<cocos2d::Color4B>(value);
						scrollview->setScrollBarColor(cocos2d::Color3B(color.r, color.g, color.b));
						scrollview->setScrollBarOpacity(color.a);
					}
					break;
				case kSliderBar:
				{
					if(slider)
					{
						auto set = [slider](const std::string& path, const cocos2d::ui::Widget::TextureResType& type)
						{
							slider->loadBarTexture(path, type);
						};
						auto scale9 = [slider, value](const cocos2d::Rect& rect)
						{
							check9Scale(slider, value, rect);
						};
						setImage(value, nullptr, nullptr, scale9, set);
					}
					break;
				}
				case kSliderProgressBar:
				{
					if(slider)
					{
						auto set = [slider](const std::string& path, const cocos2d::ui::Widget::TextureResType& type)
						{
							slider->loadProgressBarTexture(path, type);
						};
						auto scale9 = [slider, value](const cocos2d::Rect& rect)
						{
							check9Scale(slider, value, rect);
						};
						setImage(value, nullptr, nullptr, scale9, set);
					}
					break;
				}
				case kSliderBallN:
				{
					if(slider)
					{
						auto set = [slider](const std::string& path, const cocos2d::ui::Widget::TextureResType& type)
						{
							slider->loadSlidBallTextureNormal(path, type);
						};
						setImage(value, nullptr, nullptr, nullptr, set);
					}
					break;
				}
				case kSliderBallS:
				{
					if(slider)
					{
						auto set = [slider](const std::string& path, const cocos2d::ui::Widget::TextureResType& type)
						{
							slider->loadSlidBallTexturePressed(path, type);
						};
						setImage(value, nullptr, nullptr, nullptr, set);
					}
					break;
				}
				case kSliderBallD:
				{
					if(slider)
					{
						auto set = [slider](const std::string& path, const cocos2d::ui::Widget::TextureResType& type)
						{
							slider->loadSlidBallTextureDisabled(path, type);
						};
						setImage(value, nullptr, nullptr, nullptr, set);
					}
					break;
				}
				case kCursorEnabled:
					assert(textfield);
					if (textfield)
						textfield->setCursorEnabled(strTo<bool>(value));
					break;
				case kPlaceHolder:
                {
                    std::string string = macros::parse(value);
                    string = language.locale(string);
                    string = macros::parse(string);
					if (textfield)
						textfield->setPlaceHolder(string);
                    else if(editbox)
                        editbox->setPlaceHolder(string.c_str());
					break;
                }
                case kPlaceHolderColor:
                    if (textfield)
                        textfield->setPlaceHolderColor(strTo<cocos2d::Color3B>(value));
                    else if(editbox)
                        editbox->setPlaceholderFontColor(strTo<cocos2d::Color3B>(value));
                    break;
				case kIndicatorEnabled:
				{
					auto pageview = dynamic_cast<cocos2d::ui::PageView*>(node);
					if(pageview)
						pageview->setIndicatorEnabled(strTo<bool>(value));
					break;
				}
				case kIndicatorImage:
                {
                    auto pageview = dynamic_cast<cocos2d::ui::PageView*>(node);
                    auto set = [pageview](const std::string& path, const cocos2d::ui::Widget::TextureResType& type){
                        pageview->setIndicatorIndexNodesTexture(path, type);
                    };
                    if(pageview)
                        setImage(value, nullptr, nullptr, nullptr, set);
					break;
				}
                case kIndicatorColorSelected:
                {
                    auto pageview = dynamic_cast<cocos2d::ui::PageView*>(node);
                    if(pageview)
                        pageview->setIndicatorSelectedIndexColor(strTo<cocos2d::Color3B>(value));
                    break;
                }
                case kIndicatorColorNonActive:
                {
                    auto pageview = dynamic_cast<cocos2d::ui::PageView*>(node);
                    if(pageview)
                        pageview->setIndicatorIndexNodesColor(strTo<cocos2d::Color3B>(value));
                    break;
                }
				case kIndicatorPosition:
				{
					auto pageview = dynamic_cast<cocos2d::ui::PageView*>(node);
					if(pageview)
						pageview->setIndicatorPosition(strTo<cocos2d::Point>(value));
					break;
				}
                case kIndicatorSpace:
                {
                    auto pageview = dynamic_cast<cocos2d::ui::PageView*>(node);
                    if(pageview)
                        pageview->setIndicatorSpaceBetweenIndexNodes(strTo<float>(value));
                    break;
                }
                default:
                    result = false;
                    break;
			}
		}
		return result;
	}

	void bookDirectory(NodeExt* node)
	{
		_directories.push_back(node);
		if(node && node->as_node_pointer())
			node->as_node_pointer()->retain();
	}

	void unbookDirectory(NodeExt* node)
	{
		auto iter = std::find(_directories.begin(), _directories.end(), node);
		if(iter != _directories.end())
		{
			if(node && node->as_node_pointer())
				node->as_node_pointer()->release();
			_directories.erase(iter);
		}
	}

	cocos2d::ccMenuCallback get_callback_by_description(const std::string& description)
	{
		for(auto iter = _directories.rbegin(); iter != _directories.rend(); ++iter)
		{
			auto node = *iter;
			auto callback = node->get_callback_by_description(description);
			if(callback)
			{
				return callback;
			}
		}
		return nullptr;
	}
};

void setImage(const std::string& path,
			  const std::function<void(cocos2d::SpriteFrame*)>& setFrame,
			  const std::function<void(const std::string&)>& setTextureFile,
			  const std::function<void(const cocos2d::Rect&)>& check9scale,
			  const std::function<void(const std::string&, const cocos2d::ui::Widget::TextureResType& type)>& widgetSet)
{
    bool useTexture = true;
    cocos2d::Rect rect;
    if(xmlLoader::tryLoadPlist(path))
    {
        auto frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(path);
        if(frame)
        {
            widgetSet ? widgetSet(path, cocos2d::ui::Widget::TextureResType::PLIST) : setFrame(frame);
            rect = frame->getRect();
            useTexture = false;
        }
    }
    if(useTexture)
    {
        widgetSet ? widgetSet(path, cocos2d::ui::Widget::TextureResType::LOCAL) : setTextureFile(path);
    }
    if(check9scale)
    {
        check9scale(rect);
    }
}

void setTexture(cocos2d::Sprite* sprite, const std::string& path)
{
    auto frame = [sprite](cocos2d::SpriteFrame* frame)
    {
        sprite->setSpriteFrame(frame);
    };
    auto texture = [sprite](const std::string& texture)
    {
        sprite->setTexture(texture);
    };
    auto check9scale = [sprite, path](const cocos2d::Rect& rect)
    {
        xmlLoader::check9Scale(sprite, path, rect);
    };
    auto size = sprite->getContentSize();
    setImage(path, frame, texture, check9scale, nullptr);
    if(size.equals(cocos2d::Size::ZERO)==false)
    {
        sprite->setContentSize(size);
    }
}

cocos2d::Sprite* createSprite(const std::string& key)
{
    cocos2d::Sprite* sprite = nullptr;
    cocos2d::SpriteFrame* frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(key);
    
    if(!frame && xmlLoader::tryLoadPlist(key))
        frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(key);
    
    if(frame)
        sprite = cocos2d::Sprite::createWithSpriteFrame(frame);
    else
        sprite = cocos2d::Sprite::create(key);
    
    if(!sprite)
        cocos2d::log("cocos2d::Sprite with resource [%s] not created.", key.c_str());
    
    assert(sprite);
    return sprite;
}
