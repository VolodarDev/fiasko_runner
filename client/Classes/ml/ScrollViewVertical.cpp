/*
 *  Copyright 2018-2020 Vladimir Tolmachev
 *
 *   Author: Vladimir Tolmachev
 *  Project: Dungeon: Age of Heroes
 *   e-mail: tolm_vl@hotmail.com
 *
 * If you received the code is not the author, please contact me
 */

#include "ml/ScrollViewVertical.h"

ScrollViewVertical::ScrollViewVertical()
{
}

ScrollViewVertical::~ScrollViewVertical()
{
}

bool ScrollViewVertical::init()
{
    if (!cocos2d::ui::ScrollView::init() || !NodeExt::init())
        return false;
    
    setDirection(cocos2d::ui::ScrollView::Direction::VERTICAL);
    setBounceEnabled(true);
    setInertiaScrollEnabled(true);

    return true;
}

void ScrollViewVertical::onLoaded()
{
    NodeExt::onLoaded();
}

void ScrollViewVertical::doLayout()
{
    if (!_doLayoutDirty)
    {
        return;
    }
    
    cocos2d::ui::ScrollView::doLayout();
    
    _innerContainer->setLayoutType(cocos2d::ui::Layout::Type::VERTICAL);
    _innerContainer->setContentSize(cocos2d::Size::ZERO);
    xmlLoader::setProperty(_innerContainer, xmlLoader::kDoLayout, "auto_size");
    auto size = _innerContainer->getContentSize();
    size.width = getContentSize().width;
    size.height = -size.height;
    size.height = std::max(size.height, this->getContentSize().height);
    _innerContainer->setContentSize(size);
    _innerContainer->setPositionY(size.height);
    size.width = this->getInnerContainerSize().width;
    this->setInnerContainerSize(size);
}
