//
//  Exception.cpp
//  libEvent
//
//  Created by user-i157 on 14/02/17.
//  Copyright © 2017 user-i157. All rights reserved.
//

#include "ml/Exception.h"


Exception::Exception(const std::string& message) noexcept
: _message(message)
{
}

const char* Exception::what() const noexcept
{
    return _message.c_str();
}
