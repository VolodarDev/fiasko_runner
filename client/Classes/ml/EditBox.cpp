/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#include "ml/EditBox.h"

EditBox::EditBox()
{
    
}

EditBox::~EditBox()
{
    
}

bool EditBox::init()
{
    if(cocos2d::ui::EditBox::init())
    {
        return true;
    }
    return false;
}
