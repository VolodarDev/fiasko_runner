/******************************************************************************/
/*
 * Copyright 2014-2018 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: ml
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/


#include "ml/Button.h"
#include "ml/AudioEngine.h"

Button::Button()
: _fill(nullptr)
, _buildState(false)
, _isTextButton(false)
{
}

Button::~Button()
{
}

bool Button::init()
{
    if(cocos2d::ui::Button::init())
    {
        _buttonNormalRenderer->setName("normal");
        _buttonClickedRenderer->setName("selected");
        _buttonDisabledRenderer->setName("disabled");
        
        setZoomScale(-0.1f);
        setPressedActionEnabled(true);
        setSoundOnClick("audio/sound/button.mp3");
        
        return true;
    }
    return false;
}

cocos2d::Node* Button::getChildByName(const std::string& name) const
{
    if(name == _buttonNormalRenderer->getName())
        return _buttonNormalRenderer;
    if(name == _buttonClickedRenderer->getName())
        return _buttonClickedRenderer;
    if(name == _buttonDisabledRenderer->getName())
        return _buttonDisabledRenderer;
    if(name == "text" && _titleRenderer)
        return _titleRenderer;
    return cocos2d::ui::Button::getChildByName(name);
}

bool Button::setProperty(const std::string& name, const std::string& value)
{
    if(name == "frame_with_fill")
    {
        _frameWithFill = value;
        buildImage();
    }
    else if(name == "text_button")
    {
        _isTextButton = strTo<bool>(value);
        if(_isTextButton)
        {
            xmlLoader::setProperty(this, xmlLoader::kImageNormal, "point.png");
            getRendererNormal()->setOpacity(0);
        }
    }
    else
    {
        return NodeExt::setProperty(name, value);
    }
    return true;
}

void Button::setContentSize(const cocos2d::Size& size)
{
    cocos2d::ui::Button::setContentSize(size);
    buildImage();
}

void Button::onEnter()
{
    cocos2d::ui::Button::onEnter();
    if(_isTextButton)
    {
        setContentSize(getTitleRenderer()->getContentSize());
    }
}

void Button::setSoundOnClick(const std::string& path)
{
    addTouchEventListener([path](cocos2d::Ref*, cocos2d::ui::Widget::TouchEventType eventType){
        if(eventType == cocos2d::ui::Widget::TouchEventType::ENDED && !path.empty())
        {
            AudioEngine::shared().playEffect(path);
        }
    });
}

void Button::simulateClick()
{
    if(_clickEventListener)
        _clickEventListener(this);
}

void Button::onPressStateChangedToNormal()
{
    cocos2d::ui::Button::onPressStateChangedToNormal();
    if(!runEvent("enable"))
    {
        if(_fill)
        {
            xmlLoader::setProperty(_fill, xmlLoader::kShaderProgram, "default");
        }
        if(_titleRenderer)
        {
            _titleRenderer->setOpacity(255);
        }
    }
}

void Button::onPressStateChangedToDisabled()
{
    cocos2d::ui::Button::onPressStateChangedToDisabled();
    if(!runEvent("disable"))
    {
        if(_fill)
        {
            xmlLoader::setProperty(_fill, xmlLoader::kShaderProgram, "grayscale");
        }
        if(_titleRenderer)
        {
            _titleRenderer->setOpacity(128);
        }
    }
}

void Button::buildImage()
{
    //button_test _fill.png
    //button_test _frame.png
    
    if(_buildState || _frameWithFill.empty())
        return;
    _buildState = true;
    
    ParamCollection pc(_frameWithFill);
    auto texture = pc["texture"];
    auto x = pc.get<float>("x");
    auto y = pc.get<float>("y");
    
    auto size = getContentSize();
    xmlLoader::setProperty(this, xmlLoader::kImageNormal, texture + "_frame.png");
    if(!size.equals(cocos2d::Size::ZERO))
    {
        cocos2d::ui::Button::setContentSize(size);
    }
    else
    {
        size = getContentSize();
    }
    
    if(!_fill)
    {
        _fill = FillSprite::create();
        _buttonNormalRenderer->addChild(_fill, -1);
    }

    xmlLoader::setProperty(_fill, xmlLoader::kImage, texture + "_fill.png");
    _fill->setAnchorPoint(cocos2d::Point::ZERO);
    _fill->setContentSize(size - cocos2d::Size(x*2, y*2));
    _fill->setPosition(cocos2d::Point(x, y));
    
    _buildState = false;
}
