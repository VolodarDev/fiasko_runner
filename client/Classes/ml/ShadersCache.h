/******************************************************************************/
/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: ml
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/
/******************************************************************************/

#ifndef __ml_Shaders_Cache__
#define __ml_Shaders_Cache__
#include "cocos2d.h"
#include "ml/IntrusivePtr.h"
#include "ml/Singlton.h"


class CustomShadersCache : public Singlton < CustomShadersCache >
{
	friend class Singlton < CustomShadersCache > ;
	virtual void onCreate()override;
public:
	IntrusivePtr<cocos2d::GLProgram> program(const std::string& path);
private:
	void add(const std::string& path, cocos2d::GLProgram* program);
	void reload();
private:
	std::map<std::string, IntrusivePtr<cocos2d::GLProgram>> _programs;
};

#endif
