//
//  Text.hpp
//  cocos2d
//
//  Created by Vladimir Tolmachev on 20.10.2019.
//

#ifndef Text_hpp
#define Text_hpp

#include "ml/NodeExt.h"

class Text : public cocos2d::ui::Text, public NodeExt
{
    DECLARE_AUTOBUILDER(Text);
public:
    virtual bool init() override;
    virtual bool setProperty(const std::string& name, const std::string& value) override;
    virtual void setString(const std::string& text) override;
    void setStretch(Stretch stretch);
protected:
    Stretch _stretch;
};

#endif /* Text_hpp */
