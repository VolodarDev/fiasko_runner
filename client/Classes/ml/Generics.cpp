/******************************************************************************/
/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: ml
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/
/******************************************************************************/

#include <cstdint>
#include "mg/data/DataLocale.h"
#include "ml/common.h"

// from string

template <> std::string strTo(const std::string &value)
{
	return value;
}
template <> float strTo(const std::string &value)
{
	return strToFloat(value);
}
template <> int32_t strTo(const std::string &value)
{
	return strToInt(value);
}
template <> int64_t strTo(const std::string &value)
{
	return strToInt(value);
}
template <> uint32_t strTo(const std::string &value)
{
	return static_cast<uint32_t>(strToInt(value));
}
template <> uint64_t strTo(const std::string &value)
{
	return static_cast<uint64_t>(strToInt(value));
}
template <> bool strTo(const std::string &value)
{
	return strToBool(value);
}
template <> cocos2d::Point strTo(const std::string &value)
{
	return strToPoint(value);
}
template <> cocos2d::Size strTo(const std::string &value)
{
	return strToSize(value);
}
template <> cocos2d::Rect strTo(const std::string &value)
{
	return strToRect(value);
}
template <> cocos2d::Color3B strTo(const std::string &value)
{
	return strToColor3B(value);
}
template <> cocos2d::Color4B strTo(const std::string &value)
{
	return strToColor4B(value);
}
template <> cocos2d::BlendFunc strTo(const std::string &value)
{
	return strToBlendFunc(value);
}
template <> cocos2d::EventKeyboard::KeyCode strTo(const std::string &value)
{
	return strToKeyCode(value);
}
template <> Stretch strTo(const std::string &value)
{
	return strToStretch(value);
}
template <> Stretch::Mode strTo(const std::string &value)
{
    return strToStretchMode(value);
}
template <> cocos2d::Texture2D::PixelFormat strTo(const std::string &value)
{
    if(value == "RGB565")
        return cocos2d::Texture2D::PixelFormat::RGB565;
    if(value == "RGB888")
        return cocos2d::Texture2D::PixelFormat::RGB888;
    if(value == "BGRA8888")
        return cocos2d::Texture2D::PixelFormat::BGRA8888;
    if(value == "A8")
        return cocos2d::Texture2D::PixelFormat::A8;
    if(value == "I8")
        return cocos2d::Texture2D::PixelFormat::I8;
    if(value == "AI88")
        return cocos2d::Texture2D::PixelFormat::AI88;
    if(value == "RGBA4444")
        return cocos2d::Texture2D::PixelFormat::RGBA4444;
    if(value == "RGB5A1")
        return cocos2d::Texture2D::PixelFormat::RGB5A1;
    if(value == "PVRTC4")
        return cocos2d::Texture2D::PixelFormat::PVRTC4;
    if(value == "PVRTC4A")
        return cocos2d::Texture2D::PixelFormat::PVRTC4A;
    if(value == "PVRTC2")
        return cocos2d::Texture2D::PixelFormat::PVRTC2;
    if(value == "PVRTC2A")
        return cocos2d::Texture2D::PixelFormat::PVRTC2A;
    if(value == "ETC")
        return cocos2d::Texture2D::PixelFormat::ETC;
    if(value == "S3TC_DXT1")
        return cocos2d::Texture2D::PixelFormat::S3TC_DXT1;
    if(value == "S3TC_DXT3")
        return cocos2d::Texture2D::PixelFormat::S3TC_DXT3;
    if(value == "S3TC_DXT5")
        return cocos2d::Texture2D::PixelFormat::S3TC_DXT5;
    if(value == "ATC_RGB")
        return cocos2d::Texture2D::PixelFormat::ATC_RGB;
    if(value == "ATC_EXPLICIT_ALPHA")
        return cocos2d::Texture2D::PixelFormat::ATC_EXPLICIT_ALPHA;
    if(value == "ATC_INTERPOLATED_ALPHA")
        return cocos2d::Texture2D::PixelFormat::ATC_INTERPOLATED_ALPHA;
    if(value == "RGBA8888")
        return cocos2d::Texture2D::PixelFormat::RGBA8888;
    return cocos2d::Texture2D::PixelFormat::RGBA8888;
}


// to string

template <> std::string toStr(std::string value)
{
	return value;
}
template <> std::string toStr(char const * value)
{
	return std::string(value ? value : "");
}
template <> std::string toStr(const std::string &value)
{
	return value;
}
template <> std::string toStr(int value)
{
	return intToStr(value);
}
template <> std::string toStr(long value)
{
	return intToStr(static_cast<int>(value));
}
template <> std::string toStr(long long value)
{
	return intToStr(static_cast<int>(value));
}
template <> std::string toStr(unsigned int value)
{
	return intToStr(static_cast<int>(value));
}
template <> std::string toStr(unsigned long value)
{
	return intToStr(static_cast<int>(value));
}
template <> std::string toStr(unsigned long long value)
{
	return intToStr(static_cast<int>(value));
}
template <> std::string toStr(bool value)
{
	return boolToStr(value);
}
template <> std::string toStr(float value)
{
	return floatToStr(value);
}
template <> std::string toStr(cocos2d::Point value)
{
	return pointToStr(value);
}
template <> std::string toStr(cocos2d::Size value)
{
	return sizeToStr(value);
}
template <> std::string toStr(cocos2d::Rect value)
{
    return rectToStr(value);
}
template <> std::string toStr(cocos2d::Color3B value)
{
    return color3BToStr(value);
}
template <> std::string toStr(cocos2d::BlendFunc value)
{
    return blendFuncToStr(value);
}
template <> std::string toStr(const mg::DataLocale* locale)
{
    return locale->value;
}


//JSON
template <> void set(Json::Value& json, int8_t value) { json = value; }
template <> void set(Json::Value& json, int16_t value) { json = value; }
template <> void set(Json::Value& json, int32_t value) { json = value; }
template <> void set(Json::Value& json, int64_t value) { json = value; }
template <> void set(Json::Value& json, uint8_t value) { json = value; }
template <> void set(Json::Value& json, uint16_t value) { json = value; }
template <> void set(Json::Value& json, uint32_t value) { json = value; }
template <> void set(Json::Value& json, uint64_t value) { json = value; }
template <> void set(Json::Value& json, bool value) { json = value; }
template <> void set(Json::Value& json, float value) { json = value; }
template <> void set(Json::Value& json, std::string value) { json = value; }

template <> int8_t get(const Json::Value& json) { return json.asInt(); }
template <> int16_t get(const Json::Value& json) { return json.asInt(); }
template <> int32_t get(const Json::Value& json) { return json.asInt(); }
template <> int64_t get(const Json::Value& json) { return json.asInt64(); }
template <> uint8_t get(const Json::Value& json) { return json.asUInt(); }
template <> uint16_t get(const Json::Value& json) { return json.asUInt(); }
template <> uint32_t get(const Json::Value& json) { return json.asUInt(); }
template <> uint64_t get(const Json::Value& json) { return json.asUInt64(); }
template <> bool get(const Json::Value& json) { return json.asBool(); }
template <> float get(const Json::Value& json) { return json.asFloat(); }
template <> std::string get(const Json::Value& json) { return json.asString(); }

//XML
//template <class T> void set(pugi::xml_attribute& xml, T value);
//template <class T> T get(const pugi::xml_attribute& xml);

template <> void set(pugi::xml_attribute& xml, int8_t value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, int16_t value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, int32_t value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, int64_t value) { xml.set_value(static_cast<int32_t>(value)); }
template <> void set(pugi::xml_attribute& xml, uint8_t value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, uint16_t value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, uint32_t value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, uint64_t value) { xml.set_value(static_cast<uint32_t>(value)); }
template <> void set(pugi::xml_attribute& xml, bool value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, float value) { xml.set_value(value); }
template <> void set(pugi::xml_attribute& xml, std::string value) { xml.set_value(value.c_str()); }
template <> void set(pugi::xml_attribute& xml, cocos2d::Point value) { xml.set_value(toStr(value).c_str()); }
template <> void set(pugi::xml_attribute& xml, cocos2d::Rect value) { xml.set_value(toStr(value).c_str()); }

template <> int8_t get(const pugi::xml_attribute& xml) { return xml.as_int(); }
template <> int16_t get(const pugi::xml_attribute& xml) { return xml.as_int(); }
template <> int32_t get(const pugi::xml_attribute& xml) { return xml.as_int(); }
template <> int64_t get(const pugi::xml_attribute& xml) { return xml.as_int(); }
template <> uint8_t get(const pugi::xml_attribute& xml) { return xml.as_uint(); }
template <> uint16_t get(const pugi::xml_attribute& xml) { return xml.as_uint(); }
template <> uint32_t get(const pugi::xml_attribute& xml) { return xml.as_uint(); }
template <> uint64_t get(const pugi::xml_attribute& xml) { return xml.as_uint(); }
template <> bool get(const pugi::xml_attribute& xml) { return xml.as_bool(); }
template <> float get(const pugi::xml_attribute& xml) { return xml.as_float(); }
template <> std::string get(const pugi::xml_attribute& xml) { return xml.as_string(); }
template <> cocos2d::Point get(const pugi::xml_attribute& xml) { return strTo<cocos2d::Point>(xml.as_string()); }
template <> cocos2d::Rect get(const pugi::xml_attribute& xml) { return strTo<cocos2d::Rect>(xml.as_string()); }

