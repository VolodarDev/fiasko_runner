//
//  Exception.h
//  libEvent
//
//  Created by user-i157 on 14/02/17.
//  Copyright © 2017 user-i157. All rights reserved.
//

#ifndef Exception_h
#define Exception_h
#include <exception>
#include <string>

class Exception : public std::exception
{
public:
    Exception(const std::string& message) noexcept;
    virtual const char* what() const noexcept override;
private:
    std::string _message;
};

#endif /* Exception_h */
