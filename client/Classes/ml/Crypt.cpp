//
//  Crypt.cpp
//  dungeon
//
//  Created by Vladimir Tolmachev on 15.11.2019.
//

#include "ml/Crypt.h"
#include "base/base64.h"

#ifdef TESTS
#   include <iostream>
#endif

#if defined(NDEBUG) || defined(TESTS)
const char KEY[] = {0x5f, 0x4f, 0x72, 0x51};
#else
const char KEY[] = {0x0, 0x0, 0x0, 0x0};
#endif

void crypt::encode(std::string& data)
{
    size_t index = 0;
    for(char& c : data)
    {
        c = c ^ KEY[index++ % 4];
    }
}

void crypt::decode(std::string& data)
{
    size_t index = 0;
    for(char& c : data)
    {
        c = c ^ KEY[index++ % 4];
    }
}

std::string crypt::base64encode(const std::string& in)
{
    std::string out;
    char * buffer = nullptr;
    int len = cocos2d::base64Encode(reinterpret_cast<const unsigned char*>(in.data()), static_cast<unsigned int>(in.size()), &buffer);
    if(len > 0)
        out = buffer;
    else
        out = in;
    free(buffer);
    return out;
};

std::string crypt::base64decode(const std::string& in)
{
    std::string out;
    unsigned char * buffer = nullptr;
    int len = cocos2d::base64Decode(reinterpret_cast<const unsigned char*>(in.data()), static_cast<unsigned int>(in.size()), &buffer);
    if(len > 0)
        out = std::string(reinterpret_cast<const char*>(buffer), len);
    else
        out = in;
    free(buffer);
    return out;
};

#ifdef TESTS
bool crypt::tests::run()
{
    std::cout << "Crypt Key: " << KEY[0] << KEY[1] << KEY[2] << KEY[3] << std::endl;
    // test all symbols 1..127
    for(char c = 1; c < 127; ++c)
    {
        std::string data;
        data.push_back(c);
        
        encode(data);
        assert(data.size() == 1);
        assert(data[0] != c);
        
        decode(data);
        assert(data.size() == 1);
        assert(data[0] == c);
    }
    
    // test random text
    
    std::string randomText = "<CommandSequence>\n<commands>\n<ResponseUnitExpChanged data=\"hero_knight\" exp=\"1175\" has_level_up=\"true\" />\t</commands>\n</CommandSequence>";
    std::string original = randomText;
    
    encode(randomText);
    assert(randomText != original);
    assert(base64encode(randomText) != randomText);
    randomText = base64encode(randomText);
    randomText = base64decode(randomText);
    decode(randomText);
    assert(randomText == original);
    
    std::cout << "crypt:" << std::endl;
    std::cout << "  - Passed" << std::endl;

    return true;
}
#endif
