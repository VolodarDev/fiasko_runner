//
// Created by Vladimir Tolmachev on 2019-09-23.
//

#ifndef DUNGEON_COCOSACTIONBUILDER_H
#define DUNGEON_COCOSACTIONBUILDER_H

#include "cocos2d.h"

class CocosActionBuilder
{
public:
    CocosActionBuilder() = default;
    ~CocosActionBuilder() = default;
    CocosActionBuilder(const CocosActionBuilder&) = delete;
    CocosActionBuilder& operator=(const CocosActionBuilder&) = delete;
public:
    CocosActionBuilder& call(const std::function<void()>& function);
    CocosActionBuilder& delay(float duration);
    cocos2d::Sequence* build();
private:
    cocos2d::Vector<cocos2d::FiniteTimeAction*> _actions;
};

#endif //DUNGEON_COCOSACTIONBUILDER_H
