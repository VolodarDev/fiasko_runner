//
//  Settings.hpp
//  TextQuestEngine
//
//  Created by Vladimir Tolmachev on 02/09/2017.
//
//

#ifndef Settings_hpp
#define Settings_hpp

#include <string>
#include "ml/Observer.h"

class Settings
{
public:
	static Observer<std::function<void()>> eventSettingsChanged;
	
	static void loadUserSettings();
	static void apply();
};

#endif /* Settings_hpp */
