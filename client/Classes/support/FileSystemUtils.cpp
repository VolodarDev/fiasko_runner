//
//  FileSystemUtils.cpp
//  project_lord
//
//  Created by Vladimir Tolmachev on 15/04/2017.
//
//

#include "FileSystemUtils.h"
#include "cocos2d.h"

using namespace cocos2d;

namespace FileSystemUtils
{
#ifdef WIN32
	std::string projectName;
#else
	std::string projectName("project_fiasko/");
#endif

	void changeDefaultProjectName(const std::string& name)
	{
		projectName = name;
	}

	std::string getStringFromFile(const std::string& filePath)
	{
		return FileUtils::getInstance()->getStringFromFile(filePath);
	}
	
	cocos2d::Data getDataFromFile(const std::string& filePath)
	{
		return FileUtils::getInstance()->getDataFromFile(filePath);
	}
	
	std::string getWritablePath()
	{
		static std::string writablePath;
		if(writablePath.empty())
		{
			std::string defaultPath = FileUtils::getInstance()->getWritablePath();
			if(defaultPath.back() != '/')
				defaultPath.push_back('/');
			writablePath = defaultPath + projectName;
		}
		return writablePath;
	}
	
	std::string getAssetsPath()
	{
		return "";
	}
	
	void saveXml(const pugi::xml_document& doc, const std::string& fullpath, const std::string& indent)
	{
		std::stringstream stream;
		pugi::xml_writer_stream writer(stream);
		auto flags = 0;
		flags |= pugi::format_no_declaration;
		flags |= pugi::format_indent;
		//flags |= pugi::format_no_escapes;
		//flags |= pugi::format_raw;
		doc.save(writer, indent.c_str(), flags, pugi::xml_encoding::encoding_utf8);
		
		auto dir = fullpath.substr(0, fullpath.find_last_of("/"));
		FileUtils::getInstance()->createDirectory(dir);
		FileUtils::getInstance()->writeStringToFile(stream.str(), fullpath);
	}
	
	bool isFileExist(const std::string& filePath)
	{
		return FileUtils::getInstance()->isFileExist(filePath);
	}
	
	void createDirectory(const std::string& path)
	{
		FileUtils::getInstance()->createDirectory(path);
	}
	
	void createDirectoryForFile(const std::string& filePath)
	{
		auto k = filePath.rfind('/');
		if(k != std::string::npos)
		{
			auto dir = filePath.substr(0, k);
			FileUtils::getInstance()->createDirectory(dir);
		}
	}
	
	void removeFile(const std::string& filePath)
	{
		FileUtils::getInstance()->removeFile(filePath);
	}
	
//	std::string getMd5(const std::string& path)
//	{
//		auto data = cocos2d::FileUtils::getInstance()->getDataFromFile(path);
//		MD5 md5;
//		md5.update(data.getBytes(), data.getSize());
//		md5.finalize();
//		auto result = md5.hexdigest();
//		return result;
//	}
//	
}
