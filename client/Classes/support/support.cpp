#include "support.h"
#include "mg/SerializedObject.h"
#include "cocos2d.h"
#include "ml/Generics.h"
#include <time.h>

void log(mg::SerializedObject* object)
{
	pugi::xml_document doc;
	auto root = doc.append_child(object->get_type().c_str());
	object->serialize(root);

	std::stringstream stream;
	pugi::xml_writer_stream writer(stream);
	doc.save(writer, "\t", 1U, pugi::xml_encoding::encoding_utf8);
	cocos2d::log("%s", stream.str().c_str());
	//Json::Value json;
	//Json::StreamWriterBuilder wbuilder;
	//wbuilder["indentation"] = "  ";
	//object->serialize(json);
	//auto string = Json::writeString(wbuilder, json);
	//cocos2d::log("%s", string.c_str());
}

std::string formatTimeDuration(int duration)
{
	duration = std::max(0, duration);

	int D = duration / (3600 * 24);
	duration = duration % (3600 * 24);
	int H = duration / 3600;
	duration = duration % 3600;
	int M = duration / 60;
	duration = duration % 60;
	int S = duration;

	if (D > 0)
	{
		if (H > 0)
			return cocos2d::StringUtils::format("%d day %d hr", D, H);
		else
			return cocos2d::StringUtils::format("%d day", D);
	}
	if (H > 0)
	{
		if (M > 0)
			return cocos2d::StringUtils::format("%d hr %d min", H, M);
		else
			return cocos2d::StringUtils::format("%d hr", H);
	}
	if (M > 0)
	{
		if (S > 0)
			return cocos2d::StringUtils::format("%d min %d sec", M, S);
		else
			return cocos2d::StringUtils::format("%d min", M);
	}
	return cocos2d::StringUtils::format("%d sec", S);
}

std::string formatResourceValue(int count)
{
	int c = static_cast<int>(std::fabs(count));
	int index = 4;
	auto str = toStr(c);
	if (c >= 10 * 1000 * 1000)
	{
		str = toStr(c / (1000 * 1000)) + "M";
	}
	else if (c >= 10 * 1000)
	{
		str = toStr(c / (1000)) + "K";
	}
	else
	{
		index = 3;
	}
	if (static_cast<int>(str.size()) > index)
	{
		int k = static_cast<int>(str.size()) - index;
		str.insert(str.begin() + k, '\'');
	}
	if(count < 0)
		str = "-" + str;
	return str;
}

std::string formatFunds(int count)
{
	auto str = toStr(count);
	if (str.size() > 3)
	{
		int k = static_cast<int>(str.size()) - 3;
		str.insert(str.begin() + k, '\'');
	}
	if (str.size() > 7)
	{
		int k = static_cast<int>(str.size()) - 7;
		str.insert(str.begin() + k, '\'');
	}
	if (str.size() > 11)
	{
		int k = static_cast<int>(str.size()) - 11;
		str.insert(str.begin() + k, '\'');
	}
	return str;
}

std::string formatTime(int itime)
{
	time_t time = itime;
	auto ltime = localtime(&time);
	char buffer[32];
	strftime(buffer, sizeof(buffer), "%T", ltime);
	std::string result = buffer;
	return result;
}

std::string formatDate(int itime)
{
	time_t time = itime;
	auto ltime = localtime(&time);
	char buffer[32];
	strftime(buffer, sizeof(buffer), "%d.%m.%y", ltime);
	std::string result = buffer;
	return result;
}

std::string formatDateTime(int itime, const std::string& divider, int currentTime)
{
	auto currentDate = formatDate(currentTime);
	auto date = formatDate(itime);
	auto time = formatTime(itime);
	if(date == currentDate)
	{
		return time;
	}
	else
	{
		return date + divider + time;
	}
}
