#ifndef AppArguments_hpp
#define AppArguments_hpp

#include <stdio.h>
#include <string>
#include <map>
#include <vector>

class AppArguments
{
public:
	static AppArguments inst;

	bool has(const std::string& name)const;
	std::string get(const std::string& name)const;
	
	void parse(int argc, char **argv);
#ifdef WIN32
	void parse(int nCmdShow, std::vector<std::string> attrs);
#endif
private:
	std::map<std::string, std::string> _args;
};

#endif /* AppArguments_hpp */
