//
//  Settings.cpp
//  TextQuestEngine
//
//  Created by Vladimir Tolmachev on 02/09/2017.
//
//

#include "Settings.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/Generics.h"
#include "UserData.h"

Observer<std::function<void()>> Settings::eventSettingsChanged;

void Settings::loadUserSettings()
{
}

void Settings::apply()
{
}

