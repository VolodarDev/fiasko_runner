#ifndef __SUPPORT_H__
#define __SUPPORT_H__

#include <string>

namespace mg
{
	class SerializedObject;
}

void log(mg::SerializedObject* object);

std::string formatTimeDuration(int duration);
std::string formatResourceValue(int count);
std::string formatFunds(int count);

/*
 * get string with only time in format "HH:MM:SS"
 */
std::string formatTime(int time);

/*
 * get string with only date in format "DD.MM.YY"
 */
std::string formatDate(int time);

/*
 * get string with date and time in format "DD.MM.YY HH:MM:SS"
 */
std::string formatDateTime(int time, const std::string& divider = " ", int currentTime = 0);

#endif
