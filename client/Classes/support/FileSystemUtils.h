//
//  FileSystemUtils.hpp
//  project_lord
//
//  Created by Vladimir Tolmachev on 15/04/2017.
//
//

#ifndef FileSystemUtils_hpp
#define FileSystemUtils_hpp
#include <string>
#include <vector>
#include "pugixml/pugixml.hpp"

namespace cocos2d
{
	class Data;
}

namespace FileSystemUtils
{
	void changeDefaultProjectName(const std::string& name);
	std::string getStringFromFile(const std::string& filePath);
	cocos2d::Data getDataFromFile(const std::string& filePath);
	
	std::string getWritablePath();
	std::string getAssetsPath();
	
	void saveXml(const pugi::xml_document& doc, const std::string& fullpath, const std::string& indent = " ");
	
	bool isFileExist(const std::string& filePath);
	void createDirectory(const std::string& path);
	void createDirectoryForFile(const std::string& filePath);
	void removeFile(const std::string& filePath);
}

#endif /* FileSystemUtils_hpp */
