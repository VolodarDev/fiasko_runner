#include "AppArguments.h"

AppArguments AppArguments::inst;

void AppArguments::parse(int argc, char **argv)
{
	for(int i=0; i < argc-2; i += 2)
	{
		std::string name = argv[i + 1];
		std::string value = argv[i + 2];
		_args[name] = value;
	}
}

#ifdef WIN32
void AppArguments::parse(int nCmdShow, std::vector<std::string> attrs)
{
	for(int i=0; i < static_cast<int>(attrs.size())-1; i += 2)
	{
		std::string name = attrs[i];
		std::string value = attrs[i + 1];
		_args[name] = value;
	}
}
#endif


std::string AppArguments::get(const std::string& name)const
{
	return _args.count(name) ? _args.at(name) : "";
}

bool AppArguments::has(const std::string& name)const
{
	return _args.count(name) > 0;
}
