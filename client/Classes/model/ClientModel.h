#ifndef __ClientModel_h__
#define __ClientModel_h__

#include "cocos2d.h"
#include "ml/Singlton.h"
#include "ml/Observer.h"
#include "mg/model/Model.h"
#include "mg/DataStorage.h"

class RequestManager;
class ResponseHandler;

namespace mg
{
	class Request;
	class Response;
	class ModelUser;
}

class ClientModel : public Singlton<ClientModel>, public mg::Model
{
	friend class ResponseHandler;
	friend class UpdateManager;
	friend class Singlton<ClientModel>;
	ClientModel();
	virtual void onCreate()override;
public:
	void initialize();
	void save();
	void reset();
	
	void onAppPaused();
	void onAppResumed();

	void update(float dt);
protected:
	time_t _timeOnAppPaused;
};

#endif
