#include "ClientModel.h"
#include "support/FileSystemUtils.h"
#include "ml/loadxml/xmlLoader.h"

using namespace cocos2d;

ClientModel::ClientModel()
: _timeOnAppPaused(0)
{
}

void ClientModel::onCreate()
{
}

void ClientModel::initialize()
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	user = make_intrusive<mg::ModelUser>();

	auto path = FileSystemUtils::getWritablePath() + "model.xml";
	if(FileSystemUtils::isFileExist(path))
	{
		auto doc = xmlLoader::loadDoc(path);
		auto root = doc->root().first_child();
		get_user()->deserialize(root.child("user"));
		auto params = const_cast<mg::DataGameParams*>(mg::DataStorage::shared().get<mg::DataGameParams>("default"));
		*params = user->params;
	}
	else
	{
		get_user()->initialize();
		user->params = *mg::DataStorage::shared().get<mg::DataGameParams>("default");
		save();
	}
#endif
}

void ClientModel::save()
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	get_user()->params = *mg::DataStorage::shared().get<mg::DataGameParams>("default");

	auto path = FileSystemUtils::getWritablePath() + "model.xml";
	pugi::xml_document doc;
	auto root = doc.root().append_child("root");
	
	get_user()->serialize(root.append_child("user"));
	
	FileSystemUtils::saveXml(doc, path);
#endif
}

void ClientModel::reset()
{
	user = nullptr;
}

void ClientModel::onAppPaused()
{
	_timeOnAppPaused = time(nullptr);
}

void ClientModel::onAppResumed()
{
//	auto elapsed = _timeOnAppPaused > 0 ? time(nullptr) - _timeOnAppPaused : 0;
//	_timeOnAppPaused = 0;
}

void ClientModel::update(float dt)
{
}

