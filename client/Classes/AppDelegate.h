#ifndef  _APP_DELEGATE_H_
#define  _APP_DELEGATE_H_

#include "cocos2d.h"

class AppArguments;

class  AppDelegate : public cocos2d::Application
{
public:
	AppDelegate();
    virtual ~AppDelegate();
	

    virtual void initGLContextAttrs();
    virtual bool applicationDidFinishLaunching();
    virtual void applicationDidEnterBackground();
    virtual void applicationWillEnterForeground();
	
	void launch();
	
	static void configurePaths();
private:
	void createWindow();
	void changeDesignResolution();
	void registration();
	void centerWindow();
};

#endif // _APP_DELEGATE_H_

