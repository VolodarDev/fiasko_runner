/******************************************************************************/
/*
 * Copyright 2014-2017 Vladimir Tolmachev
 *
 * Author: Vladimir Tolmachev
 * Project: Defense of Greece
 * e-mail: tolm_vl@hotmail.com
 * If you received the code is not the author, please contact me
 */
/******************************************************************************/

#include "cocos2d.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "Purchase.h"
#include "ml/JniBind.h"
#include "ml/ParamCollection.h"
#include "ml/common.h"

USING_NS_CC;

static const char * Activity = "org.cocos2dx.cpp/InApps";

extern
"C"
{
	JNIEXPORT void JNICALL  Java_org_cocos2dx_cpp_InApps_nativeResultDetails(JNIEnv* env, jobject thiz,
		bool requestResult,
		jstring jproductId,
		jstring jtitle,
		jstring jdescription,
		jstring jpriceText,
		jstring jcurrency,
		float priceValue)
	{
		cocos2d::log("test");
		inapp::SkuDetails result;
		result.result = requestResult ? inapp::Result::Ok : inapp::Result::Fail;
		if( result.result == inapp::Result::Ok )
		{
			result.productId = JniHelper::jstring2string(jproductId);
			result.title = JniHelper::jstring2string(jtitle);
			result.description = JniHelper::jstring2string(jdescription);
			result.priceText = JniHelper::jstring2string(jpriceText);
			result.currency = JniHelper::jstring2string(jcurrency);
			result.priceValue = priceValue;
			
			if( result.currency != "RUB" && result.currency != "USD" )
			{
				result.priceText = toStr( result.priceValue ) + " " + result.currency;
			}
		}
		inapp::requestResultsDetails( result );
	}
	
	JNIEXPORT void JNICALL  Java_org_cocos2dx_cpp_InApps_nativeResultPurchase(JNIEnv*  env, jobject thiz,
		jstring jproductId,
		int errorCode,
		jstring jerrorMessage,
		bool purchaseSuccess)
	{
		inapp::PurchaseResult result;
		result.result = purchaseSuccess ? inapp::Result::Ok : inapp::Result::Fail;
		result.productId = JniHelper::jstring2string(jproductId);
		result.errorcode = errorCode;
		result.errormsg = JniHelper::jstring2string(jerrorMessage);
		
		inapp::requestResultsPurchase( result );
	}
}

void inapp::requestDetails( const std::string & productID )
{
	JavaBind bind("org.cocos2dx.cpp", "InApps", "requestDetails", "%s");
	bind.call( productID );
}

void inapp::requestPurchase( const std::string & productID )
{
	JavaBind bind("org.cocos2dx.cpp", "InApps", "purchase", "%s");
	bind.call( productID );
}

void inapp::confirm( const std::string & product )
{
	JavaBind bind("org.cocos2dx.cpp", "InApps", "consume", "%s");
	bind.call( product );
}

void inapp::restore()
{}

bool inapp::isPurchased( const std::string& product )
{
	JavaBind bind( "org.cocos2dx.cpp", "InApps", "isPurchased", "%s" );
	return bind.bool_call( product );
}

#endif
