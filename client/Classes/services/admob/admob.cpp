//
//  admob.cpp
//  fiasko
//
//  Created by Vladimir Tolmachev on 23.08.2020.
//

#include "admob.h"
#include "cocos2d.h"


#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID

#include "ml/JniBind.h"

void admob::showInterstitial()
{
    JavaBind bind("org.cocos2dx.cpp.ads", "AdsAdmob", "showInterstial", "");
    bind.call();
}
#endif


#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_WIN32
void admob::showInterstitial()
{
}
#endif

