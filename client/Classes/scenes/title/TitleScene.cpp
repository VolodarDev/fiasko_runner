#include "TitleScene.h"
#include "ml/loadxml/xmlProperties.h"
#include "scenes/BaseController.h"
#include "model/ClientModel.h"
#include "xml/types.h"

using namespace cocos2d;

TitleScene::TitleScene()
{
}

TitleScene::~TitleScene()
{
}

bool TitleScene::init()
{
	if( !BaseScene::init() )
		return false;

	BaseScene::_controller = std::make_shared<BaseController>(this);

	xmlLoader::bookDirectory(this);
	load(xml::scenesTitle::SCENE);
	xmlLoader::unbookDirectory(this);
	
	connectExtension(this, _TitleLayer);

	return true;
}
