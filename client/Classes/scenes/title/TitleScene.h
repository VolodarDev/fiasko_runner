#ifndef __TitleScene_h__
#define __TitleScene_h__

#include "cocos2d.h"
#include "scenes/BaseScene.h"
#include "TitleLayer.h"

class TitleScene : public BaseScene
{
public:
	DECLARE_AUTOBUILDER(TitleScene);
	virtual bool init() override;
public:
private:
	TitleLayer::Pointer _TitleLayer;
};

#endif
