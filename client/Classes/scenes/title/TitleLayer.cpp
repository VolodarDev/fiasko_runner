#include "TitleLayer.h"
#include "scenes/BaseController.h"
#include "model/ClientModel.h"
#include "support/UserData.h"
#include "ml/AudioEngine.h"
#include "inapp.h"

using namespace cocos2d;



static float _detailsRecieved = false;
TitleLayer* _instance(nullptr);

TitleLayer::TitleLayer()
: _controller(nullptr)
{
	_instance = this;
}

TitleLayer::~TitleLayer()
{
	_instance = nullptr;
}

bool TitleLayer::init()
{
	if( !LayerExt::init() )
		return false;
    
    setAudioEnable(UserData::shared().is_audio_enabled());
    
	setDispatchKeyBack(true);
	return true;
}

void TitleLayer::onLoaded()
{
	LayerExt::onLoaded();
	checkAudioEnabled();
    
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
    getNodeByPath(this, "restore")->setVisible(false);
#endif
	
	if(!UserData::shared().get<bool>("noads"))
	{
		if(!_detailsRecieved)
			details();
		else
			getNodeByPath(this, "no_ads")->setVisible(true);
	}
	else
	{
		getNodeByPath(this, "no_ads")->setVisible(false);
		getNodeByPath(this, "restore")->setVisible(false);
	}
	
	auto max = UserData::shared().get<int>("score_max");
	auto text = getNodeByPath<ui::Text>(this, "score/value");
	text->setString(toStr(max));
}

void TitleLayer::disappearance(bool force)
{
	_controller->runSceneTitle();
}

ccMenuCallback TitleLayer::get_callback_by_description(const std::string & name)
{
	if(name == "audio_off") return [this](Ref*){
		setAudioEnable(false);
	};
	if(name == "audio_on") return [this](Ref*){
		setAudioEnable(true);
	};
	if(name == "purchase:no_ads") return [this](Ref*){
		purchase();
	};
	if(name == "restore_purchases") return [this](Ref*){
		restore();
	};
	return LayerExt::get_callback_by_description(name);
}

void TitleLayer::setAudioEnable(bool mode)
{
	mode ? AudioEngine::shared().enableAudio() : AudioEngine::shared().disableAudio();
	checkAudioEnabled();
}

void TitleLayer::checkAudioEnabled()
{
	bool enabled = AudioEngine::shared().isAudioEnabled();
	if(getNodeByPath(this, "audio_on"))
        getNodeByPath(this, "audio_on")->setVisible(enabled);
	if(getNodeByPath(this, "audio_off"))
        getNodeByPath(this, "audio_off")->setVisible(!enabled);
}

void TitleLayer::details()
{
	retain();
	inapp::details(inappNoAds, std::bind(&TitleLayer::detailResult, this, std::placeholders::_1));
}

void TitleLayer::detailResult(inapp::SkuDetails details)
{
	_detailsRecieved = true;
	getNodeByPath(this, "no_ads")->setVisible(true);
	release();
}

void TitleLayer::purchase()
{
	retain();
	inapp::purchase(inappNoAds, std::bind(&TitleLayer::purchaseResult, this, std::placeholders::_1));
#if CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID
	getNodeByPath(this, "block")->setVisible(true);
#endif
	runAction(Sequence::create(DelayTime::create(20), CallFunc::create([this](){
		getNodeByPath(this, "block")->setVisible(false);
	}), nullptr));
}

void TitleLayer::purchaseResult(inapp::PurchaseResult result)
{
	if(result.result == inapp::Result::Ok)
	{
		getNodeByPath(this, "no_ads")->setVisible(false);
		getNodeByPath(this, "restore")->setVisible(false);
		
		UserData::shared().write("noads", true);
		UserData::shared().save();
		inapp::confirm(result.productId);
	}
	getNodeByPath(this, "block")->setVisible(false);
	release();
}

void TitleLayer::restore()
{
	inapp::restore();
}

void inapp::onRestored( std::string item )
{
	if( item == inappNoAds )
	{
		UserData::shared().write("noads", true);
		UserData::shared().save();
		if(_instance)
		{
			getNodeByPath(_instance, "no_ads")->setVisible(false);
			getNodeByPath(_instance, "restore")->setVisible(false);
		}
	}
}
