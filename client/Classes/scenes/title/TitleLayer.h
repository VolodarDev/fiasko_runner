#ifndef __TitleLayer_h__
#define __TitleLayer_h__

#include "cocos2d.h"
#include "ml/NodeExt.h"
#include "services/inapp/Purchase.h"

class BaseController;

class TitleLayer : public LayerExt
{
public:
	DECLARE_AUTOBUILDER( TitleLayer );
	virtual bool init()override;
	virtual void onLoaded()override;
	virtual void disappearance(bool force = true)override;
	virtual cocos2d::ccMenuCallback get_callback_by_description( const std::string & name )override;
protected:
	void setAudioEnable(bool mode);
	void checkAudioEnabled();
	
	void details();
	void detailResult(inapp::SkuDetails details);
	void purchase();
	void purchaseResult(inapp::PurchaseResult result);
	void restore();
private:
	BaseController* _controller;
};

#endif
