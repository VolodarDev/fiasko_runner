#ifndef __BaseController_h__
#define __BaseController_h__

#include "BaseScene.h"

class ClientModel;
class BaseScene;

namespace mg
{
}

class BaseController
{
public:
	BaseController(BaseScene* scene);
	virtual ~BaseController();

public:
	BaseScene* getScene();
	
	virtual void update(float dt);

	void runSceneTitle();
	void runSceneGame();
	
	void openSettingsWindow();
	void openFiaskoWindow();
	
protected:
	void runScene(SceneId id);
protected:
	BaseScene* _scene;
	ClientModel* _model;
};

#endif
