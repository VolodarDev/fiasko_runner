#ifndef __BaseWindow_h__
#define __BaseWindow_h__

#include "cocos2d.h"
#include "ml/NodeExt.h"

class BaseController;

class BaseWindow : public LayerExt
{
public:
	DECLARE_AUTOBUILDER( BaseWindow );
	virtual bool init(const std::shared_ptr<BaseController>& controller);
public:
	virtual void close();
	
	virtual cocos2d::ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual bool loadXmlEntity( const std::string & tag, const pugi::xml_node & xmlnode )override;
	virtual void onLoaded()override;
	
	static cocos2d::Size getLayoutInnerSize(cocos2d::ui::Layout* layout);
	static void buildScrollVTable(cocos2d::ui::ScrollView* scroll, const std::vector<intrusive_ptr<cocos2d::Node>>& panels, int cols);
	static void buildScrollVTable(cocos2d::ui::ScrollView* scroll, const std::vector<intrusive_ptr<cocos2d::Node>>& lines, const std::vector<intrusive_ptr<cocos2d::Node>>& panels, int cols);
	static void buildScroll(cocos2d::ui::ScrollView* scroll, const std::vector<intrusive_ptr<cocos2d::Node>>& panels, cocos2d::ui::Layout::Type type);
protected:
	std::shared_ptr<BaseController> _controller;

private:
	
public:
#if DEV==1
	std::string previewSceneTexture;
	intrusive_ptr<cocos2d::Sprite> previewSceneSprite;
#endif
};

#endif
