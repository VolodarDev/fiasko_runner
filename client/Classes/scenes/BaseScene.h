#ifndef __BaseScene_h__
#define __BaseScene_h__

#include "cocos2d.h"
#include "ml/NodeExt.h"
#include "ml/loadxml/xmlProperties.h"
#include <memory>
#include "scenes/BaseWindow.h"

class BaseController;

enum class SceneId
{
	loader,
	title,
	game,
};

class BaseScene : public cocos2d::Scene, public NodeExt
{
public:
	DECLARE_AUTOBUILDER( BaseScene );
	virtual bool init()override;
public:
	virtual bool loadXmlEntity( const std::string & tag, const pugi::xml_node & xmlnode )override;
	virtual cocos2d::ccMenuCallback get_callback_by_description( const std::string & name )override;
	virtual void onLoaded()override;
	virtual void update(float dt)override;

	template <class T, class...TArgs>
	void openWindow(const std::string& config, TArgs && ... _Args)
	{
		auto opened = getOpenedWindow<T>();
		if(opened)
		{
			opened->close();
		}
		auto window = T::create(_controller, std::forward<TArgs>(_Args)...);
		xmlLoader::bookDirectory(this);
		window->load(config);
		xmlLoader::unbookDirectory(this);
		openWindow(window);
	}
	void openWindow(intrusive_ptr<BaseWindow> window);
	
	template <class T>
	intrusive_ptr<T> getOpenedWindow()
	{
		for(auto window : _windows)
		{
			auto p = dynamic_cast<T*>(window.ptr());
			if(p)
				return intrusive_ptr<T>(p);
		}
		return nullptr;
	}
	
	BaseWindow* getTopWindow();
	
protected:
	std::shared_ptr<BaseController> _controller;
	std::vector<intrusive_ptr<BaseWindow>> _windows;
#if DEV==1
	std::string _previewSceneTexture;
	intrusive_ptr<cocos2d::Sprite> _previewSceneSprite;
#endif
};

#endif
