#include "scenes/BaseController.h"
#include "scenes/BaseScene.h"
#include "scenes/loader/LoaderScene.h"
#include "scenes/windows/SettingsWindow.h"
#include "model/ClientModel.h"
#include "xml/types.h"

using namespace cocos2d;

BaseController::BaseController(BaseScene* scene)
	: _scene(scene)
	, _model(&ClientModel::shared())
{
}

BaseController::~BaseController()
{
}

BaseScene* BaseController::getScene()
{
	return _scene;
}

void BaseController::update(float dt)
{
	
}

void BaseController::runSceneTitle()
{
	runScene(SceneId::title);
}

void BaseController::runSceneGame()
{
	runScene(SceneId::game);
}

void BaseController::runScene(SceneId id)
{
	auto scene = LoaderScene::create(id);
	if(scene->hasJobs())
	{
		Director::getInstance()->replaceScene(scene);
	}
	else
	{
		scene->runScene();
	}
}

void BaseController::openSettingsWindow()
{
	_scene->openWindow<SettingsWindow>(xml::scenesWindows::SETTINGS);
}

void BaseController::openFiaskoWindow()
{
	_scene->openWindow<BaseWindow>(xml::scenesWindows::FIASKO);
	auto window = _scene->getTopWindow();
	window->setDispatchKeyBack(false);
}

