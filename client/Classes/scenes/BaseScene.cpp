#include "scenes/BaseScene.h"
#include "scenes/BaseController.h"
#include "scenes/loader/LoaderScene.h"
#include "mg/DataStorage.h"
#include "AppDelegate.h"

using namespace cocos2d;

BaseScene::BaseScene()
{
}

BaseScene::~BaseScene()
{
	removeAllChildren();
//    SpriteFrameCache::getInstance()->removeUnusedSpriteFrames();
//    Director::getInstance()->getTextureCache()->removeUnusedTextures();
}

bool BaseScene::init()
{
	Scene::init();
	scheduleUpdate();
#if DEV==1
	EventListenerKeyboard * event = EventListenerKeyboard::create();
	event->onKeyReleased = [this](EventKeyboard::KeyCode key, Event* event)mutable
	{
		if(key == EventKeyboard::KeyCode::KEY_F2)
		{
			cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([](){
				auto app = dynamic_cast<AppDelegate*>(Application::getInstance());
				app->launch();
			});
		}
		if(key == EventKeyboard::KeyCode::KEY_P)
		{
			auto previewSprite = _previewSceneSprite;
			auto previewSceneTexture =_previewSceneTexture;
			
			auto window = getTopWindow();
			if(window)
			{
				previewSprite = window->previewSceneSprite;
				previewSceneTexture = window->previewSceneTexture;
			}
			if(previewSprite != _previewSceneSprite && _previewSceneSprite)
			{
				_previewSceneSprite->removeFromParent();
				_previewSceneSprite = nullptr;
			}
			
			if(!previewSprite && !previewSceneTexture.empty())
			{
				previewSprite = cocos2d::Sprite::create(previewSceneTexture);
				addChild(previewSprite, INT_MAX);
				auto size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
				previewSprite->setPosition(Point(size/2));
				previewSprite->setVisible(false);
			}
			if(previewSprite)
				previewSprite->setVisible(!previewSprite->isVisible());
			if(window)
				window->previewSceneSprite = previewSprite;
			else
				_previewSceneSprite = previewSprite;
		}
		if(key == EventKeyboard::KeyCode::KEY_O)
		{
			auto previewSceneSprite = _previewSceneSprite;
			auto window = getTopWindow();
			if(window)
				previewSceneSprite = window->previewSceneSprite;

			if( previewSceneSprite && previewSceneSprite->isVisible())
			{
				auto opacity = previewSceneSprite->getOpacity();
				opacity = opacity == 255 ? 128 : 255;
				previewSceneSprite->setOpacity(opacity);
			}
		}
	};
	getEventDispatcher()->addEventListenerWithSceneGraphPriority( event, this );
#endif
	return true;
}

bool BaseScene::loadXmlEntity( const std::string & tag, const pugi::xml_node & xmlnode )
{
#if DEV==1
	if(tag == "preview")
		_previewSceneTexture = xmlnode.text().as_string();
	else
#endif
		return NodeExt::loadXmlEntity(tag, xmlnode);
	return true;
}

void BaseScene::onLoaded()
{
	NodeExt::onLoaded();
}

BaseWindow* BaseScene::getTopWindow()
{
	return _windows.empty() ? nullptr : _windows.back();
}

void BaseScene::update(float dt)
{
	_controller->update(dt);
}

cocos2d::ccMenuCallback BaseScene::get_callback_by_description( const std::string & name )
{
	if (name == "run_title") return [this](Ref*){
		this->_controller->runSceneTitle();
	};
	if (name == "run_game") return [this](Ref*){
		this->_controller->runSceneGame();
	};
	if (name == "open_settings") return [this](Ref*){
		this->_controller->openSettingsWindow();
	};
	return NodeExt::get_callback_by_description(name);
}

void BaseScene::openWindow(intrusive_ptr<BaseWindow> window)
{
	auto callback = [this](BaseWindow* window)
	{
		for(size_t i = _windows.size()-1; i != -1; --i )
		{
			if(_windows[i] == window)
			{
				_windows.erase(_windows.begin() + i);
				return;
			}
		}
		assert(0);
	};
	_windows.push_back(window);
	window->setOnExitCallback(std::bind(callback, window.ptr()));
	int zOrder = static_cast<int>(_windows.size());
	addChild(window, zOrder);
}

