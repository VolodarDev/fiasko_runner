/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "GameScene.h"
#include "scenes/BaseController.h"
#include "xml/types.h"
#include "ml/AudioEngine.h"

using namespace cocos2d;

GameScene::GameScene()
{
}

GameScene::~GameScene()
{
    AudioEngine::shared().stopMusic();
}

bool GameScene::init()
{
	if (!BaseScene::init())
		return false;
	
	_controller = std::make_shared<BaseController>(this);
	
	load(xml::scenesGame::SCENE);
	
	connectExtension(this, _layer);
	_layer->setController(_controller);
    
    std::string file = StringUtils::format("audio/music_%d.mp3", rand() % 4 + 1);
    AudioEngine::shared().playMusic(file);
    cocos2d::log("%s", file.c_str());

	return true;
}

ccMenuCallback GameScene::get_callback_by_description(const std::string & name)
{
	if (name == "") return [this](Ref*){
	};
	return BaseScene::get_callback_by_description(name);
}

void GameScene::onLoaded()
{
	BaseScene::onLoaded();
}
