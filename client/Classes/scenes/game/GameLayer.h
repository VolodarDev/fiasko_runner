/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __GameLayer_h__
#define __GameLayer_h__

#include "cocos2d.h"
#include "NodeExt.h"
#include "Character.h"
#include "Block.h"

class BaseController;
class ParalaxNode;
namespace mg
{
	class DataGameParams;
}

class GameLayer : public LayerExt
{
public:
	DECLARE_AUTOBUILDER(GameLayer);
	virtual bool init() override;
public:
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name) override;
	virtual void onLoaded() override;
	virtual void disappearance( bool force ) override;
	virtual void update(float dt) override;
	void setController(const std::shared_ptr<BaseController>& controller);
protected:
	virtual void onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
	virtual void onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
	virtual void onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
	virtual void onTouchesCancelled(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event) override;
	
	void generateBlock();
	void generateBone();
    void generateLabel();
	void updateBlocks(float dt);
	void updateCharacter(float dt);
	void updateParallax(float dt);
	void updateBones(float dt);
	void jump();
	void collisions();
	void fiasko();
	void finish();
	cocos2d::Point getBottomPoint();
	void addBone();
private:
	std::shared_ptr<BaseController> _controller;
	const mg::DataGameParams* _params;
	intrusive_ptr<Character> _character;
	std::vector<intrusive_ptr<Block>> _blocks;
	std::vector<intrusive_ptr<cocos2d::Node>> _bones;
	std::vector<ParalaxNode*> _paralax;
	float _velocity;
	float _jumpCharacterVelocity;
	int _jumpCounter;
	float _generatorTimer;
	float _generatorFrequence;
	float _generatorBoneTimer;
	float _generatorBoneFrequence;
    float _captionsTimer;
	
	int _scoreValue;
	cocos2d::ui::Text* _score;
};

#endif
