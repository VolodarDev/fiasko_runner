/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "Character.h"

using namespace cocos2d;

Character::Character()
: _move(false)
, _fiasko(false)
{
}

Character::~Character()
{
}

bool Character::init()
{
	if (!NodeExt_::init())
		return false;
	
	return true;
}

ccMenuCallback Character::get_callback_by_description(const std::string & name)
{
	if (name == "") return [this](Ref*){
	};
	return NodeExt_::get_callback_by_description(name);
}

void Character::onLoaded()
{
	NodeExt_::onLoaded();
	
	connectExtension(this, "skin", _skin);
    move();
}

void Character::update(float dt)
{
}

void Character::jump()
{
    if(_fiasko)
        return;
    _move = false;
    runEvent("jump");
}

void Character::move()
{
    if(_fiasko)
        return;
    if(!_move)
    {
        _move = true;
        runEvent("move");
    }
}

void Character::fiasko()
{
    _fiasko = true;
	runEvent("fiasko");
}
