/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __Character_h__
#define __Character_h__

#include "cocos2d.h"
#include "NodeExt.h"

class Character : public NodeExt_
{
public:
	DECLARE_AUTOBUILDER(Character);
	virtual bool init() override;
public:
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name) override;
	virtual void onLoaded() override;
	virtual void update(float dt) override;

	void jump();
	void move();
	void fiasko();
protected:

private:
	SpritePointer _skin;
    bool _move;
    bool _fiasko;
	cocos2d::DrawNode* _draw;
};

#endif
