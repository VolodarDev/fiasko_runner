/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "ParalaxNode.h"

using namespace cocos2d;

ParalaxNode::ParalaxNode():
_velocity(0)
{
}

ParalaxNode::~ParalaxNode()
{
}

bool ParalaxNode::init()
{
	if (!NodeExt_::init())
		return false;

	return true;
}

void ParalaxNode::update(float dt)
{
	if(getChildren().empty())
		return;
	
	auto node = getChildren().front();
	auto right = Point(node->getContentSize());
	auto screen = node->convertToWorldSpace(right);
	if(screen.x < 0)
	{
		auto lastNode = getChildren().back();
		auto x = lastNode->getPositionX() + lastNode->getContentSize().width;
		node->retain();
		node->setPositionX(x);
		node->removeFromParent();
		addChild(node);
		node->retain();
	}

	auto z = getLocalZOrder();
	for(auto node : getChildren())
	{
		auto x = node->getPositionX();
		x -= (_velocity / (-z / 10 + 1)) * dt;
		node->setPositionX(x);
	}
}
