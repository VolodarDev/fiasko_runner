/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __GameScene_h__
#define __GameScene_h__

#include "cocos2d.h"
#include "scenes/BaseScene.h"
#include "GameLayer.h"

class GameScene : public BaseScene
{
public:
	DECLARE_AUTOBUILDER(GameScene);
	virtual bool init() override;
public:
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name) override;
	virtual void onLoaded() override;
protected:

private:
	intrusive_ptr<GameLayer> _layer;
};

#endif
