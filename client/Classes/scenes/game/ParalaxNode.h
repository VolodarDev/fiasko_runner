/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __ParalaxNode_h__
#define __ParalaxNode_h__

#include "cocos2d.h"
#include "ml/NodeExt.h"

class ParalaxNode : public NodeExt_
{
public:
	DECLARE_AUTOBUILDER(ParalaxNode);
	virtual bool init() override;
	virtual void update(float dt) override;
private:
	CC_SYNTHESIZE(float, _velocity, Velocity);
};

#endif
