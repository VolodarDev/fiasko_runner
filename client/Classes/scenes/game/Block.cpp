/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "Block.h"

using namespace cocos2d;

Block::Block()
{
}

Block::~Block()
{
}

bool Block::init()
{
	if (!NodeExt_::init())
		return false;
	
	_draw = DrawNode::create();
	addChild(_draw);

	return true;
}

ccMenuCallback Block::get_callback_by_description(const std::string & name)
{
	if (name == "") return [this](Ref*){
	};
	return NodeExt_::get_callback_by_description(name);
}

void Block::onLoaded()
{
	NodeExt_::onLoaded();
	connectExtension(this, "skin", _skin);
}

void Block::generate()
{
	std::vector<std::string> images = {
		"game/blocks/snow1.png",
		"game/blocks/snow2.png",
		"game/blocks/snow3.png",
		"game/blocks/suk1.png",
		"game/blocks/suk2.png",
		"game/blocks/suk3.png",
	};
	size_t index = rand() % images.size();
	setTexture(_skin, images[index]);
    setSize(_skin->getContentSize() * 0.75f);
}

void Block::setSize(const cocos2d::Size& size)
{
	setContentSize(size);
	_skin->setContentSize(size);
	_skin->setPosition(Point(size / 2));
	
//	_draw->clear();
//	auto s = Point(getContentSize());
//	auto a = getAnchorPoint();
//	auto l = Point(s.x * a.x, s.y * a.y);
//	auto r = l + s;
//	_draw->drawRect(l, r, Color4F::WHITE);
}

void Block::fiasko()
{
	runEvent("fiasko");
}
