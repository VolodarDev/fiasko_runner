/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "GameLayer.h"
#include "scenes/BaseController.h"
#include "xml/loader.h"
#include "mg/DataStorage.h"
#include "ParalaxNode.h"
#include "support/UserData.h"
#include "ml/AudioEngine.h"
#include "services/admob/admob.h"

using namespace cocos2d;

const float GROUND_Y = 320;
const float BLOCKS_Y = 290;

GameLayer::GameLayer()
: _velocity(500.f)
, _jumpCharacterVelocity(0.f)
, _jumpCounter(0)
, _generatorTimer(0.f)
, _generatorFrequence(2.f)
, _scoreValue(0)
, _generatorBoneFrequence(2.f)
, _generatorBoneTimer(2.f)
, _captionsTimer(5)
{
}

GameLayer::~GameLayer()
{
	if(!UserData::shared().get<bool>("noads"))
    {
        admob::showInterstitial();
    }
}

bool GameLayer::init()
{
	if (!LayerExt::init())
		return false;
	
	auto variant = rand() % 2 + 1;
	xmlLoader::macros::set({
		"variant_1", toStr(variant == 1),
		"variant_2", toStr(variant == 2),
	});
	
	auto touchListener = EventListenerTouchAllAtOnce::create();
	touchListener->onTouchesBegan = CC_CALLBACK_2(GameLayer::onTouchesBegan, this);
	touchListener->onTouchesMoved = CC_CALLBACK_2(GameLayer::onTouchesMoved, this);
	touchListener->onTouchesEnded = CC_CALLBACK_2(GameLayer::onTouchesEnded, this);
	touchListener->onTouchesCancelled = CC_CALLBACK_2(GameLayer::onTouchesCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
	scheduleUpdate();
	_params = mg::DataStorage::shared().get<mg::DataGameParams>("default");
	_velocity = _params->start_velocity;
	_generatorFrequence = _params->start_generator_frequence;
	
	return true;
}

ccMenuCallback GameLayer::get_callback_by_description(const std::string & name)
{
	if (name == "") return [this](Ref*){
	};
	return LayerExt::get_callback_by_description(name);
}

void GameLayer::onLoaded()
{
	LayerExt::onLoaded();
	
	_score = getNodeByPath<ui::Text>(this, "score/value");
	_paralax.clear();
	_paralax.push_back(getNodeByPath<::ParalaxNode>(this, "bg/paralax_1"));
	_paralax.push_back(getNodeByPath<::ParalaxNode>(this, "bg/paralax_2"));
	_paralax.push_back(getNodeByPath<::ParalaxNode>(this, "bg/paralax_3"));
	_paralax.push_back(getNodeByPath<::ParalaxNode>(this, "bg/paralax_4"));
	_paralax.push_back(getNodeByPath<::ParalaxNode>(this, "bg/paralax_5"));
	_paralax.push_back(getNodeByPath<::ParalaxNode>(this, "bg/paralax_6"));
	_paralax.push_back(getNodeByPath<::ParalaxNode>(this, "bg/paralax_7"));

    auto max = UserData::shared().get<int>("score_max");
    getNodeByPath<ui::Text>(this, "best/value")->setString(toStr(max));
}

void GameLayer::disappearance( bool force )
{
	_controller->runSceneTitle();
}

void GameLayer::update(float dt)
{
	dt = 1.f / 60.f;
	_velocity += _params->velocity_accelerate * dt;
	_generatorFrequence += _params->generate_frequence_accelerate * dt;
	
	updateBlocks(dt);
	updateCharacter(dt);
	updateBones(dt);
	updateParallax(dt);
	collisions();
	
	if((_generatorTimer -= dt) <= 0)
	{
		_generatorTimer = _generatorFrequence;
		generateBlock();
	}
    if((_generatorBoneTimer -= dt) <= 0)
    {
        _generatorBoneTimer = _generatorBoneFrequence;
        generateBone();
    }
    if((_captionsTimer -= dt) <= 0)
    {
        _captionsTimer = 5;
        generateLabel();
    }
}

void GameLayer::updateBlocks(float dt)
{
	for(auto block : _blocks)
	{
		auto pos = block->getPosition();
		pos.x -= _velocity * dt;
		block->setPosition(pos);
	}
	auto remove = std::remove_if(_blocks.begin(), _blocks.end(), [](intrusive_ptr<Block> block){
		if(block->getPositionX() < -500)
		{
			block->removeFromParent();
			return true;
		}
		return false;
	});
	_blocks.erase(remove, _blocks.end());
}

void GameLayer::updateCharacter(float dt)
{
	auto pos = _character->getPosition();
	auto bottom = getBottomPoint();
	if(pos.y > bottom.y || _jumpCharacterVelocity > 0)
	{
		pos.y += _jumpCharacterVelocity * dt;
		_jumpCharacterVelocity -= _params->jump_dumping * dt;
		if(pos.y < bottom.y)
		{
			pos.y = bottom.y;
			_jumpCounter = 0;
            _character->move();
		}
	}
	_character->setPosition(pos);
}

void GameLayer::updateParallax(float dt)
{
	for(auto node : _paralax)
	{
		node->setVelocity(_velocity);
		node->update(dt);
	}
}

void GameLayer::updateBones(float dt)
{
	for(auto bone : _bones)
	{
		auto pos = bone->getPosition();
		pos.x -= _velocity * dt;
		bone->setPosition(pos);
	}
	
	auto characterPosition = _character->getPosition();
	characterPosition += Point(_character->getContentSize() / 2);
	auto remove = std::remove_if(_bones.begin(), _bones.end(), [this, characterPosition](intrusive_ptr<Node> bone){
		if(bone->getPosition().getDistance(characterPosition) < 200)
		{
			addBone();
			bone->removeFromParent();
			return true;
		}
		return false;
	});
	_bones.erase(remove, _bones.end());

	remove = std::remove_if(_bones.begin(), _bones.end(), [](intrusive_ptr<Node> bone){
		if(bone->getPositionX() < -500)
		{
			bone->removeFromParent();
			return true;
		}
		return false;
	});
	_bones.erase(remove, _bones.end());
}

cocos2d::Point GameLayer::getBottomPoint()
{
	auto P = _character->getPosition();
	auto S = Point(_character->getContentSize());
	for(auto block : _blocks)
	{
		auto p = block->getPosition();
		auto s = Point(block->getContentSize());
		if((P.x + S.x) > p.x && P.x < (p.x+s.x) && P.y > (p.y+s.y))
		{
			return Point(P.x, p.y + s.y + 1);
		}
	}
	return Point(P.x, GROUND_Y);
}

void GameLayer::generateBlock()
{
	auto block = xml::scenesGame::load_block<Block>();
	block->setPosition(2000, BLOCKS_Y);
	_blocks.push_back(block);
	addChild(block);
	block->generate();
}

void GameLayer::generateBone()
{
	auto bone = xml::scenesGame::load_bone();
	auto y = CCRANDOM_0_1() * 500 + GROUND_Y;
	bone->setPosition(2000, y);
	addChild(bone);
	_bones.push_back(bone);
}

void GameLayer::generateLabel()
{
    runEvent("show_caption_" + toStr(rand() % 2 + 1));
}

void GameLayer::collisions()
{
	auto P = _character->getPosition();
	auto S = Point(_character->getContentSize());
	for(auto block : _blocks)
	{
		auto p = block->getPosition();
		auto s = Point(block->getContentSize());
		if((P.x + S.x) > p.x && P.x < (p.x+s.x))
		{
			const float border = 15;
			if(P.y >= p.y + s.y - border && P.y < p.y + s.y + border)
			{
				P.y = p.y + s.y + border + 1;
				_character->setPosition(P);
				_jumpCounter = 0;
				_jumpCharacterVelocity = 0;
                _character->move();
				if(rand() % 20 == 0)
				{
					fiasko();
					block->fiasko();
				}
			}
			else if(P.y < p.y + s.y - border)
			{
				fiasko();
				block->fiasko();
			}
		}
	}
}

void GameLayer::setController(const std::shared_ptr<BaseController>& controller)
{
	_controller = controller;
	
	_character = xml::scenesGame::load_character<Character>();
	addChild(_character, 1);
	_character->setPosition(300, GROUND_Y);
}

void GameLayer::jump()
{
	if(_jumpCounter < _params->count_jumps)
	{
		++_jumpCounter;
		_jumpCharacterVelocity = _params->jump_velocity;
		_character->jump();
	}
}

void GameLayer::fiasko()
{
	unscheduleUpdate();
	_character->fiasko();
	runAction(Sequence::create(
							   DelayTime::create(1),
							   CallFunc::create([this](){finish();}),
							   nullptr
			  ));
    AudioEngine::shared().stopMusic();
    runEvent("fiasko");
}

void GameLayer::finish()
{
	unscheduleUpdate();
	_controller->openFiaskoWindow();

    runAction(Sequence::create(DelayTime::create(10),
                               CallFunc::create([this](){_controller->runSceneTitle();}),
                               nullptr));
}

void GameLayer::onTouchesBegan(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event)
{
	for(auto touch : touches)
	{
		jump();
		CC_UNUSED_PARAM(touch);
	}
}

void GameLayer::onTouchesMoved(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event)
{
	for(auto touch : touches)
	{
		CC_UNUSED_PARAM(touch);
	}
}

void GameLayer::onTouchesEnded(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event)
{
	for(auto touch : touches)
	{
		CC_UNUSED_PARAM(touch);
	}
}

void GameLayer::onTouchesCancelled(const std::vector<cocos2d::Touch*>& touches, cocos2d::Event* event)
{
	for(auto touch : touches)
	{
		CC_UNUSED_PARAM(touch);
	}
}

void GameLayer::addBone()
{
	_scoreValue++;
	_score->setString(toStr(_scoreValue));
	
	auto max = UserData::shared().get<int>("score_max");
	auto sum = UserData::shared().get<int>("score_sum");
	max = std::max(max, _scoreValue);
    
    getNodeByPath<ui::Text>(this, "best/value")->setString(toStr(max));
    
	sum++;
	UserData::shared().write("score_max", max);
	UserData::shared().write("score_sum", sum);
	UserData::shared().save();
    
    runEvent("bone");
}
