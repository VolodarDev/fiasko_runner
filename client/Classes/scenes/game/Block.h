/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __Block_h__
#define __Block_h__

#include "cocos2d.h"
#include "ml/NodeExt.h"

class Block : public NodeExt_
{
public:
	DECLARE_AUTOBUILDER(Block);
	virtual bool init() override;
public:
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name) override;
	virtual void onLoaded() override;
	
	void generate();
	void setSize(const cocos2d::Size& size);
	
	void fiasko();
protected:

private:
	SpritePointer _skin;
	cocos2d::DrawNode* _draw;
};

#endif
