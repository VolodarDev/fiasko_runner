/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __MainMenu_h__
#define __MainMenu_h__

#include "cocos2d.h"
#include "NodeExt.h"

class BaseController;

class MainMenu : public NodeExt_
{
public:
	DECLARE_AUTOBUILDER( MainMenu );
	virtual bool init() override;
public:
	void set(const std::shared_ptr<BaseController>& controller);
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name) override;
	virtual void onLoaded() override;
protected:

private:
	std::shared_ptr<BaseController> _controller;
	intrusive_ptr<cocos2d::ui::Button> _buttonCharacter;
	intrusive_ptr<cocos2d::ui::Button> _buttonInformation;
	intrusive_ptr<cocos2d::ui::Button> _buttonLogbook;
	intrusive_ptr<cocos2d::ui::Button> _buttonSettings;
};

#endif
