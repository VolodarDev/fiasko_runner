#include "LoaderLayer.h"

using namespace cocos2d;

LoaderLayer::LoaderLayer()
{
}

LoaderLayer::~LoaderLayer()
{
}

bool LoaderLayer::init()
{
	if( !LayerExt::init() )
		return false;
	
	setDispatchKeyBack(false);

	return true;
}
