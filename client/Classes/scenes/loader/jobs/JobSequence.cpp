#include "JobSequence.h"
#include "Generics.h"
#include "Factory.h"
#include "IntrusivePtr.h"
#include "Job.h"
#include <algorithm>
#include <string>
#include <vector>

namespace mg
{
	const std::string JobSequence::__type__ = "JobSequence";
	REGISTRATION_OBJECT( JobSequence );
	
	JobSequence::JobSequence()
	{
		
	}
	
	JobSequence::~JobSequence()
	{
	}
	
	Job* JobSequence::getCurrentJob()
	{
		return _executedJob;
	}
	
	size_t JobSequence::count()const
	{
		return _jobs.size();
	}

	int JobSequence::getCurrentProgress() const
	{
		int sum = 0;
		for( auto& job : _jobs)
		{
			sum += job->getCurrentProgress();
		}
		return sum;
	}
	
	int JobSequence::getTotalProgress() const
	{
		int sum = 0;
		for( auto& job : _jobs)
		{
			sum += job->getTotalProgress();
		}
		return sum;
	}
	
	void JobSequence::execute()
	{
		for( auto& job : _jobs)
		{
			job->execute();
		}
	}
	
	void JobSequence::next()
	{
		_currentProgress = getCurrentProgress();
		auto job = _currentProgress < static_cast<int>(_jobs.size()) ? _jobs[_currentProgress] : nullptr;
		if(job)
		{
			_executedJob = job;
			job->execute();
			_currentProgress = getCurrentProgress();
		}
	}
	
	void JobSequence::add(Job* job)
	{
		_jobs.push_back(job);
	}
	
	std::string JobSequence::get_type() const
	{
		return JobSequence::__type__;
	}
	
	bool JobSequence::operator ==(const JobSequence& rhs) const
	{
		bool result = true;
		result = result && _jobs == rhs._jobs;
		return result;
	}
	
	bool JobSequence::operator !=(const JobSequence& rhs) const
	{
		return !(*this == rhs);
	}
	
}
