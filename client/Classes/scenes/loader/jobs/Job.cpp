#include "Job.h"
#include "Generics.h"
#include "Factory.h"
#include "mg/SerializedObject.h"
#include <algorithm>
#include <string>

namespace mg
{
	const std::string Job::__type__ = "Job";
	
	Job::Job()
	: _currentProgress(0)
	, _totalProgress(1)
	{
		
	}
	
	Job::~Job()
	{
	}
	
	int Job::getCurrentProgress() const
	{
		return _currentProgress;
	}
	
	int Job::getTotalProgress() const
	{
		return _totalProgress;
	}
	
	std::string Job::get_type() const
	{
		return Job::__type__;
	}
	
	void Job::serialize(pugi::xml_node json) const
	{
		SerializedObject::serialize(json);
		assert(0);
		//if(_currentProgress != 0)
		//{
		//	::set(json,"_currentProgress",_currentProgress);
		//}
		//if(_totalProgress != 1)
		//{
		//	::set(json,"_totalProgress",_totalProgress);
		//}
	}
	
	void Job::deserialize(const pugi::xml_node& json)
	{
		SerializedObject::deserialize(json);
		assert(0);
		//if(json.isMember("_currentProgress"))
		//{
		//	_currentProgress = ::get<int>( json["_currentProgress"] );
		//}
		//else
		//{
		//	_currentProgress = 0;
		//}
		//if(json.isMember("_totalProgress"))
		//{
		//	_totalProgress = ::get<int>( json["_totalProgress"] );
		//}
		//else
		//{
		//	_totalProgress = 1;
		//}
	}
	
	bool Job::operator ==(const Job& rhs) const
	{
		bool result = true;
		result = result && _currentProgress == rhs._currentProgress;
		result = result && _totalProgress == rhs._totalProgress;
		return result;
	}
	
	bool Job::operator !=(const Job& rhs) const
	{
		return !(*this == rhs);
	}
	
}
