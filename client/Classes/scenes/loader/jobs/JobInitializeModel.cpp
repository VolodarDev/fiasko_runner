/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "JobInitializeModel.h"
#include "model/ClientModel.h"

using namespace cocos2d;

JobInitializeModel::JobInitializeModel()
{
}

JobInitializeModel::~JobInitializeModel()
{
}

void JobInitializeModel::execute()
{
	ClientModel::shared().initialize();
	_currentProgress = 1;
}
