/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __JobInitializeDataStorage_h__
#define __JobInitializeDataStorage_h__

#include "cocos2d.h"
#include "Job.h"
#include "ml/IntrusivePtr.h"

class JobInitializeDataStorage : public mg::Job
{
public:
	JobInitializeDataStorage();
	virtual ~JobInitializeDataStorage();
	virtual void execute() override;
private:
};

#endif
