/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "JobInitializeDataStorage.h"
#include "support/UserData.h"
#include "support/FileSystemUtils.h"
#include "mg/DataStorage.h"

using namespace cocos2d;

JobInitializeDataStorage::JobInitializeDataStorage()
{
}

JobInitializeDataStorage::~JobInitializeDataStorage()
{
}

void JobInitializeDataStorage::execute()
{
	auto buffer = FileSystemUtils::getStringFromFile("data/data.xml");
	mg::DataStorage::shared().initialize(buffer);
	
	_currentProgress = 1;
}
