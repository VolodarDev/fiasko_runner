/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "JobCallback.h"

using namespace cocos2d;

JobCallback::JobCallback(const std::function<void()>& callback)
	: _callback(callback)
{
}

void JobCallback::execute()
{
	_callback();
	_currentProgress = 1;
}
