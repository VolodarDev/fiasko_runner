/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __JobCallback_h__
#define __JobCallback_h__

#include "cocos2d.h"
#include "Job.h"

class JobCallback : public mg::Job
{
public:
	JobCallback(const std::function<void()>& callback);
	virtual void execute() override;
private:
	std::function<void()> _callback;
};

#endif
