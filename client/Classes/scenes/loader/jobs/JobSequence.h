#ifndef __JobSequence_h__
#define __JobSequence_h__

#include "IntrusivePtr.h"
#include "Job.h"
#include <string>
#include <vector>

namespace mg
{
	class Job;
	
	class JobSequence : public Job
	{
	public:
		JobSequence();
		virtual ~JobSequence();

		Job* getCurrentJob();
		size_t count()const;
		
		virtual int getCurrentProgress() const override;
		virtual int getTotalProgress() const override;
		virtual void execute() override;
		virtual void next();
		virtual void add(Job* job);
		virtual std::string get_type() const override;
		bool operator ==(const JobSequence& rhs) const;
		bool operator !=(const JobSequence& rhs) const;
		
	public:
		IntrusivePtr<Job> _executedJob;
		std::vector<IntrusivePtr<Job>> _jobs;
		static const std::string __type__;
	};
	
}//namespace mg

#endif //#ifndef __JobSequence_h__
