/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "JobInitializeLanguage.h"
#include "ml/Localization.h"
#include "mg/DataStorage.h"
#include "locale/initialize.h"

using namespace cocos2d;

JobInitializeLanguage::JobInitializeLanguage()
{
}

JobInitializeLanguage::~JobInitializeLanguage()
{
}

void JobInitializeLanguage::execute()
{
	Localization::shared().set("ru");
	initializeLocalization();
	initializeLocalesInData();
	_currentProgress = 1;
}

void JobInitializeLanguage::initializeLocalization()
{
	locale::initialize();
}

void JobInitializeLanguage::initializeLocalesInData()
{
	auto& localization = Localization::shared();
	auto& storage = const_cast<mg::DataStorage&>(mg::DataStorage::shared());
	for(auto& pair : storage.locales)
	{
		pair.second.name = pair.first;
		pair.second.value = localization.locale(pair.second.name);
	}
}
