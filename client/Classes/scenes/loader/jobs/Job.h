#ifndef __Job_h__
#define __Job_h__

#include "mg/SerializedObject.h"
#include <string>

namespace mg
{
	
	class Job : public SerializedObject
	{
	public:
		Job();
		virtual ~Job();
		
		virtual int getCurrentProgress() const;
		virtual int getTotalProgress() const;
		virtual void execute() {}
		virtual std::string get_type() const override;
		virtual void serialize(pugi::xml_node json) const override;
		virtual void deserialize(const pugi::xml_node& json) override;
		bool operator ==(const Job& rhs) const;
		bool operator !=(const Job& rhs) const;
		
	public:
		int _currentProgress;
		int _totalProgress;
		static const std::string __type__;
	};
	
}//namespace mg

#endif //#ifndef __Job_h__
