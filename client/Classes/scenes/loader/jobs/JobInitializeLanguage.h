/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __JobInitializeLanguage_h__
#define __JobInitializeLanguage_h__

#include "cocos2d.h"
#include "Job.h"
#include "ml/IntrusivePtr.h"

class JobInitializeLanguage : public mg::Job
{
public:
	JobInitializeLanguage();
	virtual ~JobInitializeLanguage();
	virtual void execute() override;
private:
	void initializeLocalization();
	void initializeLocalesInData();
};

#endif
