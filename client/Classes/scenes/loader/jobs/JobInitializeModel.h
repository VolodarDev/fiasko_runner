/*
* Copyright 2014-2015 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __JobInitializeModel_h__
#define __JobInitializeModel_h__

#include "cocos2d.h"
#include "Job.h"
#include "ml/IntrusivePtr.h"

class JobInitializeModel : public mg::Job
{
public:
	JobInitializeModel();
	virtual ~JobInitializeModel();
	virtual void execute() override;
private:
};

#endif
