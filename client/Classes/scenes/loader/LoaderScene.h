#ifndef __LoaderScene_h__
#define __LoaderScene_h__

#include "cocos2d.h"
#include "scenes/BaseScene.h"
#include "LoaderLayer.h"
#include "scenes/loader/jobs/JobSequence.h"

class BattleController;

class LoaderScene : public BaseScene
{
public:
	DECLARE_AUTOBUILDER( LoaderScene );
	static LoaderScene::Pointer createStartScene();
	bool init(SceneId sceneId);
public:
	bool hasJobs()const;
	void runScene();
	
	virtual void update(float dt)override;
	virtual void onLoaded()override;
protected:
	void loadResources();
	void createTitleScene();
private:
	std::shared_ptr<BattleController> _battleController;
	intrusive_ptr<LoaderLayer> _loaderLayer;
	mg::JobSequence _jobs;
	SceneId _nextSceneId;
};

#endif
