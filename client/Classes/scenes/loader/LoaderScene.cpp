#include "LoaderScene.h"
#include "model/ClientModel.h"
#include "scenes/BaseController.h"
#include "scenes/title/TitleScene.h"
#include "scenes/game/GameScene.h"
#include "scenes/loader/jobs/JobCallback.h"
#include "scenes/loader/jobs/JobInitializeDataStorage.h"
#include "scenes/loader/jobs/JobInitializeLanguage.h"
#include "scenes/loader/jobs/JobInitializeModel.h"
#include "support/UserData.h"
#include "xml/types.h"
#include "AppDelegate.h"


using namespace cocos2d;


LoaderScene::LoaderScene()
{
}

LoaderScene::~LoaderScene()
{
}

LoaderScene::Pointer LoaderScene::createStartScene()
{
    auto scene = create(UserData::shared().getSessionNumber() > 1 ? SceneId::title : SceneId::game);
	return scene;
}

bool LoaderScene::init(SceneId sceneId)
{
	if( !BaseScene::init() )
		return false;
	
	if(ClientModel::shared().get_user()==nullptr)
	{
		_jobs.add(make_intrusive<JobInitializeDataStorage>());
		_jobs.add(make_intrusive<JobInitializeLanguage>());
		_jobs.add(make_intrusive<JobInitializeModel>());
	}

	_nextSceneId = sceneId;
	loadResources();
	switch (sceneId)
	{
		case SceneId::title:
			createTitleScene();
			break;
		case SceneId::game:
			break;
		default:
			assert(0);
	}

	load(xml::scenesLoader::SCENE);

	scheduleUpdate();
	return true;
}

bool LoaderScene::hasJobs()const
{
	return _jobs.count() > 0;
}

void LoaderScene::loadResources()
{
	std::string path;
	switch (_nextSceneId)
	{
		case SceneId::title:
			path = xml::scenesTitle::SCENE;
			break;
		case SceneId::game:
			path = xml::scenesGame::SCENE;
			break;
		default:
			assert(0);
	}
	if (path.empty())
		return;

	auto doc = xmlLoader::loadDoc(path);
	auto resourcesNode = doc->root().first_child().child("preload_resources");
	for (auto resource : resourcesNode)
	{
		std::string type = resource.name();
		if (type == "plist") 
		{
			std::string path = resource.attribute("path").as_string();
			_jobs.add(make_intrusive<JobCallback>([path]() {
				SpriteFrameCache::getInstance()->addSpriteFramesWithFile(path);
			}));
		}
	}
}

void LoaderScene::update(float dt)
{
	if (_jobs.getCurrentProgress() == _jobs.getTotalProgress())
	{
		runScene();
	}
	else
	{
		auto currentJob = _jobs.getCurrentJob();
		if (!currentJob || currentJob->getCurrentProgress() == currentJob->getTotalProgress())
		{
			_jobs.next();
		}
	}
}

void LoaderScene::onLoaded()
{
	BaseScene::onLoaded();
}

void LoaderScene::createTitleScene()
{
}

void LoaderScene::runScene()
{
	BaseScene::Pointer scene;
	switch (_nextSceneId)
	{
		case SceneId::title:
			scene = TitleScene::create();
			break;
		case SceneId::game:
			scene = GameScene::create();
			break;
		default:
			assert(0);
	}
	Director::getInstance()->replaceScene(scene);
}
