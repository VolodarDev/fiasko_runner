#ifndef __LoaderLayer_h__
#define __LoaderLayer_h__

#include "cocos2d.h"
#include "ml/NodeExt.h"

class LoaderLayer : public LayerExt
{
public:
	DECLARE_AUTOBUILDER( LoaderLayer );
	virtual bool init()override;
public:
protected:
private:
};

#endif
