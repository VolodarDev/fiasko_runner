/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "MainMenu.h"
#include "scenes/BaseController.h"

using namespace cocos2d;

MainMenu::MainMenu()
{
}

MainMenu::~MainMenu()
{
}

bool MainMenu::init()
{
	if (!NodeExt_::init())
		return false;

	return true;
}

void MainMenu::set(const std::shared_ptr<BaseController>& controller)
{
	_controller = controller;
}

ccMenuCallback MainMenu::get_callback_by_description(const std::string & name)
{
	if (name == "character")return [this](Ref*){
	};
	if (name == "information")return [this](Ref*){
	};
	if (name == "logbook")return [this](Ref*){
	};
	if (name == "settings")return [this](Ref*){
		_controller->openSettingsWindow();
	};
	return NodeExt_::get_callback_by_description(name);
}

void MainMenu::onLoaded()
{
	NodeExt_::onLoaded();
	
	_buttonCharacter = getNodeByPath<ui::Button>(this, "button_character");
	_buttonInformation = getNodeByPath<ui::Button>(this, "button_information");
	_buttonLogbook = getNodeByPath<ui::Button>(this, "button_logbook");
	_buttonSettings = getNodeByPath<ui::Button>(this, "button_settings");
}
