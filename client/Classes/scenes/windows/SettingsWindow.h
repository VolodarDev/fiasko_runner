/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __SettingsWindow_h__
#define __SettingsWindow_h__

#include "cocos2d.h"
#include "scenes/BaseWindow.h"

class BaseController;

class SettingsWindow : public BaseWindow
{
public:
	DECLARE_AUTOBUILDER( SettingsWindow );
	virtual bool init(const std::shared_ptr<BaseController>& controller) override;
public:
	virtual cocos2d::ccMenuCallback get_callback_by_description(const std::string & name) override;
	virtual void onLoaded() override;
protected:

private:
	std::shared_ptr<BaseController> _controller;
};

#endif
