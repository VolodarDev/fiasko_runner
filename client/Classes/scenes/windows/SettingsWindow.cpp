/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#include "SettingsWindow.h"
#include "support/Settings.h"
#include "scenes/BaseController.h"
#include "mg/DataStorage.h"
#include "model/ClientModel.h"

using namespace cocos2d;

SettingsWindow::SettingsWindow()
{
}

SettingsWindow::~SettingsWindow()
{
	ClientModel::shared().save();
}

bool SettingsWindow::init(const std::shared_ptr<BaseController>& controller)
{
	if (!BaseWindow::init(controller))
		return false;
	_controller = controller;

	return true;
}

ccMenuCallback SettingsWindow::get_callback_by_description(const std::string & name)
{
	return BaseWindow::get_callback_by_description(name);
}

void SettingsWindow::onLoaded()
{
	BaseWindow::onLoaded();
	auto params = const_cast<mg::DataGameParams*>(mg::DataStorage::shared().get<mg::DataGameParams>("default"));
	
	auto set = [this](float& parameter, const std::string& name){
		auto add = getNodeByPath<ui::Button>(this, name + "/+");
		auto sub = getNodeByPath<ui::Button>(this, name + "/-");
		auto value = getNodeByPath<ui::Text>(this, name + "/value");
		value->setString(toStr(parameter));
		add->addClickEventListener([this, value, &parameter](Ref*){
			auto v = strTo<float>(value->getString());
			auto add = v * 0.02f;
			add = std::max(0.01f, add);
			v += add;
			value->setString(toStr(v));
			parameter = v;
		});
		sub->addClickEventListener([this, value, &parameter](Ref*){
			auto v = strTo<float>(value->getString());
			auto sub = v * 0.02f;
			sub = std::max(0.01f, sub);
			v -= sub;
			value->setString(toStr(v));
			parameter = v;
		});
	};
	set(params->jump_velocity, "jump_velocity");
	set(params->jump_dumping, "jump_dumping");
	set(params->start_velocity, "start_velocity");
	set(params->velocity_accelerate, "velocity_accelerate");
	set(params->start_generator_frequence, "start_generator_frequence");
	set(params->generate_frequence_accelerate, "generate_frequence_accelerate");
}
