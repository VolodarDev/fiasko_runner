#include "scenes/BaseWindow.h"

using namespace cocos2d;

BaseWindow::BaseWindow()
	: _controller(nullptr)
{
}

BaseWindow::~BaseWindow()
{
}

bool BaseWindow::init(const std::shared_ptr<BaseController>& controller)
{
	if( !LayerExt::init() )
		return false;
	_controller = controller;
	return true;
}

void BaseWindow::close()
{
	disappearance(true);
}

cocos2d::ccMenuCallback BaseWindow::get_callback_by_description( const std::string & name )
{
	if( name == "" )
	{
	}
	return LayerExt::get_callback_by_description(name);
}

bool BaseWindow::loadXmlEntity( const std::string & tag, const pugi::xml_node & xmlnode )
{
#if DEV==1
	if(tag == "preview")
		previewSceneTexture = xmlnode.text().as_string();
	else
#endif
		return LayerExt::loadXmlEntity( tag, xmlnode );
	return true;
}

void BaseWindow::onLoaded()
{
	LayerExt::onLoaded();
}

Size BaseWindow::getLayoutInnerSize(cocos2d::ui::Layout* layout)
{
	Size size;
	layout->forceDoLayout();
	float min_height = 0.f;
	float max_height = 0.f;
	for(auto child : layout->getChildren())
	{
		auto r = child->getContentSize().width * (1.f - child->getAnchorPoint().x) + child->getPositionX();
		auto b = child->getContentSize().height * (1.f - child->getAnchorPoint().y) + child->getPositionY();
		min_height = std::min(min_height, b);
		max_height = std::max(max_height, b);
		size.width = std::max(size.width, r);
	}
	size.height = std::max(size.height, max_height - min_height);
	return size;
}

void BaseWindow::buildScrollVTable(ui::ScrollView* scroll, const std::vector<intrusive_ptr<Node>>& panels, int cols)
{
	auto createLayout = []()
	{
		auto layout = ui::Layout::create();
		layout->setLayoutType(ui::Layout::Type::HORIZONTAL);
		return layout;
	};
	auto height = 0;
	ui::Layout* layout = nullptr;
	auto counter = 0;
	for (auto panel : panels)
	{
		if(counter == 0)
		{
			layout = createLayout();
			layout->setContentSize(Size(1080,panel->getContentSize().height));
			scroll->addChild(layout);
			height += panel->getContentSize().height;
		}
		if(panel->getParent() != nullptr)
		{
			panel->removeFromParent();
		}
		layout->addChild(panel);
		if(++counter == cols)
		{
			counter = 0;
		}
	}
	auto size = scroll->getInnerContainerSize();
	size.height = height;
	scroll->setInnerContainerSize(size);
};

void BaseWindow::buildScrollVTable(cocos2d::ui::ScrollView* scroll, const std::vector<intrusive_ptr<cocos2d::Node>>& lines, const std::vector<intrusive_ptr<cocos2d::Node>>& panels, int cols)
{
	auto createLayout = []()
	{
		auto layout = ui::Layout::create();
		layout->setLayoutType(ui::Layout::Type::HORIZONTAL);
		return layout;
	};
	auto height = 0;
	for(auto panel : lines)
	{
		if(panel->getParent() != nullptr)
		{
			panel->removeFromParent();
		}
		scroll->addChild(panel);
		height += panel->getContentSize().height;
	}
	ui::Layout* layout = nullptr;
	auto counter = 0;
	for(auto panel : panels)
	{
		if(counter == 0)
		{
			layout = createLayout();
			layout->setContentSize(Size(1080,panel->getContentSize().height));
			scroll->addChild(layout);
			height += panel->getContentSize().height;
		}
		if(panel->getParent() != nullptr)
		{
			panel->removeFromParent();
		}
		layout->addChild(panel);
		if(++counter == cols)
		{
			counter = 0;
		}
	}
	auto size = scroll->getInnerContainerSize();
	size.height = height;
	scroll->setInnerContainerSize(size);
}

void BaseWindow::buildScroll(cocos2d::ui::ScrollView* scroll, const std::vector<intrusive_ptr<cocos2d::Node>>& panels, cocos2d::ui::Layout::Type type)
{
	Size innerSize;
	for (auto panel : panels)
	{
		if(panel->getParent() != scroll)
		{
			panel->removeFromParent();
			scroll->addChild(panel);
		}
		if(type == ui::Layout::Type::HORIZONTAL)
			innerSize.width += panel->getContentSize().width;
		else if(type == ui::Layout::Type::VERTICAL)
			innerSize.height += panel->getContentSize().height;
		else
			assert(0);
	}
	auto size = scroll->getInnerContainerSize();
	if(type == ui::Layout::Type::HORIZONTAL)
		innerSize.height = size.height;
	else if(type == ui::Layout::Type::VERTICAL)
		innerSize.width += size.width;
	else
		assert(0);
	scroll->setInnerContainerSize(innerSize);
}

