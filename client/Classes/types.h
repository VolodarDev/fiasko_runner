#ifndef __generated_xml_types__
#define __generated_xml_types__
#include <string>

namespace xml
{
	namespace templates 
	{
		extern const std::string WINDOW;
	}

	namespace scenesTitle 
	{
		extern const std::string LAYER;
		extern const std::string SCENE;
	}

	namespace scenesLoader 
	{
		extern const std::string SCENE;
	}

}

#endif
