#include "AppDelegate.h"
#include "support/AppArguments.h"
#include "model/ClientModel.h"
#include "scenes/BaseScene.h"
#include "scenes/MainMenu.h"
#include "scenes/loader/LoaderScene.h"
#include "scenes/title/TitleLayer.h"
#include "scenes/game/GameLayer.h"
#include "scenes/game/Block.h"
#include "scenes/game/ParalaxNode.h"
#include "support/UserData.h"
#include "support/FileSystemUtils.h"
#include "support/Settings.h"
#include "ml/Localization.h"
#include "ml/loadxml/xmlLoader.h"
#include "ml/mlObjectFactory.h"
#include "services/inapp/Purchase.h"
#include "inapp.h"

USING_NS_CC;

float WINDOW_WIDTH = 1920.f;
float WINDOW_HEIGHT = 1080.f;
float DESIGN_HEIGHT = 1080.f;
float WINDOW_SCALE = 0.5f;
const std::string WindowTitle = "TextQuestEngine";
bool EDITOR_MODE = false;

AppDelegate::AppDelegate()
{
	if (AppArguments::inst.has("-width"))
		WINDOW_WIDTH = strTo<float>(AppArguments::inst.get("-width"));
	if (AppArguments::inst.has("-height"))
		WINDOW_HEIGHT = strTo<float>(AppArguments::inst.get("-height"));
    if (AppArguments::inst.has("-win_scale"))
        WINDOW_SCALE = strTo<float>(AppArguments::inst.get("-win_scale"));
    if (AppArguments::inst.has("-app_mode"))
        EDITOR_MODE = AppArguments::inst.get("-app_mode") == "editor";
}

AppDelegate::~AppDelegate() 
{
}

void AppDelegate::initGLContextAttrs()
{
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};
    GLView::setGLContextAttrs(glContextAttrs);
}

static int register_all_packages()
{
    return 0;
}

bool AppDelegate::applicationDidFinishLaunching() {
	srand(static_cast<unsigned int>(time(nullptr)));
	
	UserData::shared();
	Localization::shared();
	registration();

#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
	xmlLoader::macros::set("PLATFORM_ANDROID", "yes");
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
	xmlLoader::macros::set("PLATFORM_IOS", "yes");
#else
	xmlLoader::macros::set("PLATFORM_ANDROID", "yes");
#endif
	
	launch();
	centerWindow();
	return true;
}

void AppDelegate::launch()
{
	configurePaths();
    UserData::shared().load();
	createWindow();
	Settings::apply();

	ClientModel::shared().reset();

	BaseScene::Pointer scene;
    if(!EDITOR_MODE)
        scene = LoaderScene::createStartScene();

	if (Director::getInstance()->getRunningScene())
		Director::getInstance()->replaceScene(scene);
	else
		Director::getInstance()->runWithScene(scene);
}

void AppDelegate::configurePaths()
{
	std::string root = FileSystemUtils::getAssetsPath();
	std::vector<std::string> paths;
	paths.push_back(root + "plists");
	paths.push_back(root + "textures");
	paths.push_back(root + "atlases");
	paths.push_back(root);
	
	FileUtils::getInstance()->setSearchPaths(paths);

#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC
	{
		std::string root = FileSystemUtils::getAssetsPath();
		assert(AppArguments::inst.has("-project_root"));
		if (AppArguments::inst.has("-project_root"))
			root = AppArguments::inst.get("-project_root");
									
		std::vector<std::string> paths;
		paths.push_back(root + "/assets/atlases");
		paths.push_back(root + "/assets/textures");
        paths.push_back(root + "/assets/plists");
		paths.push_back(root + "/assets/");

		FileUtils::getInstance()->setSearchPaths(paths);
	}
#endif
}

void AppDelegate::createWindow()
{
	auto director = Director::getInstance();

#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_MAC
	float W = WINDOW_WIDTH * WINDOW_SCALE;
	float H = WINDOW_HEIGHT * WINDOW_SCALE;
	if(EDITOR_MODE)
	{
		W = 1366 * WINDOW_SCALE;
		H = 768 * WINDOW_SCALE;
	}
	auto glview = Director::getInstance()->getOpenGLView();
	if (!glview)
	{
		glview = GLViewImpl::createWithRect(WindowTitle, Rect(0, 0, W, H));
		Director::getInstance()->setOpenGLView(glview);
	}
	else
	{
		glview->setFrameSize(W, H);
	}
#else
	auto glview = director->getOpenGLView();
	if (!glview)
	{
		glview = GLViewImpl::create(WindowTitle);
		director->setOpenGLView(glview);
	}
#endif
	changeDesignResolution();

	director->setAnimationInterval(1.f / 60.f);
}

void AppDelegate::changeDesignResolution()
{
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();

	if(!EDITOR_MODE)
	{
		float sx = glview->getFrameSize().width;
		float sy = glview->getFrameSize().height;
		float rate = DESIGN_HEIGHT / sy;
		sy = DESIGN_HEIGHT;
		sx *= rate;
		glview->setDesignResolutionSize(sx, sy, ResolutionPolicy::SHOW_ALL);
	}
	else
	{
		float sx = glview->getFrameSize().width;
		float sy = glview->getFrameSize().height;
		float rate = 768 / sy;
		sy = 768;
		sx *= rate;
		glview->setDesignResolutionSize(sx, sy, ResolutionPolicy::SHOW_ALL);
	}
}

void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();
	ClientModel::shared().onAppPaused();

    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();
	ClientModel::shared().onAppResumed();

    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void AppDelegate::centerWindow()
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_WIN32 || CC_TARGET_PLATFORM == CC_PLATFORM_MAC
	auto window = static_cast<GLViewImpl*>(Director::getInstance()->getOpenGLView())->getWindow();
	auto width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
	glfwSetWindowPos(window, 1920 - width - 5, 0);
#endif
}

void AppDelegate::registration()
{
	mlObjectFactory::shared().book<TitleLayer>("title_layer");
	mlObjectFactory::shared().book<GameLayer>("game_layer");
	mlObjectFactory::shared().book<Character>("character");
	mlObjectFactory::shared().book<Block>("block");
	mlObjectFactory::shared().book<MainMenu>("main_menu");
	mlObjectFactory::shared().book<ParalaxNode>("paralax_node");
}


inapp::SkuDetails inapp::Emulator::getDetails( const std::string& product )
{
	static std::map<std::string, inapp::SkuDetails> items;
	if( items.empty() )
	{
		items[inappNoAds] = inapp::SkuDetails(1.f,  "50.999emu.", inappNoAds, "No ads", "No ads", "USD", inapp::Result::Ok);
	}
	return items[product];
}

void inapp::fetch( std::list<std::string>& items )
{
	items.push_back(inappNoAds);
}
