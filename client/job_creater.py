import sys

name = raw_input()
name = name.strip()
if not name:
	exit(-1)

parent = 'mg::Job'

h = '''/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/

#ifndef __{0}_h__
#define __{0}_h__

#include "cocos2d.h"
#include "Job.h"
#include "ml/IntrusivePtr.h"

class {0} : public {3}
{1}
public:
	{0}();
	virtual ~{0}();
	virtual void execute() override;
private:
{2};

#endif
'''.format(name, "{", "}", parent)

cpp = '''/*
* Copyright 2014-2017 Vladimir Tolmachev
*
* Author: Vladimir Tolmachev
* Project: Text-Quest Engine
* e-mail: tolm_vl@hotmail.com
* If you received the code is not the author, please contact me
*/


#include "{0}.h"
#include "model/ClientModel.h"

using namespace cocos2d;

{0}::{0}()
{1}
{2}

{0}::~{0}()
{1}
{2}

void {0}::execute()
{1}
	_currentProgress = 1;
{2}
'''.format(name, "{", "}", parent)

open( name + ".h", "w" ).write(h)
open( name + ".cpp", "w" ).write(cpp)
