from PIL import Image
from stereo7 import fileutils


def run():
    files = fileutils.getFilesList('atlases/')

    for i, file in enumerate(files):
        if file.endswith('Store'):
            continue
        fullpath = 'atlases/' + file
        im = Image.open(fullpath)
        im.save(fullpath, quality=99)


if __name__ == '__main__':
    run()
